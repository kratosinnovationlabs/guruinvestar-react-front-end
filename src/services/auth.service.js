import axios from "axios";
import { BaseURL } from '../consts'

const register = (userObj) => {
  return axios.post(BaseURL + "/signup", userObj)
};

const login = (userEmail, userPassword) => {
  return axios
    .post(BaseURL + "/login", { user: { email: userEmail, password: userPassword }}, { headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*'
    }} )
    .then(response => {
      console.log(response);
      console.log(response.headers);
      console.log(response.headers.authorization);
      if (response.data) {
        localStorage.setItem("user", JSON.stringify(response.data));
      }
      if(response.headers.authorization){
        localStorage.setItem("JWTTOKEN", response.headers.authorization.split('Bearer ')[1]);

      }
      return response.data;
    });
};

const logout = () => {
  localStorage.removeItem("user");
};

export default {
  register,
  login,
  logout,
};