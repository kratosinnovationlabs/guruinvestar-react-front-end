const baseUrl = "http://localhost:4000/"

export async function getStockSummary(symbol) {
  // const response = await fetch(baseUrl + `quote_summary/${symbol}`);
  // if (response.ok) return response.json();
  // throw response;

  return fetch(baseUrl + `quote_summary/${symbol}`).then((response) => {
    if (response.ok) return response.json();
    throw response;
  });

}