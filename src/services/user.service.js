import axios from "axios";
import authHeader from "./auth-header";
import { BaseURL } from '../consts'

const getPublicContent = () => {
  return axios.get(BaseURL + "all");
};

const getUserBoard = () => {
  return axios.get(BaseURL + "user", { headers: authHeader() });
};

const getModeratorBoard = () => {
  return axios.get(BaseURL + "mod", { headers: authHeader() });
};

const getAdminBoard = () => {
  return axios.get(BaseURL + "admin", { headers: authHeader() });
};

export default {
  getPublicContent,
  getUserBoard,
  getModeratorBoard,
  getAdminBoard,
};
