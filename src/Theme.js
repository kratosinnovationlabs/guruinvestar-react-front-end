import React from 'react'
import { createMuiTheme } from '@material-ui/core/styles';

const Theme = createMuiTheme({

  palette: {
    primary: {
      light: '#fbf05b',
      main: '#faed32',
      dark: '#afa523',
      contrastText: '#000000',
    },
    secondary: {
      light: '#f3b268',
      main: '#f19f43',
      dark: '#a86f2e',
      contrastText: '#fff',
    },
    default: {
      light: '#375d75',
      main: '#053553',
      dark: '#03253a',
      contrastText: '#fff',
    },
    // success: {
    //   light: '#4692a3',
    //   main: '#18778c',
    //   dark: '#105362',
    //   contrastText: '#fff',
    // },
    //     secondary: {
    //   light: '#4b6c81',
    //   main: '#1e4862',
    //   dark: '#153244',
    //   contrastText: '#000',
    // },
    text: {
      primary: "#f5f6fa"
    },
    divider: '#5bb5bd',
    background: {
      paper: '#000',
      default: '#000000'
    }
  },
  typography: {

    fontFamily: "'Montserrat'",
    fontWeightRegular: 500,
    lineHeight: 1,

    h1: {
      color: '#fff'
    },
    h1: {
      color: '#fff'
    },
    h2: {
      color: '#000',
      fontSize: 'calc(16px + 0.5vw)',
      fontWeight: 'bold'
    },
    h3: {
      color: '#fff'
    },
    h4: {
      color: '#fff'
    },
    h5: {
      color: '#f5f6fa',
      fontSize: 'calc(16px + 0.5vw)',
      fontWeight: 'bold'
    },
    h6: {
      
      fontSize: '18px',
      fontWeight: 'bold',
      
      

    },
    subtitle1: {
      
      fontSize: '18px',
      fontWeight: 'bold',
      textAlign: 'center'
    },
    subtitle2:{
      fontSize: '12px',
      fontWeight: 'bold',

    },
    body1: {
      fontSize: '14px',
      fontWeight: 'bold',


    },
    body2:{
      fontSize: '12px',
      fontWeight: 'bold'
    },
    button:{
      fontSize: '16px',
      
      fontWeight: 'bold',


    }
  },
  zIndex:{
    mobileStepper: 1000,
    speedDial: 1050,
    appBar: 1100,
    drawer: 1200,
    modal: 1300,
    snackbar: 1400,
    tooltip: 1800
  },
  overrides: {
    MuiAppBar: {
      root:{
        backgroundColor: '#000000 !important'
      }
    },
    
    MuiBox:{
      root:{
        padding:'0px'
      }
    },
    MuiPaper: {
      root: {
        borderRadius: '6px',
      }
    },
    MuiListItemIcon:{
        root:{
            color: '#f5f6fa',
            minWidth: '46px'
        }
    },
    MuiDrawer:{
        paperAnchorDockedLeft: {
            borderRight: '0px',
            borderTop: '0px'
        }
    },
    MuiIconButton:{
        root:{
          color: '#f5f6fa'
        }
    },
    MuiSelect:{
      root: {
        color: '#faed32',
        textAlign: 'left',
        fontWeight: 500,
        border: 'solid 1px #707070',
        "&$selected": {
          "color": "#faed32"
        }
      },
      icon: {
        color: '#f5f6fa'
      },
      selectMenu: {
        backgroundColor: '#707070'
      }
    },
    MuiOutlinedInput:{
      root:{
        backgroundColor: "#444444",
        borderRadius: '6px',
        border: 'solid 1px #707070',
      },
      input:{
        paddingLeft: '15px'
      }
    },
    MuiButton: {
      root: {
        minWidth: '10vw'
      }
    },

    MuiCheckbox: {
      root: {
        color: '#f5f6fa'
      }
    },
    MuiTab: {
      root: {
        color: '#f5f6fa',
        padding:'0px 5px',
        minHeight:'30px'
      },
      wrapper: {
        
        fontWeight: 'bold',
        textTransform: "capitalize",
        alignItems:'center',
        fontSize:'18px',
        "@media (min-width: 1920px)": {
          fontSize:'25px'
        }
      },
      textColorInherit:{
        opacity: 1
      }
      
    },
    MuiTabPanel:{
      root:{
        padding:'0px '
      }
    },
    MuiTableCell: {
      root: {
        borderBottom: '0px',
        padding: '0px'
      },
      head: {
        height: '16px',
        padding: '05px'
      },
      sizeSmall:{
        padding: '0px ',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      }
    },
    MuiFormHelperText:{
      root:{
        color:'#ffffff'
      }
     
    },
    MuiRadio: {
      root: {
        color: 'inherit'
      }
    },

    MuiAutocomplete: {
      paper: {
        backgroundColor: '#707070'
      }
    },
    MuiFormControlLabel: {
      label: {
        fontSize: '12px'
      }
    },
    MuiOutlinedInput:{
      inputMarginDense:{
       color:'#c0c0c0',
       fontSize:'20px',
    },
    
  },
}

})
export default Theme
