import React from "react";


export default function NotFoundPage() {

  return (
    <div>Oops! Page not found.</div>    
  );
}
