import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  SET_MESSAGE
} from "./types";

import AuthService from "../services/auth.service";

export const register = (username, email, password) => (dispatch) => {
  return AuthService.register(username, email, password).then(
    (response) => {
      if( response.status === 200){
        dispatch({
          type: REGISTER_SUCCESS,
        });
      }
      
     
      // dispatch({
      //   type: SET_MESSAGE,
      //   payload: response.data.message,
      // });

      return Promise.resolve();
    },
    (error) => {
      debugger;
      // const message =
      //   (error.response &&
      //     error.response.data &&
      //     error.response.data.message) ||
      //   error.message ||
      //   error.toString();
      if(error.response.status === 422){
        dispatch({
          type: REGISTER_FAIL,
          payload: error.response.data,
        });
      }

      // dispatch({
      //   type: SET_MESSAGE,
      //   payload: message,
      // });

      return Promise.reject();
    }
  );
};

export const login = (email, password) => (dispatch) => {
  return AuthService.login(email, password).then(
    (response) => {
      if( response.status === 200){
        dispatch({
          type: LOGIN_SUCCESS,
          payload: { user: response.data },
        });
  
      }
      return Promise.resolve();

      
    },
    (error) => {
      //const message = (error.response && error.response.data && error.response.data.message) || error.message || error.toString();
      var errorMessage;
      if(error.response.status === 422 ){
        errorMessage = error.response.data.message
      }else{
        errorMessage = error.response.data.error
      }

      dispatch({
        type: LOGIN_FAIL,
        payload: errorMessage
      });
      

      // dispatch({
      //   type: SET_MESSAGE,
      //   payload: message,
      // });

      return Promise.reject();
    }
  );
};

export const logout = () => (dispatch) => {
  AuthService.logout();
  dispatch({
    type: LOGOUT,
  });
  // return AuthService.logout().then(
  //   () => {
      
  //     dispatch({
  //       type: LOGOUT,
  //     });

  //     return Promise.resolve();
  //   },
  //   (error) => {
  //     return Promise.reject();
  //   }
  // );

};