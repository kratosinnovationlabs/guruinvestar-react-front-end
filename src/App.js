import './App.css';
import React from 'react';
//import Navbar from './components/Header/Navbar';
import Grid from '@material-ui/core/Grid';
import clsx from 'clsx';
import Navbar from './components/Header/Navbar'
import SideMenu from './components/Header/SideMenu'
import { Route, Switch} from "react-router-dom";
import DashBoard from './components/Dashboard/DashBoard'
import AccountManagement from './components/Account_Management/AccountManagement'
import CreateYourScreen from './components/Create_Your_Screen/YourScreen'
import NotFoundPage from './NotFoundPage'
import { fade, makeStyles } from "@material-ui/core/styles";
import Theme from './Theme'
import { ThemeProvider } from '@material-ui/core/styles'
import Footer from './components/Footer/Footer'
import Login from './components/Authentication/Login'
import Logout from './components/Authentication/Logout'
import SignUp from './components/Authentication/SignUp'
import ForgotPassword from './components/Authentication/ForgotPassword'
import EditPassword from './components/Authentication/EditPassword'
import { withRouter } from 'react-router-dom'
import StockView from './components/StockView/StockView'
import TradeBook from './components/Guru/TradeBook'
import OtherGurus from './components/Guru/OtherGurus'
import Edit from './components/Profile/EditProfile'
import Demo from './components/Authentication/Demo'
import Income from './components/StockView/Income'
import BalanceSheet from './components/StockView/BalanceSheet'
import Investor from './components/StockView/Investor'
import CashFlow from './components/StockView/CashFlow'
import Home from './components/Home/Home'
import GuruList from './components/Guru/GuruList'
import GuruExit from './components/Guru/GuruExit'
import AllStocks from './components/Guru/AllStocks'
import Portfolio from './components/StockView/Portfolio'
import Notify from './components/HelpCenter/Notify';
import AlertOne from './components/HelpCenter/AlertOne'
import AlertTwo from './components/HelpCenter/AlertTWo'
import AlertThree from './components/HelpCenter/AlertThree'
import AlertFour from './components/HelpCenter/AlertFour'
import AlertFive from './components/HelpCenter/AlertFive'
import AlertConfig from './components/HelpCenter/AlertConfig'
import GuruAlert from './components/HelpCenter/GuruAlert';
import StockAlert from './components/HelpCenter/StockAlert';
import StockGuruAlert from './components/HelpCenter/StockGuruAlert';
import TeamManagment from './components/Team/TeamManagement'
import CreateUser from './components/Team/CreateUser';
import GuruPicks from './components/Guru/GuruPicks';
import FooterOne from './components/Footer/FooterOne';
import ConfirmEmail from './components/Profile/ConfirmEmail'
import { SnackbarContainer } from './context/SnackbarContext';
import Snackbar from './components/Utility/Snackbar';
import FavGurus from './components/Guru/FavGuru';
import FavStocks from './components/Guru/FavStock';





const drawerWidth = 250;
const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex'
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop: '150px',

  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  footer:{
    position:'relative',
    bottom:0,
  }
  // footer:{
  //   position:'absolute',
  //   bottom:0,
  //   marginTop:'20px'
  // }
}));
function App() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const toggleDrawer = () => {
    setOpen(!open);
  };

  return (
    <div className={classes.root}>
      <ThemeProvider theme={Theme}>
        <Navbar toggleDrawer={toggleDrawer} open={open}/>
        <SnackbarContainer>

        { localStorage.getItem("user") && 
          <SideMenu  open={open}/> 
        }
        <Snackbar />

        <main className={classes.content} >
            <Switch>
            <Route path="/GuruSearch" exact component={DashBoard} />
            <Route path="/stockview/:symbol" component={StockView} />
            <Route path="/create_your_screen" exact component={CreateYourScreen} />
            <Route path="/account_management" exact component={AccountManagement} />
            <Route path="/tradebook/:cik/:symbol" exact component={TradeBook} />
            <Route path="/logout" exact component={Logout} />
            <Route path="/signup" exact component={SignUp} />
            <Route path="/forgotpassword" exact component={ForgotPassword} />
            <Route path="/edit_password/:token" component={EditPassword}/>
            <Route path="/othergurus" exact component={OtherGurus} />
            <Route path="/editprofile" exact component={Edit} />
            <Route path="/demo" exact component={Demo} />
            <Route path="/balance" exact component={BalanceSheet} />
            <Route path="/income" exact component={Income} />
            <Route path="/investor" exact component={Investor} />
            <Route path="/cashflow" exact component={CashFlow} />
            <Route path="/" exact component={Login}/>
            <Route path="/home" exact component={Home}/>
            <Route path="/gurulist" exact component={GuruList}/>
            <Route path="/allstocks" exact component={AllStocks}/>
            <Route path="/guruexit" exact component={GuruExit}/>
            <Route path="/portfolio" exact component={Portfolio}/>
            <Route path="/notify" exact component={Notify}/>
            <Route path="/alert1" exact component={AlertOne}/>
            <Route path="/alert2" exact component={AlertTwo}/>
            <Route path="/alert3" exact component={AlertThree}/>
            <Route path="/alert4" exact component={AlertFour}/>
            <Route path="/alert5" exact component={AlertFive}/> 
            <Route path="/alertconfig" exact component={AlertConfig}/> 
            <Route path="/gurualert" exact component={GuruAlert}/> 
            <Route path="/stockalert" exact component={StockAlert}/> 
            <Route path="/stockgurualert" exact component={StockGuruAlert}/> 
            <Route path="/team" exact component={TeamManagment}/> 
            <Route path="/createuser" exact component={CreateUser}/> 
            <Route path="/gurupicks" exact component={GuruPicks}/>
            <Route path="/favgurus" exact component={FavGurus}/>
            <Route path="/favstocks" exact component={FavStocks}/>
            <Route path="/confirm_email/:toten" exact component={ConfirmEmail}/>

            {/* <Route path="/alert" exact component={MainAlert}/> */}



            <Route component={NotFoundPage} />
            
          </Switch>
         
          { localStorage.getItem("user") && 
          <FooterOne className={classes.footer} />
        }
        
      
        </main>
        </SnackbarContainer>
      </ThemeProvider>
     
    </div>
  );
}

export default withRouter(App); 