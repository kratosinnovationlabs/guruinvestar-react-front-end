import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import {Link} from 'react-router-dom';
import PriceWidget from '../Dashboard/PriceWidget';
import trading from '../../assets/trading.jpg';
import SaveIcon from '@material-ui/icons/Save';
import { Card } from '@material-ui/core';
import Tooltip from '@material-ui/core/Tooltip';
import { Redirect, useHistory } from 'react-router-dom';
import Footer from '../Footer/Footer';
import axios from 'axios';
import { BaseURL } from '../../consts';
import { green, red } from '@material-ui/core/colors';
import Spinner from '../../Spinner';
import { formattedDate, toTitleCase } from '../Utility/Utility';
import Appbar from '../Header/Appbar'
import { toMillion, errorHandler } from '../Utility/Utility';
import authHeader from '../../services/auth-header';
import {useSetSnackbar} from "../../hooks/useSnackbar";

const useStyles = makeStyles((theme) => ({

  head:{
    color: '#faed32',
    padding:'5px',
    fontSize:'25px',
   
  },
  
  
  table: {
    width:'100%',
    height:'90%',
    tableLayout: 'fixed'
  },
  favStockTitle:{
    color: theme.palette.primary.main

  },
  
  tableHeadCell:{
    
    color: '#ffc000',
    fontSize: '22px',
    padding:'0px 3px',
    fontWeight:'bold',
   
  },
  tableCell:{
    fontSize:'20px',
    padding:'0px 3px',
    
  
    
  },
  tableCellOne:{
    fontSize:'20px',
    padding:'0px 40px 0px 3px',
   
  },
  image:{
    marginTop:'35px',
    backgroundImage: `url(${trading})`,
    backgroundSize: 'cover',
    borderRadius: '4px',    
    height: 'calc(100% - 35px)'
  },
 
  link:{
    color:"#f88e00",
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: '3px',
    fontSize:'16px',
    
  },
  svgIcon:{
    color:'#fff000'
  },
  price:{
    borderColor:'#707070'

  },
  paper:{
    padding:'10px',
    marginTop:'1px',
    borderColor:'#707070'

  },
  dollar:{
    fontSize:'10px'
  },
  priceUp: {
    color: green
  },
  priceDown: {
    color: red
  },
 
  // footer:{
  //   marginTop:'500px'
  // },


  

}));


export default function Home() {
  const classes = useStyles();
  const [topGuruPicks, setTopGuruPicks ] = React.useState([])
  const [topGuruExits, setTopGuruExits ] = React.useState([])
  const [topFiveStocks, setTopFiveStocks ] = React.useState([])
  const [loading, setLoading] = React.useState(true);
  const setSnackbar = useSetSnackbar();
  const currentUser = JSON.parse(localStorage.getItem('user'))
  const history = useHistory()

  React.useEffect(() => {
    axios.get(BaseURL+`/guru/top_picks_exits`, {
      headers: authHeader()
    })
    .then(response =>{
      console.log(response)
      if (response.status===200){ 
        setTopGuruPicks(response.data.slice(0,5));
        setTopGuruExits(response.data.slice(1).slice(-5).reverse());
      }
    }).catch(error => {
      error && error.response && errorHandler(setSnackbar, history, error.response.status)
    })
  }, []);

  
  // const toMillion = (str) =>{
  //   var a = str  ;
  //   a = (str)/1000;
  //   a = parseInt(a);

  //   return(a);
  // }

  React.useEffect(() => {
    axios.get(BaseURL+`/guru/top_five_stocks`, {
      headers: authHeader()
    })
    .then(response =>{
      console.log(response)
      console.log(response.request.headers);
      if (response.status===200){ 
        // setTopFiveStocks(response.data);
        setTopFiveStocks(response.data.slice(0,5));
        setLoading(false);
      }
    }).catch(error => {
      setLoading(false);
      // if(error && error.response){
      //   errorHandler(setSnackbar, history, error.response.status)
      // }
      error && error.response && errorHandler(setSnackbar, history, error.response.status)
    })
  }, []);

  const addToFavGurus = (user_id,filer_id,security_id) => {
    try {
      axios({
          method: 'post',
          url: BaseURL + '/guru/add_to_favourite_guru',
          data: {
              guru: {
                user_id: user_id,
                filer_id: filer_id,
                security_id: security_id
              }
          },
          headers: authHeader()
      }).then(res => {
        // setLoading(false);
        setSnackbar(res.data.message, "success");
      }).catch(error => {
        // setLoading(false);
        error && error.response && errorHandler(setSnackbar, history, error.response.status)
      })
    } catch (e) {
      // setLoading(false);
      setSnackbar("Something went Wrong", "error")
    }
  }

  const addToFavStocks = (user_id,security_id,filer_id) => {
  
    try {
      axios({
          method: 'post',
          url: BaseURL + '/stock/add_to_favourite_stocks',
          data: {
              stock: {
                user_id: user_id,
                security_id: security_id,
                filer_id: filer_id               
              }
          },
          headers: authHeader()
      }).then(res => {
        // setLoading(false);
        setSnackbar(res.data.message, "success");
      }).catch(error => {
        // setLoading(false);
        // if (error.response && error.response.status !== 200) {
        //   setSnackbar("Stock not found!", "error")
        // }
        error && error.response && errorHandler(setSnackbar, history, error.response.status)
      })
    } catch (e) {
      // setLoading(false);
      setSnackbar("Some thing went Wrong", "error")
    }
  }

  if( localStorage.getItem('user') === null ){
    return <Redirect to="/" />;
  }
  
  // if (loading) return <Spinner />;
  
  return (
    <div >
      { loading ? <Spinner /> :
        <>
          <Appbar/>
            <Paper variant='outlined' className={classes.price}>
              <Grid container  direction="row" justify="space-around" alignItems="center">
              <Grid item xs={12} md={3} lg={3} sm={12 } >
                <PriceWidget title="NYSE" price={'15,337.85'} priceChange={'+8.29'}  percentChange={'(+0.06 %)'} gradientColor="#1fd286" />
              </Grid>
              <Grid item xs={12} md={3} lg={3} sm={12 }>
                <PriceWidget title="NASDAQ" price={'15,337.85'} priceChange={'+8.29'} percentChange={'(+0.06 %)'} gradientColor="#ffc700" />
              </Grid>
              <Grid item xs={12} md={3} lg={3}sm={12 }>
                <PriceWidget title="DOW & JONES" price={'20,337.85'} priceChange={'-10.29'} percentChange={'(-0.06 %)' } gradientColor="#1e5eff"  />
              </Grid>
              <Grid item xs={12} md={3} lg={3}sm={12}>
                <PriceWidget title="S & P FUTURES " price={'20,337.85'} priceChange={'-10.29'} percentChange={'(-0.06 %)'} gradientColor="#1e5eff"  />
              </Grid>
            </Grid>
            </Paper>
            <Grid container spacing={2} direction="row" justify="center" alignItems="stretch">
              <Grid item xs={12} sm={12} lg={6}>
                
                <Card className={classes.image}></Card>
              </Grid>
              <Grid item xs={12} sm={12} lg={6}>
                <Typography variant='h6' className={classes.head}>
                    Recent Top  Guru's Picks
                  </Typography>
                
                <Paper variant='outlined' className={classes.paper}>
                  <TableContainer component={Paper}>
                    <Table className={classes.table} size='small' aria-label="a dense table" >
                      <TableHead className={classes.tableHead}>
                        <TableRow >
                          <TableCell variant='head' style={{ width: '25%' }} className={classes.tableHeadCell}>GuruinveStars</TableCell>
                          <TableCell variant='head' style={{ width: '15%' }}  className={classes.tableHeadCell} align="left">Guru Type</TableCell>
                          <TableCell variant='head' style={{ width: '20%' }} className={classes.tableHeadCell} align="left">Stock </TableCell>
                          <TableCell variant='head' style={{ width: '15%' }} className={classes.tableHeadCell} align="left">Last <br/> Quarter</TableCell>
                          <TableCell variant='head' style={{ width: '20%' }} className={classes.tableHeadCell} align="right">Port. Value<Typography className={classes.dollar}>[in $'000] </Typography></TableCell>
                          <TableCell variant='head' style={{ width: '5%' }} className={classes.tableHeadCell} align="right"></TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        { topGuruPicks && topGuruPicks.map((row) => (
                          <TableRow key={row.name}>
                            <TableCell colSpan={1} component="th" scope="row"  className={classes.tableCell}>{toTitleCase(row.guru_name)}</TableCell>
                            <TableCell align='left'  className={classes.tableCell}>{row.guru_type}</TableCell>
                            <TableCell align='left'  className={classes.tableCell}>{  toTitleCase(row.stock_name) }</TableCell>
                            <TableCell align='left'  className={classes.tableCell}>{ formattedDate(row.last_qtr) }</TableCell>
                            <TableCell style={{ color: row.value >=0 ? 'green': 'red' }} align='right' className={classes.tableCell}>{ toMillion(row.value) }</TableCell>
                            <TableCell align="right" >
                            <Tooltip title="Add to favorite Guru" placement="right">
                              <Link variant="body" className={classes.wrapIcon}>
                                  <GroupAddIcon className = {classes.svgIcon} onClick = { () => addToFavGurus(currentUser.id, row.filer_master_id, row.security_id) } />  
                              </Link>
                              </Tooltip>
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                  <Link to='gurupicks' className={classes.link}>see more </Link>
                </Paper>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} lg={6}>
                <Typography variant='h6' className={classes.head}>
                  Recent Top Guru's Stocks
                </Typography>
                
                <Paper variant='outlined' className={classes.paper}>
                <TableContainer component={Paper}>
                    <Table className={classes.table} size='small' aria-label="a dense table" >
                      <TableHead className={classes.tableHead}>
                        <TableRow >
                          <TableCell style={{ width: '30%' }} variant='head' className={classes.tableHeadCell} align="left">GuruinveStars</TableCell>
                          <TableCell style={{ width: '23%' }} variant='head' className={classes.tableHeadCell} align="left">Stock </TableCell>
                          <TableCell style={{ width: '10%' }} variant='head' className={classes.tableHeadCell} align="left">Ticker</TableCell>
                          <TableCell style={{ width: '12%' }}  variant='head' className={classes.tableHeadCell} align="left">Last <br/> Quarter</TableCell>
                          <TableCell style={{ width: '20%' }}  variant='head' className={classes.tableHeadCell} align="left">Total #Shares <br/>  Invested</TableCell>                   
                          <TableCell style={{ width: '5%' }} variant='head' className={classes.tableHeadCell} align="right"></TableCell>

                        </TableRow>
                      </TableHead>
                      <TableBody>
                        { topFiveStocks && topFiveStocks.map((row) => (
                          <TableRow key={row.name}>
                            <TableCell className={classes.tableCell}>{row.guru_name && toTitleCase(row.guru_name)}</TableCell>
                            <TableCell className={classes.tableCell} align="left">{row.stock_name && toTitleCase(row.stock_name) }</TableCell>
                            <TableCell className={classes.tableCell} align="left">{ row.symbol}</TableCell>
                            <TableCell className={classes.tableCell} align="left">{ row.last_qtr && formattedDate(row.last_qtr) }</TableCell>
                            <TableCell className={classes.tableCellOne} align="right">{ row.value && row.value.toLocaleString('en-US') }</TableCell>
                            <TableCell className={classes.tableCell} align="right">
                            <Tooltip title="Save" placement="right">
                              <Link variant="body" className={classes.wrapIcon}>
                                <SaveIcon className = {classes.svgIcon} onClick = { () => addToFavStocks(currentUser.id, row.security_id, row.filer_id) }/>  
                              </Link>
                              </Tooltip>
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                <Link  to="/allstocks" className={classes.link}> see more </Link>
                </Paper>
              </Grid>
              <Grid item xs={12} sm={12} lg={6}>
                <Typography variant='h6'  className={classes.head}>Recent Top Guru's Exits</Typography>
                <Paper variant='outlined' className={classes.paper}>
                  <TableContainer component={Paper}>
                    <Table className={classes.table} size='small' aria-label="a dense table" >
                      <TableHead className={classes.tableHead}>
                        <TableRow >
                          <TableCell variant='head' style={{ width: '25%' }} className={classes.tableHeadCell}>GuruinveStars</TableCell>
                          <TableCell variant='head' style={{ width: '15%' }} className={classes.tableHeadCell} align="inherit">Guru Type</TableCell>
                          <TableCell variant='head' style={{ width: '20%' }} className={classes.tableHeadCell} align="inherit">Stock </TableCell>
                          <TableCell variant='head' style={{ width: '15%' }} className={classes.tableHeadCell} align="inherit"> Last <br/>   Quarter</TableCell>
                          <TableCell variant='head'  style={{ width: '20%' }} className={classes.tableHeadCell} align="right"> Port.Value<Typography className={classes.dollar}>[in $'000] </Typography> </TableCell>                   
                          <TableCell variant='head' style={{ width: '5%' }} className={classes.tableHeadCell} align="right"></TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        { topGuruExits && topGuruExits.map((row) => (
                          <TableRow key={row.name} >
                            <TableCell className={classes.tableCell} align="left" component="th" scope="row">{toTitleCase(row.guru_name)}</TableCell>
                            <TableCell className={classes.tableCell} align="left">{row.guru_type}</TableCell>
                            <TableCell className={classes.tableCell} align="left">{toTitleCase(row.stock_name)}</TableCell>
                            <TableCell className={classes.tableCell} align="left">{formattedDate(row.last_qtr)}</TableCell>
                            <TableCell style={{ color: row.difference >=0 ? 'green': 'red'  }}className={classes.tableCell} align="right">{toMillion(row.difference)}</TableCell>                     
                            <TableCell className={classes.tableCell} align="right">
                            <Tooltip title="Add to favorite Guru" placement="right">

                              <Link variant="body" className={classes.wrapIcon}>
                                  <GroupAddIcon className = {classes.svgIcon} onClick = { () => addToFavGurus(currentUser.id, row.filer_master_id, row.security_id) } />  
                              </Link>
                              </Tooltip>
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer> 
                  <Link to="guruexit" className={classes.link}>see more</Link>
                </Paper>  
              </Grid>
            </Grid>
          <Footer />
        </>
      }
    </div>

  );
}