import { useState, useEffect } from "react";

const baseUrl = "http://localhost:4000/";

export default function useFetch(url) {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  
//   useEffect(() => {
//     async function init() {
//       try {
//         const response = await fetch(baseUrl + url);
//         debugger;
//         if (response.ok) {
//           const json = await response.json();
//           debugger;
//           setData(json);
//         } else {
//           throw response;
//         }
//       } catch (e) {
//         setError(e);
//       } finally {
//         setLoading(false);
//       }
//     }
//     init();
//   }, [url]);
//   debugger;
//   return { data, error, loading };
// }

useEffect(() => {
  fetch(baseUrl + url)
    .then(res => res.json())
    .then(
      (result) => {
        setLoading(true);
        setData(result);
      },
      // Note: it's important to handle errors here
      // instead of a catch() block so that we don't swallow
      // exceptions from actual bugs in components.
      (error) => {
        setLoading(true);
        setError(error);
      }
    )
  }, []);
  return { data, error, loading };
}