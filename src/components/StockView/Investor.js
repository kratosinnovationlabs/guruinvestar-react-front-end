import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import {Link} from 'react-router-dom'
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import { useParams } from "react-router-dom";
import { Card } from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: 'black'
  },
  head:{
    color: '#faed32'
  },
  name:{
    color:'#fff000'
  },
  title:{
    color:'#ffc000'
  },
  link:{
    fontSize:'14px',
    color:'#ffc000',
    textDecoration:'underline',
    padding:'3px 0px'

  },
  linkOne:{
    color:'#ffffff'
  },
  breadCrumLink:{
    color: theme.palette.secondary.contrastText,
    textAlign: 'right',
    
  },
  key:{
    color:'#ffff00'
  },
  value:{
    color:'#ffc000',
   
  },
  tag:{
    color:'#ffff00',
    fontSize:'10px'
  },
  management:{
    color:'#faed32'
  },
  icon:{
    color:'#fff'
  },
  image:{
    height:'100px',
    width:'100px',
    backgroundColor:'#8C8989'
  }

}));

export default function Investor(props) {
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [data, setData] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const { symbol } = useParams();
  const baseUrl = "http://localhost:4000/";
  
  //const { data, loading, error } = useFetch(`quote_summary/${symbol}`);

  useEffect(() => {
    fetch(baseUrl+`quote_summary/${symbol}`)
      .then(res => res.json())
      .then(
        (result) => {
          setLoading(true);
          debugger;
          setData(result.quoteSummary.result[0].assetProfile);
          setLoading(false);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          setLoading(true);
          setError(error);
        }
      )
  }, [symbol]);

  
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  //if (loading) return <Spinner />;

  return (
    <div>
      <Grid container
        direction='row'
        justify='space-between'
        className={classes.head}>
          <Typography variant='h6' gutterBottom  >
            Investor Profile
          </Typography>
          <Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumLink}>
                
            <Link to='dashboard' className={classes.breadCrumLink}>
              {"Dashboard"}
            </Link>
            <Link  to="/investor" className={classes.breadCrumLink}>
                { "Investor Profile"}
              </Link >
          </Breadcrumbs>
        </Grid>  
        <Grid container>  
        <Grid item xs={1}>  
         <Typography variant="body1" gutterBottom  >
            <Link to="/portfolio" className={classes.key}>
            Fund Name
            </Link>
          </Typography>
        </Grid>
        <Grid item xs={5}>
          <Typography variant="body1" gutterBottom  className={classes.value}> Stone House Capital Maanagement,LLC  </Typography>     
        </Grid>
        <Grid item xs={1}>
          <Typography variant="body1" gutterBottom className={classes.key}> Logo </Typography>     
        </Grid>
        <Grid item xs={1}>
          <LinkedInIcon></LinkedInIcon>
        </Grid>
      </Grid>      
      <Grid container>    
        <Grid item xs={1}>
          <Typography variant="body1" gutterBottom className={classes.key}  >Address  </Typography>
        </Grid>
        <Grid item xs={5}>
          <Typography variant="body1"  gutterBottom className={classes.value} > 1019 Kane Concourse, Suite 202  Bay Harbor Islands, FL 33154 </Typography> 
        </Grid>
        <Grid item xs={2}>
          <Typography variant="body1" gutterBottom className={classes.key}>Company Index Key (CIK)</Typography>  
        </Grid>
         
        <Grid item xs={4}>
          <Typography variant="body1" gutterBottom className={classes.value}> 0001589943</Typography>     
        </Grid>
      </Grid>
      
      <Grid container> 
        <Grid item xs={1}
        container
        direction='column'>
          <Typography variant="body1" gutterBottom className={classes.key} >Phone </Typography>
          <Typography variant="body1" gutterBottom className={classes.key} >Email </Typography> 
          <Typography variant="body1" gutterBottom className={classes.key}> Website </Typography> 
          <Typography variant="body1" gutterBottom className={classes.key} >Est.Year </Typography>
          <Typography variant="body1" gutterBottom className={classes.key}>Investment Style</Typography> 

        </Grid>
        <Grid item xs={5}
        container
        direction='column'
        >
          <Typography variant="body1" gutterBottom className={classes.value}>212.543.1500 </Typography>
          <Link className={classes.link}>info@stonehousemgt.com</Link> 
          <Link className={classes.link}>https://www.stonehousemgt.com</Link> 
          <Typography variant="body1" gutterBottom className={classes.value} >2010</Typography>
          <Typography variant="body1" gutterBottom className={classes.value}>Investment Style</Typography> 

        </Grid>
        <Grid item xs={2}>
          <Typography variant="body1" gutterBottom className={classes.key}>Fund Manager</Typography>  
          <Typography variant="body1" gutterBottom className={classes.key}>Latest Quarter Reported</Typography>
          <Typography variant="body1" gutterBottom className={classes.key}>Portfolio Value </Typography> 
          <Typography variant="body2" gutterBottom className={classes.tag}>(last quarter in $ million)</Typography> 
          <Typography variant="body1" gutterBottom className={classes.key}>Portfolio Value </Typography>
          <Typography variant="body2" gutterBottom className={classes.tag}>(current value in $ million)</Typography> 
          <Typography variant="body1" gutterBottom className={classes.key}>Guruinvestar Ranking</Typography> 
        </Grid>
        <Grid item xs={4}>
          <Typography variant="body1" gutterBottom  className={classes.value}>MArk Cohen</Typography>    
          <Typography variant="body1" gutterBottom  className={classes.value}>Dec 2020</Typography>  
          <Typography variant="body1" gutterBottom  className={classes.value}>148.832</Typography>   <br/> 
          <Typography variant="body1" gutterBottom  className={classes.value}></Typography>    <br/>  
          <Typography variant="body1" gutterBottom  className={classes.value}></Typography>  
        </Grid>
      </Grid>
      <br/>
      <Grid container>
        <Grid item xs={1}>
          <Typography variant="body1" gutterBottom  className={classes.management}>Management Members</Typography>   

        </Grid>
        <Grid item xs={1}>
           <Card className={classes.image}>
           </Card>
        </Grid>
        <Grid item xs={4}>
          <Typography variant="body1" gutterBottom   className={classes.name}>Mark Cohen</Typography>
          <Typography variant="body1" gutterBottom className={classes.title} >Founder and Managing Partner</Typography>
          <Link className={classes.icon}><LinkedInIcon/></Link>
        </Grid>
        <Grid item xs={1}>
          <Card className={classes.image}>
          </Card>
        </Grid>
        <Grid item xs={4}>
        <Typography variant="body1" gutterBottom   className={classes.name}>Raphael Rabin-Havt</Typography>
        <Typography variant="body1" gutterBottom className={classes.title} >Investment Analyst</Typography>
        <Link className={classes.icon}><LinkedInIcon/></Link>
        </Grid>
      </Grid><br/>
      <Grid container>
        <Grid item xs={1}>
          <Typography variant="body1" gutterBottom className={classes.key} >Qualification</Typography>
        </Grid>
        <Grid item xs={3}>
          <Typography variant="body1" gutterBottom className={classes.value} >B.S. in Economics from theUniversity of Pennsylvania’s Wharton School</Typography>
        </Grid>
        <Grid item xs={2}>

        </Grid>
        <Grid item xs={3}>
          <Typography variant="body1" gutterBottom  className={classes.value}>BS from Cornell University and a JD from George Washington University Law School</Typography>
        </Grid>
      </Grid><br/>
      <Grid container>
        <Grid item xs={1}>
          <Typography variant="body1" gutterBottom  className={classes.key} >Past Experience</Typography>
        </Grid>
        <Grid item xs={2}>
          <Typography variant="body1" gutterBottom className={classes.value} >Investment Analyst at Force Capital Management, LLC</Typography>
        </Grid>
        <Grid item xs={3}></Grid>
        <Grid item xs={4}>
          <Typography variant="body1" gutterBottom  className={classes.value}>Editorial Researcher at the New York Times. From 2011 -2016, he was the Founder and Managing Member of GP&GM Capital, an investment partnership, and previously was an Associate at Ropes and Gray LLP</Typography>
        </Grid>
      </Grid>
        
    </div>
  );
}
