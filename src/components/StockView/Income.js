import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Sheets from './Sheets';
import { BaseURL } from '../../consts'
import { useParams } from "react-router-dom";
import { apend, prepend, getShortMonth, errorHandler } from '../Utility/Utility'
import authHeader from '../../services/auth-header';
import {useSetSnackbar} from "../../hooks/useSnackbar";
import { Redirect, useHistory } from 'react-router-dom';
import Spinner from '../../Spinner';

const useStyles = makeStyles(({
    
    viewStockDetails: {
      height: '30px', 
      fontSize: '16px',  
      lineHeight: '2.2',
      textAlign: 'left',
    },
    viewStock: {
        color:'#f0f000',
      textAlign: 'right',
      marginBottom:'10px'
    },
    table: {
      minWidth: 650,
    }  ,
    Header:{
      color: "#faed32",
      lineheight: '18px',
      fontSize: '18px'
    },
    paper:{
      padding:'5px'
    },
    link:{
        color: '#ffffff'
    },
    
    tradeDetails: {
      color:"#faed32",
      fontSize: '15px'
    },
    tradeHead: {
      backgroundColor: "#f89b00",
      color: "black"
    },
    tradeCell: {
      backgroundColor: "#000000", 
      borderRight: '3px solid white',
      textAlign:'left'
    },
    tradeTitle:{
      backgroundColor: "#000000", 
      color: '#faed32',
      fontSize: '25px',
      textAlign:'left'
    },
    tag:{
        fontSize: '11px'
    },
    tradeCellTwo:{
      color:'#eb8600',
      
    },
    tradeTitleTwo:{
      textAlign: 'left',
      color:'#eb8600',
      borderRight: '3px solid white'
    },
    linkTwo:{
      color:'#f0f000'
    },
    subHead:{
     color:'#fff000'
    }
 
  }));

  
export default function Income(props) {
  
  const classes = useStyles();
  const [data, setData] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [header, setHeader] = useState(null);
  const [rows, setRows] = useState(null);
  const [yearcount, setYearcount] = useState(0);
  const [quartercount, setQuartercount] = useState(0);
  const history = useHistory()
  const setSnackbar = useSetSnackbar();
  //const { symbol } = useParams();
  const { stock_profile_id } = props;

  // const rows=[ ["Total Revenue", "3,084", "3,363","3,400",  "2,724", "725","539","698","762", "2,724","bold" ],
  // ["Growth", "", "9.1%","1.1%",  "-19.9%", "","-25.7%","29.5%","9.2%", "","italic" ],
  // ["Cost of Revenue, Total", "1,165", "1,255","1,284",  "1,160", "294","257","289","320", "1,160","normal" ],
  // ["Gross Profit", "1,919", "2,108","2,116",  "1,564", "431","282","409","442", "1,564" ,"bold"],
  // ["Growth", "", "9.8%","0.4%",  "-26.1%", "","-34.6%","45.0%","8.1%", "","italic" ],
  // ["Margin", "62.2%", "62.7%","62.2%",  "57.4%", "59.4%","52.3%","58.6%","58.0%", "57.4%","italic" ],
  // ["Selling/General/Admin,Expenses,Total", "613", "697","707",  "701", "198","151","164","188", "701","normal" ],
  // ["Research & Development", "184", "202","188",  "166", "51","31","41","43", "166","normal" ],
  // ["Other Operating Expenses", "-", "-","-",  "-", "-","-","-","-", "-","normal" ],
  // ["EBITDA", "1,122", "1,209","1,221",  "697", "182","100","204","211", "697" ,"bold"],
  // ["Growth", "", "7.7%","1.0%",  "-42.9%", "","-45.1%","104.9%","3.4%", "" ,"italic"],
  // ["Margin", "36.4%", "35.9%","35.9%",  "25.6%", "25.1%","18.6%","29.2%","27.7%", "25.6%","italic" ],
  // ["Depreciation/ Amortization", "683", "690","638",  "554", "138","140","136","140", "556","normal" ],
  // ["EBIT", "439", "519","583",  "143", "44","-40","68","71", "143" ,"bold"],
  // ["Growth", "", "18.2%","12.3%",  "-75.5%", "","-190.9%","-270.0%","4.4%", "" ,"italic"],
  // ["Margin", "14.2%", "15.4%","17.1%",  "5.2%", "6.1%","-7.4%","9.7%","9.3%", "5.2%" ,"italic"],
  // ["Interest Income(Expense) Net Non Operating", "-583", "-572","-565",  "-509", "-126","-127","-129","-127", "-509" ,"normal"],
  // ["Other, Net", "0", "-12","-",  "-5", "-3","-1","-","-1", "-5" ,"normal"],
  // ["Net Income before Taxes", "-144", "-65","18",  "-371", "-85","-168","-61","-57", "-371","bold" ],
  // ["Provision for Income tax", "24", "13","10",  "4", "4","2","5","-7", "4" ,"normal"],
  // ["Effective Tax Rate", "-10.7%", "-3.9%","-9.3%",  "-0.7%", "-2.6%","-1.0%","-4.7%","-7.7%", "-0.7%","italic" ],
  // ["Net Income After Taxes", "-168", "-78","8",  "-375", "-89","-170","-66","-50", "-375","bold" ],
  // ["Minority Interest", "", "","12",  "-21", "-4","-5","-6","-6", "-21" ,"normal"],
  // ["Equity in Affiliates", "", "","",  "", "","","","", "","normal" ],
  // ["Diluted Net Income", "-168", "-78","-4",  "-396", "-93","-175","-72","-56", "-396" ,"bold"],
  // ["Growth", "", "-53.7%","-94.9%",  "####", "","88.2%","-58.9%","-22.2%", "" ,"italic"],
  // ["Margin", "-5.5%", "-2.3%","-0.1%",  "-14.5%", "-12.8%","-32.5%","-10.3%","-7.3%", "-14.5%" ,"italic"],
  // ["Diluted Weighted Average Shares", "89.1", "91.1","93",  "95", "94","95","95","95", "95" ,"normal"],
  // ["EPS", "$ -1.89", "$-0.86","$-0.04",  "$-4.17", "$-0.99","$-1.84","$-0.76","$-0.59", "$-4.17","normal" ],
  // ["DPS", "$ -", "$ -","$ -",  "$ -", "$ -","$ -","$ -","$ -", "$ -" ,"$ -"],
  // ["Payout Ratio", "0.0%", "0.0%","0.0%",  "0.0%", "0.0%","0.0%","0.0%","0.0%", "0.0%" ,"italic"],
  
  // ["Notes", "", "","",  "", "","","","", "" ,"bold"],
  // ["Unusual Expense(Income)", "84", "276","126",  "173", "66","28","45","34", "173" ,"normal"],
  // ["Total Extraordinary Items", "10", "-","-",  "-", "-","-","-","-", "-","normal" ],
  // ];

  // const header=["Scientific Games Crop","FY-Dec 2017 ", "FY-Dec 2018 ", "FY-Dec 2019 ","FY-Dec 2020 ",  "Q1-Mar 2020", " Q2-Jun 2020  ","Q3-Sep 2020  ","Q4-Dec 2020  ", "LTM" ]
 

  useEffect(() => {
    
    const keys_pairs = {
      "total_revenue": { label: "Total Revenue", type: "bold", append: "", prepend: "" },
      "growth": { label: "Growth", type: "italic", append: "%", prepend: "" },
      "cost_revenue": { label: "Cost of Revenue, Total", type: "normal", append: "", prepend: "" },
      "gross_profit": { label: "Gross Profit", type: "bold", append: "", prepend: "" },
      "gross_profit_growth": { label: "Growth", type: "italic", append: "%", prepend: "" },
      "gross_profit_margin": { label: "Margin", type: "italic", append: "%", prepend: "" },
      "expenses": { label: "Selling/General/Admin,Expenses,Total", type: "normal", append: "", prepend: "" },
      "rd": { label: "Research & Development", type: "normal", append: "" , prepend: "" },
      "other_expenses": { label: "Other Operating Expenses", type: "normal", append: "", prepend: "" },
      "ebitda": { label: "EBITDA", type: "bold", append: "", prepend: "" },
      "ebitda_growth": { label: "Growth", type: "italic", append: "%", prepend: "" },
      "ebitda_margin": { label: "Margin", type: "italic", append: "%", prepend: "" },
      "dep_or_amor":{ label: "Depreciation/ Amortization", type: "normal", append: "", prepend: "" },
      "ebit": { label: "EBIT", type: "bold", append: "", prepend: "" },
      "ebit_growth": { label: "Growth", type: "italic", append: "%", prepend: "" },
      "ebit_margin":{ label: "Margin", type: "italic", append: "%", prepend: "" },
      "interest_income": { label: "Interest Income(Expense) Net Non Operating", type: "normal", append: "", prepend: "" },
      "other_net": { label: "Other, Net", type: "normal", append: "", prepend: "" },
      "ni_before_taxes": { label: "Net Income before Taxes", type: "bold", append: "", prepend: "" },
      "provision_for_taxes": { label: "Provision for Income tax", type: "normal", append: "", prepend: "" },
      "ni_after_taxes": { label: "Net Income After Taxes", type: "bold", append: "", prepend: "" },
      "minority_interest": { label: "Minority Interest", type: "normal", append: "", prepend: "" },
      "equity_in_affiliates": { label: "Equity in Affiliates", type: "normal", append: "", prepend: "" },
      "dni": { label: "Diluted Net Income", type: "bold", append: "", prepend: "" },
      "dni_growth": { label: "Growth", type: "italic", append: "%", prepend: "" },
      "dni_margin": { label: "Margin", type: "italic", append: "%", prepend: "" },
      "dwas": { label: "Diluted Weighted Average Shares", type: "normal", append: "", prepend: "" },
      "eps": { label: "EPS", type: "normal", append: "", prepend: "$" },
      "dps": { label: "DPS", type: "normal", append: "", prepend: "$" },
      "payout_ratio": { label: "Payout Ratio", type: "italic", append: "%", prepend: "" },
      "notes": { label: "Notes", type: "bold", append: "", prepend: "" },
      "unusal_income": { label: "Unusual Expense(Income)", type: "normal", append: "", prepend: "" },
      "extra_items": { label: "Total Extraordinary Items", type: "normal", append: "", prepend: "" }
    }

    var formattedRows = []
    Object.keys(keys_pairs).forEach((key) => {
      var row = [];
      const valObj = keys_pairs[key];
      row.push(valObj.label)
    
      data.year_data && data.year_data.map((data) => {

        if( data[key] === null){
          row.push('-')
        }else{
          if(valObj.append !== null){
            row.push(apend(data[key], valObj.append))
          }else if(valObj.prepend !== null){
            row.push(prepend(data[key], valObj.prepend))
          }else{
            row.push(data[key])
          }
        }
      })
      data.year_data && setYearcount(data.year_data.length)

      data.quarter_data && data.quarter_data.map((data) => {
        if( data[key] === null){
          row.push('-')
        }else{
          if(valObj.append !== null){
            row.push(apend(data[key], valObj.append))
          }else if(valObj.prepend !== null){
            row.push(prepend(data[key], valObj.prepend))
          }else{
            row.push(data[key])
          }
        }
      })
      data.quarter_data && setQuartercount(data.quarter_data.length)
      data.ltm_data && data.ltm_data.map((data) => {
        if( data[key] === null){
          row.push('-')
        }else{
          if(valObj.append !== null){
            row.push(apend(data[key], valObj.append))
          }else if(valObj.prepend !== null){
            row.push(prepend(data[key], valObj.prepend))
          }else{
            row.push(data[key])
          }
        }
      })

      //row.push('-')
      row.push(valObj.type)

      formattedRows.push(row)
      

    })

    setRows(formattedRows)
    console.log(rows)
  }, [header])

  useEffect(() => {
    fetch(BaseURL + `/stock_income_statements_details/${stock_profile_id}`, {
        headers: authHeader()
      })
      .then(res => res.json())
      .then(
        (result) => {

          console.log(result)
          setLoading(true);
          setData(result);

          var tableHeader = [];
          tableHeader.push(result.stock.name)

          result.year_data && result.year_data.forEach((d) => {
            var date = new Date(d.period);
            var month = parseInt(date.getMonth());
            tableHeader.push(`FY-${getShortMonth(month)} ${date.getFullYear()}`)
          });

          result.quarter_data && result.quarter_data.forEach((d) => {
            var date = new Date(d.period);
            var month = date.getMonth() 
            var qtr = parseInt(parseInt(month) / 3)
            tableHeader.push(`Q${qtr+1}-${getShortMonth(month)} ${date.getFullYear()}`)
          });

          result.quarter_data && tableHeader.push("LTM")

          console.log(tableHeader)
          setHeader(tableHeader)

          setLoading(false);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        // (error) => {
        //   setLoading(true);
        //   setError(error);
        // }
      ).catch(error => {
        setLoading(false);
        error && error.response && errorHandler(setSnackbar, history, error.response.status)
      })
  }, []);
  console.log(rows)
  return (
    <div>
      { loading ? <Spinner /> :
        <>
          <Sheets rows={rows} header={header} yearcount={yearcount} quartercount={quartercount}/>
        </>
      }
    </div>
  );
}
  