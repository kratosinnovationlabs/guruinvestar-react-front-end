import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Sheets from './Sheets';
import { BaseURL } from '../../consts'
import { apend, prepend, errorHandler, getShortMonth } from '../Utility/Utility'
import authHeader from '../../services/auth-header';
import {useSetSnackbar} from "../../hooks/useSnackbar";
import { Redirect, useHistory } from 'react-router-dom';
import Spinner from '../../Spinner';

const useStyles = makeStyles(({
    
    viewStockDetails: {
      height: '30px', 
      fontSize: '16px',  
      lineHeight: '2.2',
      textAlign: 'left',
    },
    viewStock: {
        color:'#f0f000',
      textAlign: 'right',
      marginBottom:'10px'
    },
    table: {
      minWidth: 650,
    }  ,
    Header:{
      color: "#faed32",
      lineheight: '18px',
      fontSize: '18px'
    },
    paper:{
      padding:'5px'
    },
    link:{
        color: '#ffffff'
    },
    
    tradeDetails: {
      color:"#faed32",
      fontSize: '15px'
    },
    tradeHead: {
      backgroundColor: "#f89b00",
      color: "black"
    },
    tradeCell: {
      backgroundColor: "#000000", 
      borderRight: '3px solid white',
      textAlign:'left'
    },
    tradeTitle:{
      backgroundColor: "#000000", 
      color: '#faed32',
      fontSize: '25px',
      textAlign:'left'
    },
    tag:{
        fontSize: '11px'
    },
    tradeCellTwo:{
      color:'#eb8600',
      
    },
    tradeTitleTwo:{
      textAlign: 'left',
      color:'#eb8600',
      borderRight: '3px solid white'
    },
    linkTwo:{
      color:'#f0f000'
    },
    subHead:{
     color:'#fff000'
    }
 
  }));

  
export default function BalanceSheet(props) {
  const classes = useStyles();
  const [data, setData] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [header, setHeader] = useState(null);
  const [rows, setRows] = useState(null);
  const [yearcount, setYearcount] = useState(0);
  const [quartercount, setQuartercount] = useState(0);
  const history = useHistory()
  const setSnackbar = useSetSnackbar();
  const { stock_profile_id } = props;

  // const rows=[ ["Total Current Assets", "1,867", "3,163","3,400",  "2,724", "1,256","539","698","762","2,724","bold1" ],
  // ["Cash and Short Term Investments", "789", "168","253",  "1,106", "735","439","120","920","normal2" ],
  // ["Cash", "1,165", "1,224","1,284",  "1,160", "1,294","257","289","320","1,160 " ,"normal2"],
  // ["Cash and Equivalents", "1,919", "2,108","2,116",  "1,564", "431","282","409","442","1,564","normal2" ],
  // ["Short Term Investments", "-", "-","-",  "-", "-","-","-","-","normal2" ],
  // ["Total Receivables, Net","243", "222", "413","191",  "248", "223","128","114","56","bold2" ],
  // ["Account Receivables Trade, Net", "613", "697","707",  "701", "198","151","165","188","701","normal2" ],
  // ["Total Inventory", "184", "202","188",  "166", "51","31","41","43","166","bold2" ],
  // ["Prepaid Expenses", "284", "212","143",  "567", "132","31","41","43","166","normal1" ],
  // ["Other Current Assets, Total", "1,122", "8,992","8,680",  "9,259", "8,620","8,769","9,334","9,259" ,"normal1"],
  // ["Total Assets", "7,239", "7,123","7,512",  "7,012", "7,123","7,006","7,120","7,340","7,140","bold3" ],
  // ["Property/Plant/Eqp., Total - Net", "568", "108","91",  "79", "87","91","93","79","456","bold2" ],
  // ["Property/Plant/Eqp., Total - Gross", "690", "697","104",  "129", "108","114","121","129","501","normal2" ],
  // ["Accumulated Depreciation, Total", "500", "334","380",  "337", "374","378","369","337","545","normal2" ],
  // ["Goodwill, Net", "", "1,122","1,209",  "2,653", "2,466","2,593","2,662","2,653","697","bold2" ],
  // ["Intangibles, Net", "3,122", "2,209","3,221",  "2,689", "1,282","2,100","1,204","2,211","1,690" ,"bold2"],
  // ["Long Term Investments","1,867","3,163","3,400","2,724","1,256","539","698","762","2,724","normal2"],
  // ['Note Receivable - Long Term',"789","168","253","1,160","1,294","257","289","320","1,920","normal2"],
  // ["Other Long Term Assets, Total","1,165","1,224","1,284","1,160","1,294","257","289","320","1,160","normal2"],
  // ["Other Assets, Total","-","-","-","-","-","-","-","-","-","normal2"],
  // ["Total Current Liabilities","1,919","2,108","2,116","1,564","431","282","409","442","1,563","bold3"],
  // ["Accounts Payable","243","222","413","191","248","223","128","114","56","normal2"],
  // ["Accounts Payable","613","697","707","701","198","151","165","188","701","normal2"],
  // ["Accrued Expenses","184","202","188","166","51","31","41","43","166","normal2"],
  // ["Notes Payable/Short Term Debt","284","214","143","567","51","31","41","43","126","normal2"],
  // ["Notes Payable/Short Term Debt","1,122","1,209","1,221","697","182","100","204","211","697","normal2"],
  // ["Other Current liabilities, Total","7,329","7,123","7,152","7,012","7,123","7,006","7,120","7,340","7,140","normal2"],
  // ["Total Liabilities","568","645","567","405","345","523","412","213","456","bold3"],
  // ["Total Long Term Debt","690","697","707","701","198","251","125","188","501","bold2"],
  // ["Long Term Debt","439","500","580","697","182","100","140","211","545","normal2"],
  // ["Capital Lease Obligations","1,122","1,209","1,211","1,697","1,182","1,100","204","2,220","697","normal2"],
  // ["Deferred Income Tax","3,122","2,209","3,221","2,689","1,282","2,100","1,204","2,211","1,690","normal1"],
  // ["Minority Interest","1,867","3,163","3,400","2,724","1,256","539","698","762","2,724","normal1"],
  // ["Other Liabilities, Total","789","168","253","1,106","735","439","120","920","1,920","normal1"],
  // ["Total Equity","1,165","1,224","1,284","1,160","1,295","257","289","320","1,160","bold3"],
  // ["Redeemable Preferred Stock, Total","-","-","-","-","-","-","-","-","-","normal1"],
  // ["Pref. Stock - Non Redeem., Net","1,919","2,108","2,116","1,564","431","282","409","442","1,564","bold2"],
  // ["Common Stock, Total","243","222","413","919","2,108","2,116","1,564","442","56","normal1"],
  // ["Additional Paid-In Capital","613",'697',"807","701","189","151","166","188","201","normal1"],
  // ["Retained Earnings (Accum.Deficit)","184","212","143","567","132","31","41","43","166","normal1"],
  // ["Treasury Stock - Common","284","212","143","567","132","31","121","150","126","normal1"],
  // ["ESOP Debt Guarantee","1,122","1,209","1,221","697","181","100","204","211","697","normal1"],
  // ["Unrealized Gain (Loss)","7,231","7,120","7,512","7,012","7,123","7,006","7,120","7,340","7,140","normal1"],
  // ["Other Equity, Total","568","645","567","405","345","523","412","213","456","normal1"],
  // ["Total Liability. & Sharehold. Equity","690","697","707","701","198","251","125","188","501","bold3"],
  // ["Total Common Shares Outstanding","439",'500',"583","697","182","100","140",'211','545',"normal1"],
  // ["Total Preferred Shares Outstanding","1,122","1,209","1,221","1,697","1,182",'1,100','204','2,220'," 697","normal1"],
  // ];


  // const header=["","FY-Dec 2017 ", "FY-Dec 2018 ", "FY-Dec 2019 ","FY-Dec 2020 ",  "Q1-Mar 2020", " Q2-Jun 2020  ","Q3-Sep 2020  ","Q4-Dec 2020  ", "LTM" ]

    useEffect(() => {
      
      const keys_pairs = {
        "total_current_assets": { label: "Total Current Assets", type: "bold1", append: "", prepend: "" },
        "cash_and_strm_investments": { label: "Cash and Short Term Investments", type: "normal2", append: "", prepend: "" },
        "cash": { label: "Cash", type: "normal2", append: "", prepend: "" },
        "cash_equivalents": { label: "Cash and Equivalents", type: "normal2", append: "", prepend: "" },
        "strm_investments": { label: "Short Term Investments", type: "normal2", append: "", prepend: "" },
        "receivables_net": { label: "Total Receivables, Net", type: "bold2", append: "", prepend: "" },
        "accounts_receivables": { label: "Account Receivables Trade, Net", type: "normal2", append: "", prepend: "" },
        "inventory": { label: "Total Inventory", type: "bold2", append: "" , prepend: "" },
        "prepaid_expenses": { label: "Prepaid Expenses", type: "normal1", append: "", prepend: "" },
        "other_current_assets": { label: "Other Current Assets, Total", type: "normal1", append: "", prepend: "" },

        "total_assets": { label: "Total Assets", type: "bold3", append: "", prepend: "" },
        "ppe_net": { label: "Property/Plant/Eqp., Total - Net", type: "bold2", append: "", prepend: "" },
        "ppe_gross":{ label: "Property/Plant/Eqp., Total - Gross", type: "normal2", append: "", prepend: "" },
        "accu_dep": { label: "Accumulated Depreciation, Total", type: "normal2", append: "", prepend: "" },
        "good_will": { label: "Goodwill, Net", type: "bold2", append: "", prepend: "" },
        "intangibles":{ label: "Intangibles, Net", type: "bold2", append: "", prepend: "" },
        "ltrm_investments": { label: "Long Term Investments", type: "normal2", append: "", prepend: "" },
        "nr_ltrm": { label: "Note Receivable - Long Term", type: "normal2", append: "", prepend: "" },
        "other_ltrm_assets": { label: "Other Long Term Assets, Total", type: "normal2", append: "", prepend: "" },
        "other_assets": { label: "Other Assets, Total", type: "normal2", append: "", prepend: "" },

        "total_current_liabilities": { label: "Total Current Liabilities", type: "bold3", append: "", prepend: "" },
        "accounts_payable": { label: "Accounts Payable", type: "normal2", append: "", prepend: "" },
        "payable_or_occrued": { label: "Payable/Accrued", type: "normal2", append: "", prepend: "" },
        "accrued_expenses": { label: "Accrued Expenses", type: "normal2", append: "", prepend: "" },
        "strm_debt": { label: "Notes Payable/Short Term Debt", type: "normal2", append: "", prepend: "" },
        "cap_leases": { label: "Current Port. of LT Debt/Capital Leases", type: "normal2", append: "", prepend: "" },
        "other_current_liabilities": { label: "Other Current liabilities, Total", type: "normal2", append: "", prepend: "" },

        "total_liabilities": { label: "Total Liabilities", type: "bold3", append: "", prepend: "" },
        "total_ltrm_debt": { label: "Total Long Term Debt", type: "bold2", append: "", prepend: "" },
        "ltrm_debt": { label: "Long Term Debt", type: "normal2", append: "", prepend: "" },
        "capital_lease_obligations": { label: "Capital Lease Obligations", type: "normal2", append: "", prepend: "" },
        "deferred_tax": { label: "Deferred Income Tax", type: "normal1", append: "", prepend: "" },
        "minority_interest": { label: "Minority Interest", type: "normal1", append: "", prepend: "" },
        "other_liabilities": { label: "Other Liabilities, Total", type: "normal1", append: "", prepend: "" },

        "total_equity": { label: "Total Equity", type: "bold3", append: "", prepend: "" },
        "redeemable": { label: "Redeemable Preferred Stock, Total", type: "normal1", append: "", prepend: "" },
        "non_redeemable": { label: "Preferred Stock - Non Redeemable, Net", type: "bold2", append: "", prepend: "" },
        "common_stock": { label: "Common Stock, Total", type: "normal1", append: "", prepend: "" },
        "additional_paid": { label: "Additional Paid-In Capital", type: "normal1", append: "", prepend: "" },
        "retained_earnings": { label: "Retained Earnings (Accum.Deficit)", type: "normal1", append: "", prepend: "" },
        "treasury_stock": { label: "Treasury Stock - Common", type: "normal1", append: "", prepend: "" },
        "esop_debt_guarantee": { label: "ESOP Debt Guarantee", type: "normal1", append: "", prepend: "" },
        "unrealized_gain": { label: "Unrealized Gain (Loss)", type: "normal1", append: "", prepend: "" },
        "other_equity": { label: "Other Equity, Total", type: "normal1", append: "", prepend: "" },

        "total_liabilities_and_shareholders_equity": { label: "Total Liability. & Sharehold. Equity", type: "bold3", append: "", prepend: "" },
        "comm_shares": { label: "Total Common Shares Outstanding", type: "normal1", append: "", prepend: "" },
        "pref_shares": { label: "Total Preferred Shares Outstanding", type: "normal1", append: "", prepend: "" },
        "diff": { label: "Difference", type: "normal1", append: "", prepend: "" },

      }

      var formattedRows = []
      Object.keys(keys_pairs).forEach((key) => {
        var row = [];
        const valObj = keys_pairs[key];
        row.push(valObj.label)
      
        data.year_data && data.year_data.map((data) => {

          if( data[key] === null){
            row.push('-')
          }else{
            if(valObj.append !== null){
              row.push(apend(data[key], valObj.append))
            }else if(valObj.prepend !== null){
              row.push(prepend(data[key], valObj.prepend))
            }else{
              row.push(data[key])
            }
          }
        })
        data.year_data && setYearcount(data.year_data.length)
        data.quarter_data && data.quarter_data.map((data) => {
          if( data[key] === null){
            row.push('-')
          }else{
            if(valObj.append !== null){
              row.push(apend(data[key], valObj.append))
            }else if(valObj.prepend !== null){
              row.push(prepend(data[key], valObj.prepend))
            }else{
              row.push(data[key])
            }
          }
        })
        data.quarter_data && setQuartercount(data.quarter_data.length)
        //row.push('-')
        row.push(valObj.type)

        formattedRows.push(row)


      })

      setRows(formattedRows)
      console.log(rows)
    }, [header])

    useEffect(() => {
      fetch(BaseURL + `/stock_balance_sheets/${stock_profile_id}`, {
        headers: authHeader()
      })
        .then(res => res.json())
        .then(
          (result) => {
  
            console.log(result)
            setData(result);
  
            var tableHeader = [];
            debugger;
            tableHeader.push(result.stock.name)
  
            result.year_data.forEach((d) => {
              var date = new Date(d.period);
              var month = parseInt(date.getMonth());
              tableHeader.push(`FY-${getShortMonth(month)} ${date.getFullYear()}`)
            });
  
            result.quarter_data.forEach((d) => {
              var date = new Date(d.period);
              var month = date.getMonth() 
              var qtr = parseInt(parseInt(month) / 3)
              tableHeader.push(`Q${qtr+1}-${getShortMonth(month)} ${date.getFullYear()}`)
            });
  
            //tableHeader.push("LTM")
  
            console.log(tableHeader)
            setHeader(tableHeader)
  
            setLoading(false);
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          // (error) => {
          //   setLoading(true);
          //   setError(error);
          // }
        ).catch(error => {
          setLoading(false);
          error && error.response && errorHandler(setSnackbar, history, error.response.status)
        })
    }, []);

    //  if (loading) return <Spinner />;

    return (
      <div>
        { loading ? <Spinner /> :
        <>
          <Sheets rows={rows} header={header} yearcount={yearcount} quartercount={quartercount}/>
        </>
      }
      </div>
    );
  }
  