import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import Paper from "@material-ui/core/Paper";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TabPanel from '@material-ui/lab/TabPanel';
import { makeStyles } from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import { BaseURL } from '../../consts'
import { useParams } from "react-router-dom";
import {useSetSnackbar} from "../../hooks/useSnackbar";
import { Redirect, useHistory } from 'react-router-dom';
import TableContainer from '@material-ui/core/TableContainer';
import authHeader from '../../services/auth-header';
import Spinner from '../../Spinner';
import { errorHandler } from '../Utility/Utility'

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      backgroundColor: 'black'
    },
    paper:{
      padding:'0px!important'
    },
    customTableContainer: {
      overflowX: "auto",
      maxHeight:300,
      width:"100%",
      // "@media (min-width: 1920px)": {
      //   maxHeight:550,
      // }
    },
    table: {
      minWidth: 650,
      maxHeight:520,
      stickyHeader:'true'

    },
    head: {
      color:'#faed32',
     
    },
    tableLine:{
      color:'#faed32'
    },
    tableHead: {
      backgroundColor: '#f89b00',
      color: '#000000',
      padding:'0px 5px',
      fontSize:'25px',
      fontFamily:'Montserrat',
      fontWeight:'bold',
      height:'35px',
      // "@media (min-width: 1920px)": {
      //   fontSize:'25px'
      // }
    },
    tableCell: {
      color: '#fff',
      padding:'0px 5px',
      fontSize:'24px',
      // "@media (min-width: 1920px)": {
      //   fontSize:'24px'
      // }


    },
    link:{
      color:'#000000',
      fontWeight:'bold',
      textDecoration:'underline'
    },
    linkOne:{
      color: '#ffffff'
    },
    head: {
      color:'#faed32',
      fontSize:'28px'
     
    },
    about:{
      color:'#ffc000',
      fontSize:'22px',
      textAlign:'justify',
      // "@media (min-width: 1920px)": {
      //   fontSize:'22px'
      // }
    },
    linkOne:{
      fontSize:'22px',
      color:'#ffc000',
      textDecoration:'underline',
      // "@media (min-width: 1920px)": {
      //   fontSize:'22px'
      // }
    },
    
  }));
  
  
  function createData(name, age,  since, title) {
    return { name, age,  since, title};
  }
  
  

  
   
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  
  export default function TopExecuties(props) {
    const classes = useStyles();
    const [value, setValue] = useState(1);
    const [data, setData] = useState({});
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);
    const { symbol } = useParams();
    const history = useHistory()
    const setSnackbar = useSetSnackbar();
    useEffect(() => {
      fetch(BaseURL + `/stock_details/${symbol}`, {
        headers: authHeader()
      })
        .then(res => res.json())
        .then(
          (result) => {
            
            setData(result);
            setLoading(false);
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          // (error) => {
          //   setLoading(true);
          //   setError(error);
          // }
        ).catch(error => {
          setLoading(false);
          error && error.response && errorHandler(setSnackbar, history, error.response.status)
        })
    }, [symbol]);
  
    
    const handleChange = (event, newValue) => {
      setValue(newValue);
    };

    

    const calculate_age = dob => {
      var fiscal_year =  props.data.fiscal_year; 
      var year_born =  props.data.year_born;
      var age = fiscal_year - year_born;
      // const age = new fysl_yr(difference);
    
      // return Math.abs(age.getUTCFullYear() - year_born);
      return age
    }

    const formattedDate = (d) => {
      if( d ){
        var date = new Date(d);
        var dd = date.getDate();
        var mm = date.getMonth()+1; 
        var yy = date.getFullYear().toString().substr(-2);
        if(dd<10) { dd='0'+dd; } 
        if(mm<10) { mm='0'+mm; } 
  
        return `${mm}-${dd}-${yy}`
      }else{
        return null
      }
    }

    return (
      <div>
        { loading ? <Spinner /> :
          <>
            <Grid container>
                <Grid item xs={12}
                container
                justify='center'>
                  <Typography variant="body1" gutterBottom  className={classes.about}> { data.desc}</Typography>  
                </Grid>
                <Grid item xs={2}>
                  <Link className={classes.linkOne} to={data.website}>{data.website}</Link>
                </Grid>
            </Grid>
            <Paper><br/>
              <TableContainer classes={{root: classes.customTableContainer}} component={Paper}>
                <Table className={classes.table} size='small' stickyHeader aria-label="sticky table" >
                  <TableHead className={classes.tableHead} >
                    <TableRow>
                        <TableCell  style={{ width: '20%' }} align="left" className={classes.tableHead}>Name</TableCell>
                        {/* <TableCell  style={{ width: '5%' }} align="left" className= {classes.tableHead}>Age</TableCell> */}
                        <TableCell  style={{ width: '7%' }} align="left" className={classes.tableHead}>Since</TableCell>
                        <TableCell  style={{ width: '70%' }} align="left"className= {classes.tableHead}>Title </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {  props.data.map((row) => (
                      <TableRow key={row.name} >
                          <TableCell style={{ width: '10%' }} className={classes.tableCell} align="left" component="th" scope="row">{row.name}</TableCell>
                          {/* <TableCell style={{ width: '5%' }}  className={classes.tableCell} align="left">{calculate_age}</TableCell> */}
                          <TableCell style={{ width: '7%' }} className={classes.tableCell} align="left">{row.year_born}</TableCell>
                          <TableCell style={{ width: '38%' }} className={classes.tableCell} align="left">{row.title}</TableCell>
                          <TableCell style={{ width: '40%' }} className={classes.tableCell} align="left"></TableCell>

                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Paper>
          </>
        }
     </div>
    )};