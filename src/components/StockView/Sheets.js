import React ,{ useEffect, useState }  from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Link} from 'react-router-dom';
import { BaseURL } from '../../consts'
import { useParams } from "react-router-dom";
import { Height } from '@material-ui/icons';



const useStyles = makeStyles(({
    paper:{
      width:'100%',
    },
    customTableContainer: {
      overflowX: "auto",
      maxHeight:550,
      width:"100%",
      // "@media (min-width: 1920px)": {
      //   maxHeight:550,
      // }
    },
    table: {
        minWidth: 750,
        // maxHeight:'500px',
        stickyHeader:'true'
      }  ,
      Header:{
        color: "#faed32",
        lineheight: '18px',
        fontSize: '18px',
        position:'sticky'
      },
     
      link:{
          color: '#ffffff'
      },
      
      tradeDetails: {
        color:"#faed32",
        fontSize: '15px'
      },
      headCell: {
        backgroundColor: "#f89b00",
        color: "#000000",
        
        fontSize:'24px',
        fontWeight:'bold',
        position:'sticky',
        // "@media (min-width: 1920px)": {
        //   fontSize:'24px'
        // }
      },
      tableCell:{
      padding:'0px 5px',
      position:'sticky'

      },
      tradeTitle:{
        color: '#faed32',
        fontSize: '30px',
        textAlign:'left',
        fontWeight:'bold',
        position:'sticky',
        // "@media (min-width: 1920px)": {
        //   fontSize:'30px'
        // }
      },
      tag:{
          fontSize: '20px',
          // "@media (min-width: 1920px)": {
          //   fontSize:'20px'
          // }
      },
    
    
      tradeTitleTwo:{
        textAlign: 'left',
        color:'#eb8600',
        borderRight: '3px solid white'
      },
      link:{
        color:'#000000',
        textDecoration:'underline',
        fontWeight:'bold'
        
      },
      italic:{
        fontStyle:'italic',
        color:'#ffc000',
        fontSize:'22px',
        padding:'0px 15px 0px 0px',
        // "@media (min-width: 1920px)": {
        //   fontSize:'22px'
        // }


      },
      normal:{
        
        fontWeight:'normal',
        fontSize:'22px',
        padding:'0px 15px 0px 0px',
        // "@media (min-width: 1920px)": {
        //   fontSize:'22px'
        // }


      },
      normalOne:{
        fontWeight:'normal',
        padding:'0px 8px',
        fontSize:'22px',
        padding:'0px 15px 0px 15px',
        // "@media (min-width: 1920px)": {
        //   fontSize:'22px'
        // }


      },
      normalTwo:{
        fontWeight:'normal',
        padding:'0px 15px',
        fontSize:'22px',
        padding:'0px 15px  0px 25px',
        // "@media (min-width: 1920px)": {
        //   fontSize:'22px'
        // }


      },
      bold:{
        fontWeight:'bold',
        fontSize:'22px',
        padding:'0px 15px  0px 0px',
        // "@media (min-width: 1920px)": {
        //   fontSize:'22px'
        // }

      },
      boldOne:{
        fontWeight:'bold',
        color:"#ffc000",
        fontSize:'22px',
        padding:'0px 15px 0px 0px',
        // "@media (min-width: 1920px)": {
        //   fontSize:'22px'
        // }


      },
      boldTwo:{
        fontWeight:'bold',
        fontSize:'22px',
        padding:'0px 15px 0px 15px',
        // "@media (min-width: 1920px)": {
        //   fontSize:'22px'
        // }


      },
      boldThree:{
        fontWeight:'bold',
        color:"#ffc000",
        fontSize:'22px',
        padding:'20px 15px 0px 0px',
        // "@media (min-width: 1920px)": {
        //   fontSize:'22px'
        // }


      },
      headCellLast:{
        backgroundColor: "#f89b00",
        color: "#000000",
        padding:'0px 5px',
        fontSize:'24px',
        fontWeight:'bold',
        position:'sticky',
        padding:'0px 10px 0px 0px',
        // "@media (min-width: 1920px)": {
        //   fontSize:'24px'
        // }

      },
      quarter:{
        borderLeft:'1px solid',
        borderColor:'#707070'
      },
      notes:{
        padding:'30px 0px 0px 0px'
      }
}));

  
export default function Sheets(props) {
  const classes = useStyles();

  const [value, setValue] = useState(1);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  
  return (
    <div>
    <Paper className={classes.paper}>
      <br/>
      <TableContainer classes={{root: classes.customTableContainer}} component={Paper}>
        <Table className={classes.table} stickyHeader aria-label="sticky table">
          <TableHead >  
          
          <TableRow >
            <TableCell width="33%" className={classes.tradeTitle}> { props.header && props.header[0]} <br/><Typography variant="body1" className={classes.tag}  > *in Millions of USD(except for per share items)</Typography> </TableCell>
            
           {props.header && props.header.slice(1,).map((h) => (
             
             <TableCell width="7%" align='center'className={classes.headCell}>{h.split("-")[0]}<br/>{ h.split("-")[1]}</TableCell>
           ))
           
         }


              
            
            {/* { props.header && props.header.slice(1,-1).map((h) => (
              <TableCell width="7%" align='center'className={classes.headCell}>{h.split("-")[0]}<br/>{ h.split("-")[1]}</TableCell>
            ))}
            { props.header && props.header.slice(9,).map((h) => (
              <TableCell width="7%" align='right'className={classes.headCellLast}>{h}</TableCell>
            ))} */}
          </TableRow>
        </TableHead>
          <TableBody>
          { props.rows && props.rows.map((row) => (
              <TableRow>
               
                <TableCell width='33%' className={`${row[0]==="Notes"? classes.notes : ''} ${row[row.length-1] === "bold1" ? classes.boldOne : row[row.length-1] === "bold" ? classes.bold : row[row.length-1] === "bold2" ? classes.boldTwo : row[row.length-1] === "bold3" ? classes.boldThree  : ( row[row.length-1] === "italic" ? classes.italic : row[row.length-1] === "normal1" ? classes.normalOne : row[row.length-1] === "normal2" ? classes.normalTwo: classes.normal  ) }`}>{row[0]}</TableCell>
                { row.slice(1,props.yearcount+1).map((c) => (
                   
                  <TableCell  width='7%' className={`${classes.a} ${row[row.length-1] === "bold1" ? classes.boldOne : row[row.length-1] === "bold" ? classes.bold  : row[row.length-1] === "bold2" ? classes.boldTwo  : row[row.length-1] === "bold3" ? classes.boldThree  : ( row[row.length-1] === "italic" ? classes.italic :classes.normal ) }`} align='right'>{c}</TableCell>
                ))}
                  <TableCell  width='7%' className={`${ props.yearcount > 0 && classes.quarter} ${row[row.length-1] === "bold1" ? classes.boldOne : row[row.length-1] === "bold" ? classes.bold  : row[row.length-1] === "bold2" ? classes.boldTwo  : row[row.length-1] === "bold3" ? classes.boldThree  : ( row[row.length-1] === "italic" ? classes.italic :classes.normal ) }`} align='right'>{ props.yearcount > 0 && row[props.yearcount+1]}</TableCell>

                  { props.yearcount > 0 && row.slice(props.yearcount+2,-1).map((q) => (
                   
                   <TableCell  width='7%' className={`${classes.a} ${row[row.length-1] === "bold1" ? classes.boldOne : row[row.length-1] === "bold" ? classes.bold  : row[row.length-1] === "bold2" ? classes.boldTwo  : row[row.length-1] === "bold3" ? classes.boldThree  : ( row[row.length-1] === "italic" ? classes.italic :classes.normal ) }`} align='right'>{q}</TableCell>
                 ))}
                 
              </TableRow>
                     
          ))}

          </TableBody>
 
        </Table>
      </TableContainer>
    </Paper>
    </div>
  );
}