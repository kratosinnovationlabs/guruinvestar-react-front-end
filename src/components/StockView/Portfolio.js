import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Link} from 'react-router-dom';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Tooltip from '@material-ui/core/Tooltip';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';


const useStyles = makeStyles((theme) => ({
  
  
  head: {
    color:'#faed32',
   
  },
  subTitle:{
    fontSize:'14px'
  },
  subTitleEnd:{
    color:'#faed32',
    fontSize:'14px'

  },
  tableHeadStart:{
    color: '#ffc000',
    fontSize:'14px',
    fontWeight:'bold',
  },
  tableHead: {
    color: '#ffc000',
    fontSize:'14px',
    fontWeight:'bold',
    padding:'0px 8px'
  },
  tableCellStart:{
    fontSize:'12px',

  },
  tableCell: {
    fontSize:'12px',
    padding:'0px 8px'
  },
  linkOne:{
    color:'#f0f000',
    fontSize:'15px',
    textDecoration:'underline'
  },
  link:{
    color:'#ffffff'
  },
  svgIcon:{
    color:'#ffc000'
  },
  text:{
    color:'green',
    padding:'0px 8px'

  },
  paper:{
    padding:'5px'
  },
  
  key:{
    color:'#ffff00'
  },
  value:{
    color:'#ffc000',
    padding:'0px 10px'
  },
  breadCrumLink:{
    color: theme.palette.secondary.contrastText,
    textAlign: 'right',
    
  },

}));

function createData(company, securityType, tickerSymbol, sector, industry,value,securities,price,changeQuarter,cmp,changeLast,currentValue,tradeBook) {
  return {company, securityType, tickerSymbol, sector, industry,value,securities,price,changeQuarter,cmp,changeLast,currentValue,tradeBook };
}

const rows = [
  createData('SCIENTIFIC GAMES CROP', "Common Stock", "SGMS", 'Consumer',"Gambling","$1,31,731",'31,75,00','$41.49','-', '$59.50','43.4%','$1,88,913','View'),
  createData('Travel center of America', "Common Stock", "TA ", 'Consumer',"Gambling","$1,31,731",'31,75,00','$41.49','-', '$59.50','43.4%','$1,88,913','View'),
  createData('TWITTER INC', "Common Stock", "SGMS", 'Consumer',"Gambling","$1,31,731",'31,75,00','$41.49','-', '$59.50','43.4%','$1,88,913','View'),

];

export default function Portfolio(props) {
  const classes = useStyles();
 
 



  return (
    <>
    <div className={classes.root}>

      <Grid container >
        <Grid 
           container
           direction="row"
           justify="space-between"
           alignItems="center"className={classes.head}>
          <Typography variant='h6' gutterBottom  >Portfolio</Typography>
          <Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumLink}>
              <Link to ="/dashboard" className={classes.link}>
                {"Dashboard  "}
              </Link>
              <Link className={classes.link}>
                { " My Favorite Stock "}
              </Link >
              <Link  className={classes.link}>
              {" Stock Profile "}
              </Link>
            </Breadcrumbs>
        </Grid> 
        </Grid>
        <Grid container>
        <Grid item xs={9} >
          <Typography variant="button" gutterBottom className={classes.subTitle} >You are viewing the Portfolio of </Typography>
          <Typography variant="button" className={classes.subTitleEnd} gutterBottom  >Stone House Capital Management LLC</Typography>     
        </Grid>
        <Grid item xs={3}className={classes.tableLine}>
          <Link  to="/investor" className={classes.linkOne}>  Investor Profile Page  </Link>
        </Grid>
      </Grid>
     
        <Grid container >
          <Grid 
          direction='column'
          justify='flex-start'
          className={classes.key}>
            <Typography variant="body1" gutterBottom  >Central Index Key (CIK) no </Typography>
            <Typography variant="body1" gutterBottom  >Last Reported Quarter </Typography>
          </Grid>
          <Grid item xs={1} className={classes.value}
          container
          direction='column'
          justify='flex-start'
          >
            <Typography variant="body1"  gutterBottom  >1589943</Typography> 
            <Typography variant="body1" gutterBottom  >Dec 2020</Typography>
          </Grid>
          <Grid item xs={2}className={classes.key}
          container
          direction='column'
          justify='flex-start'>
            <Typography variant="body1" gutterBottom >Current Portfolio Value ($ million)</Typography>
            <Typography variant="body1" gutterBottom >Return</Typography> 
          </Grid>
          <Grid item xs={1}className={classes.value}
          container
          direction='column'
          justify='flex-start'>
            <Typography variant="body1" gutterBottom >$204.9</Typography>   
            <Typography variant="body1" gutterBottom >Mar 2021</Typography>      
          </Grid>
          <Grid item xs={2} className={classes.key}
          container
          direction='column'
          justify='flex-start'>
          <Typography variant="body1" gutterBottom  >Portfolio Value(last Quarter $m) </Typography>
          <Typography variant="body1" gutterBottom >Portfolio Beta</Typography>
          </Grid>
          <Grid item xs={4} className={classes.value}
          container
          direction='column'
          justify='flex-start'>
            <Typography variant="body1" gutterBottom  >148.8</Typography> 
            <Typography variant="body1" gutterBottom  >148.8</Typography> 

          </Grid>
        </Grid>


          <Table  aria-label="a dense table" size="small" >
            <TableHead  >
              <TableRow>
                
                <TableCell style={{width:'10%'}} variant='head' align="left" className={classes.tableHeadStart}>Company</TableCell>
                <TableCell style={{width:'7%'}} variant='head' align="left" className={classes.tableHead}>Security Type</TableCell>
                <TableCell style={{width:'5%'}} variant='head' align="left" className={classes.tableHead}>Ticker</TableCell>
                <TableCell style={{width:'7%'}} variant='head' align="left" className={classes.tableHead}>Sector </TableCell>
                <TableCell style={{width:'7%'}} variant='head' align="left" className={classes.tableHead}>Industry </TableCell>
                <TableCell style={{width:'5%'}} variant='head' align="right" className={classes.tableHead}>Value </TableCell>
                <TableCell style={{width:'7%'}} variant='head' align="left" className={classes.tableHead}>No.of Securities </TableCell>
                <TableCell style={{width:'6%'}} variant='head' align="right" className={classes.tableHead}>Price of Dec 2020 </TableCell>
                <TableCell style={{width:'10%'}} variant='head' align="left" className={classes.tableHead}>Change Last Qtr(No.of Securities) </TableCell>
                <TableCell style={{width:'5%'}} variant='head' align="right" className={classes.tableHead}>CMP </TableCell>
                <TableCell style={{width:'10%'}} variant='head' align="left" className={classes.tableHead}>Change Last Qtr </TableCell>
                <TableCell style={{width:'5%'}} variant='head' align="right" className={classes.tableHead}>Current Value </TableCell>
                <TableCell style={{width:'4%'}} variant='head' align="left" className={classes.tableHead}></TableCell>


              </TableRow>
            </TableHead>
            <TableBody>

            {rows.map((row) => (
              <TableRow key={row.name}>
            
                <TableCell style={{width:'10%'}}className={classes.tableCellStart}   align="left">{row.company}</TableCell>
                <TableCell style={{width:'7%'}}className={classes.tableCell}  align="left">{row.securityType}</TableCell>
                <TableCell style={{width:'5%'}}className={classes.tableCell}  align="left">{row.tickerSymbol}</TableCell>
                <TableCell style={{width:'7%'}}className={classes.tableCell}  align="left">{row.sector}</TableCell>
                <TableCell style={{width:'7%'}}className={classes.tableCell}  align="left">{row.industry}</TableCell>
                <TableCell style={{width:'5%'}}className={classes.tableCell}  align="right">{row.value}</TableCell>
                <TableCell style={{width:'7%'}}className={classes.tableCell}  align="left">{row.securities}</TableCell>
                <TableCell style={{width:'6%'}}className={classes.tableCell}  align="right">{row.price}</TableCell>
                <TableCell style={{width:'10%'}}className={classes.tableCell}  align="center">{row.changeQuarter}</TableCell>
                <TableCell style={{width:'5%'}}className={classes.tableCell}  align="right">{row.cmp}</TableCell>
                <TableCell style={{width:'10%'}}className={classes.tableCell}  align="left" className={classes.text}>{row.changeLast} </TableCell>
                <TableCell style={{width:'5%'}}className={classes.tableCell}  align="right">{row.currentValue}</TableCell>
                <TableCell style={{width:'10%'}}className={classes.tableCell}  color='#ffc000'align="left">
                <Tooltip title="Tradebook" placement="right">
                <VisibilityIcon className = {classes.svgIcon} ></VisibilityIcon> 
                </Tooltip>
                </TableCell>
              </TableRow>
            ))}
              
            </TableBody>
          </Table>
      

      
      
        </div>  
    
    </>
  );
}
