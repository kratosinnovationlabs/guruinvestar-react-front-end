import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Sheets from './Sheets';
import { BaseURL } from '../../consts'
import { apend, prepend, getShortMonth, errorHandler } from '../Utility/Utility'
import authHeader from '../../services/auth-header';
import {useSetSnackbar} from "../../hooks/useSnackbar";
import { Redirect, useHistory } from 'react-router-dom';
import Spinner from '../../Spinner';

const useStyles = makeStyles(({
    
    viewStockDetails: {
      height: '30px', 
      fontSize: '16px',  
      lineHeight: '2.2',
      textAlign: 'left',
    },
    viewStock: {
        color:'#f0f000',
      textAlign: 'right',
      marginBottom:'10px'
    },
    table: {
      minWidth: 650,
    }  ,
    Header:{
      color: "#faed32",
      lineheight: '18px',
      fontSize: '18px'
    },
    paper:{
      padding:'5px'
    },
    link:{
        color: '#ffffff'
    },
    
    tradeDetails: {
      color:"#faed32",
      fontSize: '15px'
    },
    tradeHead: {
      backgroundColor: "#f89b00",
      color: "black"
    },
    tradeCell: {
      backgroundColor: "#000000", 
      borderRight: '3px solid white',
      textAlign:'left'
    },
    tradeTitle:{
      backgroundColor: "#000000", 
      color: '#faed32',
      fontSize: '25px',
      textAlign:'left'
    },
    tag:{
        fontSize: '11px'
    },
    tradeCellTwo:{
      color:'#eb8600',
      
    },
    tradeTitleTwo:{
      textAlign: 'left',
      color:'#eb8600',
      borderRight: '3px solid white'
    },
    linkTwo:{
      color:'#f0f000'
    },
    subHead:{
     color:'#fff000'
    }
 
  }));

  
export default function BalanceSheet(props) {
  const classes = useStyles();
  const [data, setData] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [header, setHeader] = useState(null);
  const [rows, setRows] = useState(null);
  const history = useHistory()
  const { stock_profile_id } = props;
  const [yearcount, setYearcount] = useState(0);
  const [quartercount, setQuartercount] = useState(0);
  const setSnackbar = useSetSnackbar();

  // const rows=[ ["Cash from Operative Activities", "507", "346","546",  "471", "120","172","312","471","2,724" ,"bold1"],
  // ["Net Income", "-242", "-352","-118",  "-548", "-155","-353","-464","-548","1,920","bold2" ],
  // ["Deprciation/Depletion", "683", "690","647",  "554", "-","-","-","-","1,160","normal1" ],
  // ["Amortization", "-", "-","-",  "-", "-","-","-","-","1,564","normal1" ],
  // ["Deferred Taxes", "-6", "-33","-21",  "-22", "7","8","10" ,"-22","-","normal1"],
  // ["Non - Cash Items", "93", "123","137",  "305", "243","460","665","969","665" ,"normal1"],
  // ["Changes in Working Capital", "-121", "-81","-99",  "182", "25","57","101","72","135" ,"normal1"],
  // ["","","","","","","","","","","normal1"],
  // ["Cash From Investing Activities", "-415", "-798","-263",  "-173", "-31","-84","-134","-173","-1,321","bold3" ],
  // ["Capital Expenditure", "-294", "-391","-285",  "-190", "-53","-92","-142","-190","1,121" ,"normal1"],
  // ["Other Investing Cash Flow Items , Total", "-121", "-407","22",  "17", "22","8","8","17","121","normal1" ],
  // ["","","","","","","","","","","normal"],
  // ["Cash From Financing Activities", "580", "-156","-129",  "463", "-59","402","587","463","1,321","bold3" ],
  // ["Financing Cash Flow Items", "-111", "-83","-73",  "-46", "-9","-16","-30","-46","1,543" ,"normal1"],
  // ["Total Cash Dividends Paid", "-", "-","-",  "-", "-","-","-","-","-" ,"normal1"],
  // ["Issuance(Retirement) of Stock, Net", "10", "-21","341",  "2", "-","-2","-1","2","15","bold3" ],
  // ["Issuance(Retirement) of Debt, Net", "701", "-52","-397",  "507", "-50","420","618","507","213" ,"normal1"],
  // ["","","","","","","","","","","normal"],
  // ["Foreign Exchange Effects", "5", "-6","1",  "7", "-5","-1","1","7","4" ,"normal"],
  // ["Net Change in Cash", "677", "-614","155",  "768", "25","489","766","768","561" ,"bold3"],
  // ["","","","","","","","","","","normal"],
  // ["","","","","","","","","","","normal"],
  // ["Cash Taxes Paid", "38", "33","41",  "22", "6","7","18","22","121" ,"normal"],
  // ["Cash Interest Paid", "575", "633","549",  "471", "110","224","335","471","309" ,"normal"],
  
  // ];

  // const header=["Scientific Games Crop","FY-Dec 2017 ", "FY-Dec 2018 ", "FY-Dec 2019 ","FY-Dec 2020 ",  "Q1-Mar 2020", " Q2-Jun 2020  ","Q3-Sep 2020  ","Q4-Dec 2020  ", "LTM" ]
    
    useEffect(() => {
        
      const keys_pairs = {
        "operating_activities": { label: "Cash from Operative Activities", type: "bold1", append: "", prepend: "" },
        "net_income": { label: "Net Income", type: "bold2", append: "", prepend: "" },
        "depreciation": { label: "Deprciation/Depletion", type: "normal1", append: "", prepend: "" },
        "amortization": { label: "Amortization", type: "normal1", append: "", prepend: "" },
        "deferred_tax": { label: "Deferred Taxes", type: "normal1", append: "", prepend: "" },
        "non_cash_items": { label: "Non - Cash Items", type: "normal1", append: "", prepend: "" },
        "working_capital": { label: "Changes in Working Capital", type: "normal2", append: "", prepend: "" },
        
        "investing_activities": { label: "Cash From Investing Activities", type: "bold3", append: "" , prepend: "" },
        "capital_expenditures": { label: "Capital Expenditure", type: "normal1", append: "", prepend: "" },
        "other_investing_cash": { label: "Other Investing Cash Flow Items , Total", type: "normal1", append: "", prepend: "" },

        "financing_activities": { label: "Cash From Financing Activities", type: "bold3", append: "", prepend: "" },
        "financing_cash_flow_items": { label: "Financing Cash Flow Items", type: "normal1", append: "", prepend: "" },
        "dividends_paid":{ label: "Total Cash Dividends Paid", type: "normal1", append: "", prepend: "" },
        "issuance_of_stock": { label: "Issuance(Retirement) of Stock, Net", type: "bold3", append: "", prepend: "" },
        "issuance_of_debt": { label: "Issuance(Retirement) of Debt, Net", type: "normal1", append: "", prepend: "" },

        "foreign_exchange": { label: "Foreign Exchange Effects", type: "normal", append: "", prepend: "" },
        "net_change": { label: "Net Change in Cash", type: "bold3", append: "", prepend: "" },
        "taxes_paid": { label: "Cash Taxes Paid", type: "normal", append: "", prepend: "" },
        "interest_paid": { label: "Cash Interest Paid", type: "normal", append: "", prepend: "" }

      }

      var formattedRows = []
      Object.keys(keys_pairs).forEach((key) => {
        var row = [];
        const valObj = keys_pairs[key];
        row.push(valObj.label)
      
        data.year_data && data.year_data.map((data) => {

          if( data[key] === null){
            row.push('-')
          }else{
            if(valObj.append !== null){
              row.push(apend(data[key], valObj.append))
            }else if(valObj.prepend !== null){
              row.push(prepend(data[key], valObj.prepend))
            }else{
              row.push(data[key])
            }
          }
        })
        data.year_data && setYearcount(data.year_data.length)
        data.quarter_data && data.quarter_data.map((data) => {
          if( data[key] === null){
            row.push('-')
          }else{
            if(valObj.append !== null){
              row.push(apend(data[key], valObj.append))
            }else if(valObj.prepend !== null){
              row.push(prepend(data[key], valObj.prepend))
            }else{
              row.push(data[key])
            }
          }
        })
        data.quarter_data && setQuartercount(data.quarter_data.length)
        //row.push('-')
        row.push(valObj.type)

        formattedRows.push(row)


      })

      setRows(formattedRows)
      console.log(rows)
    }, [header])

    useEffect(() => {
      fetch(BaseURL + `/stock_cashflow_statements/${stock_profile_id}`, {
        headers: authHeader()
      })
        .then(res => res.json())
        .then(
          (result) => {
  
            console.log(result)
            setData(result);
  
            var tableHeader = [];
            tableHeader.push(result.stock.name)
  
            result.year_data.forEach((d) => {
              var date = new Date(d.period);
              var month = parseInt(date.getMonth());
              tableHeader.push(`FY-${getShortMonth(month)} ${date.getFullYear()}`)
            });
  
            result.quarter_data.forEach((d) => {
              var date = new Date(d.period);
              var month = date.getMonth() 
              var qtr = parseInt(parseInt(month) / 3)
              tableHeader.push(`Q${qtr+1}-${getShortMonth(month)} ${date.getFullYear()}`)
            });
  
            //tableHeader.push("LTM")
  
            console.log(tableHeader)
            setHeader(tableHeader)
  
            setLoading(false);
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          // (error) => {
          //   setLoading(true);
          //   setError(error);
          // }
        ).catch(error => {
          setLoading(false);
          error && error.response && errorHandler(setSnackbar, history, error.response.status)
        })
    }, []);
    console.log(rows)
    return (
      <div>
        { loading ? <Spinner /> :
          <>
            <Sheets rows={rows} header={header} yearcount={yearcount} quartercount={quartercount}/>
          </>
        }
      </div>
    );
  }
  