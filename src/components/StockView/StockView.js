import React, { useEffect, useState } from 'react';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import TabContext from '@material-ui/lab/TabContext';
import TabList from '@material-ui/lab/TabList';
import TabPanel from '@material-ui/lab/TabPanel';
import TopExecuties from './TopExecuties';
import CashFlow from './CashFlow';
import Income from './Income';
import BalanceSheet from './BalanceSheet';
import { BaseURL } from '../../consts'
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Footer from '../Footer/Footer'
//import { getStockSummary }from '../../services/yahooService'
import { useParams } from "react-router-dom";
import PropTypes from 'prop-types';
import Tabs from '@material-ui/core/Tabs';
import { Button } from '@material-ui/core';
import authHeader from '../../services/auth-header';
import {useSetSnackbar} from "../../hooks/useSnackbar";
import { Redirect, useHistory } from 'react-router-dom';
import Spinner from '../../Spinner';
import { errorHandler } from '../Utility/Utility'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: 'black',
    marginTop:'-30px'
  },
  table: {
    minWidth: 650
  },
  head: {
    color: '#faed32',
    fontSize: '28px',
    // "@media (min-width: 1920px)": {
    //   fontSize:'28px'
    // }

  },
  tableLine: {
    color: '#faed32'
  },
  tableHead: {
    backgroundColor: '#f89b00',
    color: '#000000'
  },
  tableCell: {
    color: '#000000'
  },
  link: {
    color: '#f0f000',
    fontSize: '15px',
    textDecoration: 'underline'
  },
  linkOne: {
    fontSize: '15px',
    color: '#ffc000',
    textDecoration: 'underline'
  },
  startTab: {
    padding: '0px !important',
    borderRight: '2px solid white',

  },
  tabButton: {
    color: '#000',
    backgroundColor: '#ffc000'
  },
  endTab: {
    padding: '0px 30px !important',
  },
  breadCrumLink: {
    color: theme.palette.secondary.contrastText,
    textAlign: 'right',
    fontSize: '16px',
    // "@media (min-width: 1920px)": {
    //   fontSize:'16px'
    // }

  },
  subtitleOne: {
    color: '#fff'
  },
  subtitleTwo: {
    color: '#faed32',
    padding: '0px 5px',
    fontWeight: 'bold'

  },
  key: {
    color: '#ffff00',
    fontSize: '25px',
    // "@media (min-width: 1920px)": {
    //   fontSize:'25px'
    // }
  },
  about: {
    color: '#ffc000',
  },
  value: {
    color: '#ffc000',
    padding: '0px 20px',
    fontSize: '25px',
    // "@media (min-width: 1920px)": {
    //   fontSize:'25px'
    // }

  },

  tab: {
    fontSize:'25px',
    color:'#ffff00',
    height:'40px',
    textAlign: 'center',
    margin:'0px 20px',
   
    '&:hover': {
      backgroundColor: '#ffff00',
     
      color:'#000',
      borderRadius:'6px'
      
    },
    '&$selected': {
     
      backgroundColor: '#ffff00',
      color:'#000'
    },
    // "@media (min-width: 1920px)": {
    //   fontSize:'25px !important',
    //   height:'40px'
    // }
    


  },
  activeTab:{
    backgroundColor:'#ffff00',
    color:'#000',
    borderRadius:'6px'
  }


}));


function createData(name, age, since, title) {
  return { name, age, since, title };
}
const rows = [
  createData('Dhananjay', "45", "12-April-2021 ", "Independent",),
  createData('Smitha', "30", "12-April-2021 ", "Independent"),
  createData('Samiksha', "32", "12-April-2021 ", "Independent"),
  createData('Charan', "30", "12-April-2021 ", "Independent"),
  createData('Satheesh', "44", "12-April-2021 ", "Independent"),
  ,
];
export default function StockView(props) {
  const classes = useStyles();
  const [value, setValue] = useState(1);
  const [data, setData] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const { symbol } = useParams();
  const history = useHistory()
  const setSnackbar = useSetSnackbar();
  //const { data, loading, error } = useFetch(`quote_summary/${symbol}`);

  useEffect(() => {
    fetch(BaseURL + `/stock_details/${symbol}`, {
      headers: authHeader()
    })
    .then(res => res.json())
    .then(
      (result) => {
        setData(result);
        setLoading(false);
      },
      // Note: it's important to handle errors here
      // instead of a catch() block so that we don't swallow
      // exceptions from actual bugs in components.
      // (error) => {
      //   setLoading(true);
      //   setError(error);
      // }
    ).catch(error => {
      setLoading(false);
      error && error.response && errorHandler(setSnackbar, history, error.response.status)
    })
  }, [symbol]);


  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  //if (loading) return <Spinner />;
  return (
    <div>
      { loading ? <Spinner /> :
        <>
         <div className={classes.root}>
            <Grid item xs={5}
              container
              direction='row'
              justify='flex-start' >
              <Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumLink}>

                <Link to='/dashboard' className={classes.breadCrumLink}>
                  {"Dashboard "}
                </Link>
                <Link className={classes.breadCrumLink}>
                  {"My Favorite Stock"}
                </Link>

                <Link className={classes.breadCrumLink}>
                  {" Stock Profile"}
                </Link>
              </Breadcrumbs>
            </Grid>
            <Typography variant='h6' gutterBottom className={classes.head} >{data.name}</Typography>
            <Grid container>
              <Grid item xs={3} container direction='row' justify='flex-start'>
                <Typography variant="body1" gutterBottom className={classes.key}>Sector </Typography>
                <Typography variant="body1" gutterBottom className={classes.value} >{data.sector && data.sector.name}</Typography>
              </Grid>



              <Grid item xs={4}
                container direction='row' justify='flex-start'>
                <Typography variant="body1" gutterBottom className={classes.key}>Industry</Typography>
                <Typography variant="body1" gutterBottom className={classes.value}>{data.industry && data.industry.name}</Typography>

              </Grid>

              <Grid item xs={3}
                container direction='row' justify='flex-start'>
                <Typography variant="body1" gutterBottom className={classes.key} >Stock Ticker </Typography>
                <Typography variant="body1" gutterBottom className={classes.value}>{data.symbol}</Typography>

              </Grid>

            </Grid>
            <TabContext value={value}>
              <Tabs
                variant="fullWidth"
                TabIndicatorProps={{
                  style: {
                    display: "none",
                    backgroundColor:'#ffff00'

                  },
                }} onChange={handleChange} aria-label="full width tabs example"  >
                  <Tab className={`${classes.tab} ${ value == 1 ? classes.activeTab : ''}`} value={1} label="Fact Sheet" ></Tab>
                  <Tab className={`${classes.tab} ${ value == 2 ? classes.activeTab : ''}`} label="Income Statement" value={2} />
                  <Tab className={`${classes.tab} ${ value == 3 ? classes.activeTab : ''}`} label="Balance Sheet" value={3} />
                  <Tab className={`${classes.tab} ${ value == 4 ? classes.activeTab : ''}`} label="Cash Flow Statement" value={4} />
              </Tabs>

              <TabPanel value={1}>{data.stock_company_officers && <TopExecuties data={data.stock_company_officers} />}</TabPanel>
              <TabPanel value={2}>{data.id && <Income stock_profile_id={data.id }/>}</TabPanel>
              <TabPanel value={3}>{data.id && <BalanceSheet stock_profile_id={data.id }/>}</TabPanel>
              <TabPanel value={4}>{data.id &&<CashFlow stock_profile_id={data.id }/>}</TabPanel>
            </TabContext>
            {/* <Footer/> */}
          </div>
        </>
      }
    </div>
  );
}
