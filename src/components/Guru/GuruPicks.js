import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Link} from 'react-router-dom';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import { Tooltip } from '@material-ui/core';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import axios from 'axios';
import { BaseURL } from '../../consts';
import Spinner from '../../Spinner';
import { formattedDate, toMillion, toTitleCase, errorHandler } from '../Utility/Utility';
import TablePagination from '@material-ui/core/TablePagination';
import ReactPaginate from 'react-paginate';
import {useSetSnackbar} from "../../hooks/useSnackbar";
import authHeader from '../../services/auth-header';
import { Redirect, useHistory } from 'react-router-dom';


const useStyles = makeStyles((theme) =>({

head:{
  color:'#faed32',
  fontSize:'28px'

},
paper: {
  padding:'8px',
  
  
},
Header:{
  color:'#faed32',
  size: '24px'
},
tradeHead: {
  backgroundColor: "#f89b00",
  color: "black",
  padding:'0px 5px',
  fontSize:'22px',
  fontWeight:'bold'
},
svgIcon:{
  color:'#faed32'
},
link:{
  color: '#fff'
},
tableCell:{
  padding:'0px 5px',
  fontSize:'22px'
},
breadCrumLink:{
  fontSize:"16px",
  color: theme.palette.secondary.contrastText,
  textAlign: 'right',
  
},
dollar:{
  fontSize:'14px'
},

pageContainer:{
  
  listStyle:'none',
  display:'flex',
  justifyContent:'flex-end',
  borderRadius:'5px',
  color:'#fff',
  cursor:'pointer',
  fontSize:'20px',
  padding:'10px'
},
prevBtn:{
  padding:'10px'
},
nxtBtn:{
  padding:'10px'
},

active:{
  color:'#000',
  backgroundColor:'#fff'

},
page:{

  padding:'0px 10px',
  borderRight:'2px solid #fff',

}

}));

// function createData(guruName, guruType,  stockName, lastChange, lastActive,) {
//   return {guruName, guruType,   stockName, lastChange, lastActive};
// }
//   const rows  = [
//     { guruName: "Dhananjay", guruType: "Sample", lastActive: "Q1", total: " 1132", stockName: "Twitter",  }, 
//     { guruName: "Rama Siva Challa", guruType: "Sample", lastActive: "Q1", total: "1132", stockName: "Amazon.com Inc",  }, 
//     { guruName: "Satheesh", guruType: "Sample", lastActive: "Q1", total: "1132", stockName: "HoneyWell Intl Inc",  }, 
//     { guruName: "Samiksha Seth", guruType: "Sample", lastActive: "Q1", total: "1132", stockName: "Tesla Motors Inc",  }, 
//     { guruName: "Smitha Shenoy", guruType: "Sample", lastActive: "Q1", total: "1132", stockName: "Interactive Intellegence",  }, 
//     { guruName: "Charan Kumar Borra", guruType: "Sample", lastActive: "Q1", total: "1132", stockName: "Twitter",  }, 
//     { guruName: "Gireesh Durga", guruType: "Sample", lastActive: "Q1", total: "1132", stockName: "Interactive Intellegence",  }, 
//     { guruName: "Srinu Borra", guruType: "Sample", lastActive: "Q1", total: "1132", stockName: "HoneWell Intl Inc",  }, 
//     { guruName: "Naresh Katta", guruType: "Sample", lastActive: "Q1", total: "1132", stockName: "Amazon.cpm Inc",  }, 
//     { guruName: "Lavanya Paddolker", guruType: "Sample", lastActive: "Q1", total: "1132", stockName: "Tesla Motors Inc",  }, 
//     { guruName: "Mounika Burla", guruType: "Sample", lastActive: "Q1", total: "1132", stockName: "Twitter",  },
//    ];


  

export default function GuruPicks() {
    const classes = useStyles();
    const [GuruPicks, setGuruPicks ] = React.useState([])
    const [loading, setLoading] = React.useState(true);
    const [picks, setPicks] = React.useState(0);
    const [pageNumber,setPageNumber] = React.useState(0);
    const setSnackbar = useSetSnackbar();
    const currentUser = JSON.parse(localStorage.getItem('user'))
    const history = useHistory()

    const picksPerPage = 15;
    const pagesVisited = pageNumber*picksPerPage;

    const pageCount = Math.ceil( GuruPicks.length/picksPerPage)

    const changePage =({selected}) =>{
      setPageNumber(selected);

    };

    
    React.useEffect(() => {
      axios.get(BaseURL+`/guru/top_picks_exits`, {
        headers: authHeader()
      })
      .then(response =>{
        if (response.status===200){ 
          setGuruPicks(response.data);
          setLoading(false);
        }
      }).catch(error => {
        setLoading(false);
        error && error.response && errorHandler(setSnackbar, history, error.response.status)
      })
    }, []);

    const addToFavGurus = (user_id,filer_id,security_id) => {
      try {
        axios({
            method: 'post',
            url: BaseURL + '/guru/add_to_favourite_guru',
            data: {
                guru: {
                  user_id: user_id,
                  filer_id: filer_id,
                  security_id: security_id
                }
            }, 
            headers: authHeader()
        }).then(res => {
            setSnackbar(res.data.message, "success");
        }).catch(error => {
          setLoading(false);
          error && error.response && errorHandler(setSnackbar, history, error.response.status)
        })
      } catch (e) {
        setLoading(false);
        setSnackbar("Some thing went Wrong", "error")
      }
    }


    return (
        <div >
          { loading ? <Spinner /> :
            <>
              <Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumLink}>

                <Link to ="/"  className={classes.link}>
                  {"Home "}
                </Link>
                <Link to="/gurupicks"  className={classes.link}>
                  { " List of GuruinveStars Picks "}
                </Link>
              </Breadcrumbs>
            
              <Typography variant="h6" className={classes.head}gutterBottom >List of GuruinveStars Picks</Typography>
                
                <Table  size="small" aria-label="a dense table">
                  <TableHead  >
                    <TableRow >
                    <TableCell align="left" style={{ width: '30%' }} className={classes.tradeHead}>GuruinveStars </TableCell>
                      <TableCell align="left" style={{ width: '15%' }} className={classes.tradeHead}>Guru Type</TableCell>
                      <TableCell align="left" style={{ width: '25%' }} className={classes.tradeHead}> Stock </TableCell>
                      <TableCell align="left" style={{ width: '8%' }} className={classes.tradeHead}> Last  <br/>Quarter</TableCell>
                      <TableCell align="right" style={{ width: '15%' }} className={classes.tradeHead}>Total Port. Value<Typography className={classes.dollar}>[in Million] </Typography></TableCell>
                      <TableCell align="center" style={{ width: '7%' }} className={classes.tradeHead}> </TableCell>

                    
      
                    </TableRow>
                  </TableHead>
                  <TableBody>

                  {GuruPicks && GuruPicks.slice(pagesVisited, pagesVisited + picksPerPage).map((row) => (
                  <TableRow  >
                        <TableCell className={classes.tableCell} style={{ width: '30%' }} align="left">{toTitleCase(row.guru_name)}</TableCell>
                        <TableCell className={classes.tableCell} style={{ width: '15%' }} align="left">{row.guru_type}</TableCell>
                        <TableCell className={classes.tableCell} style={{ width: '25%' }} align="left">{toTitleCase(row.stock_name)}</TableCell>
                        <TableCell className={classes.tableCell} style={{ width: '8%' }} align="left">{formattedDate(row.last_qtr)}</TableCell>
                        <TableCell className={classes.tableCell} style={{ width: '15%' }} align="right">{toMillion(row.value)}</TableCell>
                        <TableCell className={classes.tableCell} style={{ width: '7%' }}  align="center">
                          <Tooltip title="Add to Favorite" placement="right"> 
                          <Link variant="body" className={classes.wrapIcon}>
                              <GroupAddIcon className = {classes.svgIcon} onClick = { () => addToFavGurus(currentUser.id, row.filer_master_id, row.security_id) } />  
                          </Link>
                          </Tooltip>  
                        </TableCell>
          
                    </TableRow>
                    ))}
                </TableBody>
                </Table>
              <Grid container
                direction='row'
                justify='flex-end'
                alignItems='center'>
                <Typography variant='h6' className={classes.pagination}>Showing 15 Filers </Typography>
                <ReactPaginate
                previousLabel={''}
                nextLabel ={''}
                pageCount = {pageCount}
                onPageChange={changePage}
                containerClassName={classes.pageContainer}
                previousLinkClassName={classes.prevBtn}
                nextLinkClassName={classes.nxtBtn}
                disabledClassName={classes.disable}
                activeClassName={classes.active}
                pageClassName={classes.page}
                
                />
              </Grid>
            </>
          }
        </div>
      );
    }
    