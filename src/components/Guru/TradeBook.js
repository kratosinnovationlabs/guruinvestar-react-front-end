import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { BaseURL } from '../../consts';
import Spinner from '../../Spinner';
import { useParams } from "react-router-dom";
import axios from 'axios';
import {Link} from 'react-router-dom';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import authHeader from '../../services/auth-header';
import { Redirect, useHistory } from 'react-router-dom';
import { errorHandler } from '../Utility/Utility';
import {useSetSnackbar} from "../../hooks/useSnackbar";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(1),
    color: theme.palette.text.primary,
    
  },
  key:{
    color:'#ffff00',
    fontSize:'14px'
  },
  value:{
    color:'#ffc000',
    fontSize:'14px'
  },
  
  viewStockDetails: {
    height: '30px', 
    fontSize: '16px',  
    lineHeight: '2.2',
    textAlign: 'left',
  },
  viewStock: {
    textAlign: 'right',
    textTransform:'none'    
  },
  table: {
    minWidth: 1800,
    marginTop: '20px'
  }  ,

  tradeHeader:{
    color: "#faed32",
  
    fontSize: '28px'
  },
  tradeLine: {
    color: "#faed32", 
    fontSize: '12px',
    
  },
  tradeLineOne:{
    fontSize: '12px',
    padding:'5px'
  },
  tradeDetails: {
    color:"#faed32",
    fontSize: '15px'
  },
  tradeHead: {
    backgroundColor: "#f89b00",
    color: "black",
    fontSize: '14px'
    
  },
  tradeCell: {
    color: "#f5f6fa",
    lineHeight: '20px',
    fontSize: '12px',
    padding:'0px 5px'
  },
  tradeCellHead: {
    color: "#ffff00",
    lineHeight: '20px',
    fontSize: '14px',
    padding:'0px 5px'

  },
  preHeader: {
    borderBottom: '1px solid #f5f6fa',
  },
  endCell:{
    borderRight: '1px solid  #f5f6fa',
    color: "#f5f6fa",
    lineHeight: '20px',
    fontSize: '12px',
    padding:'0px 8px'

  },
  endCellHead:{
    borderRight: '1px solid  #f5f6fa !important',
    color: "#ffff00",
    lineHeight: '20px',
    fontSize: '15px',
    padding:'0px 8px'
  },
  rowEnd: {
    borderRight: '.5px solid #f5f6fa',

  },
  breadCrumLink:{
    color: theme.palette.secondary.contrastText,
    textAlign: 'right',
    
  },
  textFiledTwo:{
    paddingLeft:'100px'
  },
  textFiledThree:{
    // paddingLeft:'40px'
  },

  floatLeft: {
    float: 'left'
  }

}));




export default function TradeBook() {
  const classes = useStyles();
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const { cik, symbol } = useParams();
  const history = useHistory()
  const setSnackbar = useSetSnackbar();

  useEffect(() => {
      axios.get(BaseURL+`/trade_book/${cik}/${symbol}`, {
        headers: authHeader()
      })
      .then(response =>{
        console.log(response)
        if (response.status===200){ 
          setData(response.data);
          setLoading(false);
        }
      }).catch(error => {
        setLoading(false);
        error && error.response && errorHandler(setSnackbar, history, error.response.status)
      })
  }, []);

  const formattedCurrency = (num, type) => {
    if( parseInt(num) === 0 || num === null || num === undefined){
      return '-'
    }else{
      //Type Value 0 for Integer, 1 for Float
      if( type == 0){
        return num.toLocaleString('en-US') 
      }else{
        return parseFloat(num).toLocaleString('en-US') 
      }
    }
  }

  if (loading) return <Spinner />;

  return (

      <>
      <Grid container
      direction='row'
      justify='space-between'
       >
          <Typography variant="subtitle2" gutterBottom className={classes.tradeHeader}>
            Trade Book
          </Typography>
        <Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumLink}>
            <Link to='dashboard' className={classes.breadCrumLink}>
              {"Dashboard"}
            </Link>
            <Link to='dashboard' className={classes.breadCrumLink}>
              {"My Favorite Guru"}
            </Link>

            <Link className={classes.breadCrumLink}>
              {"Trade Book"}
            </Link>
        </Breadcrumbs>
        </Grid> 
        <Grid container>
        <Grid item xs={10} 
        container
        justify='flex-start'
        alignItems='baseline'
        >
            <Typography variant="subtitle2"  className={classes.tradeLine}>{ data && data["stock"]["name"] }</Typography><br/><Typography variant="subtitle2"  className={classes.tradeLineOne}> Trade Book of</Typography><Typography variant="subtitle2" gutterBottom className={classes.tradeLine}>{data && data["issuer"]["name"]}</Typography><Typography variant="subtitle2"  className={classes.tradeLineOne}> as on </Typography><Typography variant="subtitle2" gutterBottom className={classes.tradeLine}>{data && data['filings'].length > 0 && `${data['filings'][data['filings'].length - 1].qtr} ${data['filings'][data['filings'].length - 1].year}` }</Typography>
            
          </Grid>
          <Grid item xs={2} >
            <Button className={classes.viewStock} variant="contained" color="primary">
              View Stock Mood
            </Button>
          </Grid>
      </Grid>

        <Grid container direction="row" justify='space-between' alignItems='baseline' style={{width: '75%'}} >
          <Grid container xs={6}>
            <Grid item xs={4}>
              <Typography variant="h6" className={classes.key} gutterBottom > Portfolio Name </Typography>
              <Typography variant="h6" className={classes.key} gutterBottom > Stock Name </Typography>
              <Typography variant="h6" className={classes.key} gutterBottom > Industry </Typography>
              <Typography variant="h6" className={classes.key} gutterBottom > CUSIP </Typography>
            </Grid>
            <Grid item xs={8}>
              <Typography variant="h6" className={classes.value} gutterBottom >{data && data["issuer"]["name"]}</Typography>
              <Typography variant="h6" className={classes.value} gutterBottom >{ data && data["stock"]["name"] }</Typography>
              <Typography variant="h6" className={classes.value} gutterBottom >{ data && data["stock"]["industry"]["name"] }</Typography>
              <Typography variant="h6" className={classes.value} gutterBottom >{ data && data["cusip"]}</Typography>
            </Grid>
          </Grid>

          <Grid container xs={6}>
            <Grid item xs={4}>
              <Typography variant="h6" className={classes.key} gutterBottom > Sector </Typography>
              <Typography variant="h6" className={classes.key} gutterBottom > Stock Ticker </Typography>
              <Typography variant="h6" className={classes.key} gutterBottom > Stock Current MV of </Typography>
              <Typography variant="h6" className={classes.key} gutterBottom > % of Portfolio </Typography>
            </Grid>
        
            <Grid item xs={8}>
              <Typography variant="h6" className={classes.value} gutterBottom >{ data && data["stock"]["sector"]["name"] }</Typography>
              <Typography variant="h6" className={classes.value} gutterBottom >{ data && data["stock"]["symbol"]  }</Typography>
              <Typography variant="h6" className={classes.value} gutterBottom ></Typography>
              <Typography variant="h6" className={classes.key} gutterBottom ></Typography>
            </Grid>
          </Grid>
        </Grid> 
        
        <TableContainer component={Paper}>
          <Table className={classes.table}  size="small" aria-label="a dense table">
            <TableHead>
              <TableRow className={classes.header}> 
                <TableCell align="center" colSpan={2}className={classes.tradeHead}>Period</TableCell>
                <TableCell align="center" colSpan={3}className={classes.tradeHead}>Opening Balance</TableCell>
                <TableCell align="center" colSpan={4}className={classes.tradeHead}>Changes</TableCell>
                <TableCell align="center" colSpan={4}className={classes.tradeHead}>Closing Balance</TableCell>
                <TableCell align="center" colSpan={4}className={classes.tradeHead}>Profitability</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow className={classes.preHeader}>
                <TableCell align="left" className={classes.tradeCellHead}>Quater</TableCell>
                <TableCell align="left" className={classes.endCellHead}>Year</TableCell>
                <TableCell align="left" className={classes.tradeCellHead}>Shares</TableCell>
                <TableCell align="right" className={classes.tradeCellHead}>Price</TableCell>
                <TableCell align="right" className={classes.endCellHead }>Cost (in $)</TableCell>
                <TableCell align="left"className={classes.tradeCellHead}>Trans_dt</TableCell>
                <TableCell align="left"className={classes.tradeCellHead}>Buy/(Sell)</TableCell>
                <TableCell align="right"className={classes.tradeCellHead}>Avg Price</TableCell>
                <TableCell align="right"className={classes.endCellHead}>Trans_Value</TableCell>
                <TableCell align="left"className={classes.tradeCellHead}>Shares</TableCell>
                <TableCell align="right"className={classes.tradeCellHead}>Price</TableCell>
                <TableCell align="right"className={classes.endCellHead}>Cost (in $)</TableCell>
                <TableCell align="right"className={classes.endCellHead}>CMP ($)</TableCell>
                <TableCell align="left"className={classes.tradeCellHead}>Realized Cash Profit</TableCell>
                <TableCell align="left"className={classes.tradeCellHead}>Cumm Unrealized Profit</TableCell>
                <TableCell align="left"className={classes.tradeCellHead}>Cumm Total Cash Profit</TableCell>

            </TableRow>
              { data && data["filings"].length > 0 ?  
                data["filings"].map((d) => (
                  <TableRow>
                    <TableCell align="left" className={classes.tradeCell}>{d.qtr}</TableCell>
                    <TableCell align="left" className={classes.endCell}>{d.year}</TableCell>
                    <TableCell align="left" className={classes.tradeCell}>{ formattedCurrency( d.opening_balance_shares, 0) }</TableCell>
                    <TableCell align="right" className={classes.tradeCell}>{ formattedCurrency(d.opening_balance_price, 0) }</TableCell>
                    <TableCell align="right" className={classes.endCell }>{ formattedCurrency(d.opening_balance_cost, 1) }</TableCell>
                    <TableCell align="left"className={classes.tradeCell}>{ d.changes_trans_dt }</TableCell>
                    <TableCell align="left"className={classes.tradeCell}>{ formattedCurrency( d.changes_buy_sell, 0 ) }</TableCell>
                    <TableCell align="right"className={classes.tradeCell}>{ d.changes_avg_price }</TableCell>
                    <TableCell align="right"className={classes.endCell}>{ formattedCurrency(d.changes_trans_val,1) }</TableCell>
                    <TableCell align="left"className={classes.tradeCell}>{ formattedCurrency(d.closing_balance_shares,0) }</TableCell>
                    <TableCell align="right"className={classes.tradeCell}>{  formattedCurrency(d.closing_balance_price, 0) }</TableCell>
                    <TableCell align="right"className={classes.endCell}>{ d.closing_balance_cost}</TableCell>
                    <TableCell align="right"className={classes.endCell}>{ d.closing_balance_cmp}</TableCell>
                    <TableCell align="left"className={classes.tradeCell}>{  formattedCurrency(d.profitability_cash_profit, 1)}</TableCell>
                    <TableCell align="left"className={classes.tradeCell}>{ formattedCurrency( d.profitability_unrealized_profit, 1) }</TableCell>
                    <TableCell align="left"className={classes.tradeCell}>{ formattedCurrency( d.profitability_total_cash_profit, 1) }</TableCell>
                  </TableRow>
                ))
                :
                <TableRow colspan={16} align="center" className={classes.tradeCell}>No Pricing Data available</TableRow>
              }
            </TableBody>
          </Table>
        </TableContainer>
        <br/><br/><br/><br/>
      
    </>
  );
}

