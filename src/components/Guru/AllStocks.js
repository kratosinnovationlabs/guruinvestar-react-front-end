import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Link} from 'react-router-dom';
import { Tooltip } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import ReactPaginate from 'react-paginate';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import axios from 'axios';
import { BaseURL } from '../../consts';
import Spinner from '../../Spinner';
import { toMillion, formattedDate, toTitleCase, errorHandler } from '../Utility/Utility';
import {useSetSnackbar} from "../../hooks/useSnackbar";
import authHeader from '../../services/auth-header';
import { Redirect, useHistory } from 'react-router-dom';



const useStyles = makeStyles((theme)=>({

head:{
  align: 'left',
  color:'#faed32',
  fontSize:'28px'

},
paper:{
 padding: '0px'
},
Header:{
  margin:'10px',
  color:'#faed32',
  size: '24px'
},
tableHead: {
  backgroundColor: "#f89b00",
  color: "black",
  padding:'0px 5px',
  fontWeight:'bold',
  fontSize:'24px'
 
},
svgIcon:{
  color:'#faed32'
},
link:{
  color:'white'
},
tableCell:{
  padding:'0px 5px',
  fontSize:'22px'
},
breadCrumLink:{
  color: theme.palette.secondary.contrastText,
  textAlign: 'right',
  fontSize:'16px'
},
pageContainer:{
  
  listStyle:'none',
  display:'flex',
  justifyContent:'flex-end',
  borderRadius:'5px',
  color:'#fff',
  cursor:'pointer',
  fontSize:'20px',
  padding:'10px'
},
prevBtn:{
  padding:'10px'
},
nxtBtn:{
  padding:'10px'
},

active:{
  color:'#000',
  backgroundColor:'#fff'

},
page:{

  padding:'0px 10px',
  borderRight:'2px solid #fff',

}



}));




export default function AllStocks() {
    const classes = useStyles();
    const [allStocks, setAllStocks ] = React.useState([])
    const [loading, setLoading] = React.useState(true);
    const [pageNumber,setPageNumber] = React.useState(0);
    const setSnackbar = useSetSnackbar();
    const currentUser = JSON.parse(localStorage.getItem('user'))

    const picksPerPage = 20;
    const pagesVisited = pageNumber*picksPerPage;

    const pageCount = Math.ceil( allStocks.length/picksPerPage)
    const history = useHistory()
    const changePage =({selected}) =>{
      setPageNumber(selected);

    };


    React.useEffect(() => {
      axios.get(BaseURL+`/guru/top_five_stocks`, {
        headers: authHeader()
      })
      .then(response =>{
        console.log(response)
        if (response.status===200){ 
          setAllStocks(response.data);
          setLoading(false);
        }
      }).catch(error => {
        setLoading(false);
        error && error.response && errorHandler(setSnackbar, history, error.response.status)
      })
    }, []);

    const addToFavStocks = (user_id,security_id,filer_id) => {
  
      try {
        axios({
            method: 'post',
            url: BaseURL + '/stock/add_to_favourite_stocks',
            data: {
                stock: {
                  user_id: user_id,
                  security_id: security_id,
                  filer_id: filer_id               
                }
            },
            headers: authHeader()
        }).then(res => {
            setSnackbar(res.data.message, "success");
        }).catch(error => {
          setLoading(false);
          error && error.response && errorHandler(setSnackbar, history, error.response.status)
        })
      } catch (e) {
        setLoading(false);
        setSnackbar("Some thing went Wrong", "error")
      }
    }

    if (loading) return <Spinner />;

    return (
      <div>
        { loading ? <Spinner/> :
          <>
            <Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumLink}>
              <Link to ="/" className={classes.link}>{"Home"}</Link>
              <Link to="/allstocks"  className={classes.link}>{ " List of Stocks"}</Link>
            </Breadcrumbs>
            <Typography className={classes.head}variant="h6" gutterBottom >List of Stocks</Typography>
              <Table  size="small" aria-label="a dense table">
                <TableHead >
                  <TableRow >
                  <TableCell align="left" style={{ width: '35%' }}   className={classes.tableHead}> GuruinveStars</TableCell>
                    <TableCell align="left" style={{ width: '25%' }} className={classes.tableHead}>Stock </TableCell>
                    <TableCell align="left" style={{ width: '7%' }}  className={classes.tableHead}>Ticker</TableCell>
                    <TableCell align="left" style={{ width: '8%' }} className={classes.tableHead}>Last Quarter</TableCell>
                    <TableCell align="right" style={{ width: '15%' }}  className={classes.tableHead}> Total Shares <br/>Invested</TableCell>                 
                    <TableCell align="left" style={{ width: '10%' }} className={classes.tableHead}> </TableCell>   
                  </TableRow>
                </TableHead>
                <TableBody>
                {allStocks && allStocks.slice(pagesVisited, pagesVisited + picksPerPage).map((row) => (
                <TableRow  >
                      <TableCell style={{ width: '35%' }} className={classes.tableCell} align="left">{toTitleCase(row.guru_name)}</TableCell>
                      <TableCell style={{ width: '25%' }} className={classes.tableCell} align="left">{toTitleCase(row.stock_name)}</TableCell>
                      <TableCell style={{ width: '7%' }} className={classes.tableCell} align="left">{row.symbol}</TableCell>
                      <TableCell style={{ width: '8%' }} className={classes.tableCell} align="left">{formattedDate(row.last_qtr)}</TableCell>
                      <TableCell style={{ width: '15%' }} className={classes.tableCell} align="right">{row.value.toLocaleString('en-US')}</TableCell>
                      {/* <TableCell style={{ width: '15%' }} className={classes.tableCell} align="right">{toMillion(row.value)}</TableCell> */}
                      
                      <TableCell style={{width:'10%'}} align='center'>
                      <Tooltip title="Save" placement="right">
                        <Link variant="body" className={classes.wrapIcon}>
                            <SaveIcon className = {classes.svgIcon} onClick = { () => addToFavStocks(currentUser.id, row.security_id, row.filer_id) } />  
                        </Link>
                        </Tooltip>
                      </TableCell>
        
                  </TableRow>
                  ))}
              
              </TableBody>
              </Table>
          
              <Grid container
              direction='row'
              justify='flex-end'
              alignItems='center'>
              <Typography variant='h6' className={classes.pagination}>Showing 15 Filers </Typography>
              <ReactPaginate
              previousLabel={''}
              nextLabel ={''}
              pageCount = {pageCount}
              onPageChange={changePage}
              containerClassName={classes.pageContainer}
              previousLinkClassName={classes.prevBtn}
              nextLinkClassName={classes.nxtBtn}
              disabledClassName={classes.disable}
              activeClassName={classes.active}
              pageClassName={classes.page}
              
              />
              </Grid>
          </>
        }
      </div>
      
    );
  }
    