import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Link} from 'react-router-dom';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import { Tooltip } from '@material-ui/core';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import axios from 'axios';
import { BaseURL } from '../../consts';
import Spinner from '../../Spinner';
import { formattedDate, toTitleCase, errorHandler } from '../Utility/Utility';
import ReactPaginate from 'react-paginate';
import SaveIcon from '@material-ui/icons/Save';
import {useSetSnackbar} from "../../hooks/useSnackbar";
import DeleteForeverIcon from '@material-ui/icons//DeleteForever';
import authHeader from '../../services/auth-header';
import { Redirect, useHistory } from 'react-router-dom';


const useStyles = makeStyles((theme) =>({

head:{
  color:'#faed32',
  fontSize:'28px'

},
paper: {
  padding:'8px',
  
  
},
Header:{
  color:'#faed32',
  size: '24px'
},
tradeHead: {
  backgroundColor: "#f89b00",
  color: "black",
  padding:'0px 5px',
  fontSize:'24px',
  fontWeight:'bold',
  height:'50px'
},
svgIcon:{
  color:'#FF6347'
},
link:{
  color: '#fff'
},
tableCell:{
  padding:'0px 5px',
  fontSize:'22px'
},
breadCrumLink:{
  color: theme.palette.secondary.contrastText,
  textAlign: 'right',
  fontSize:'16px'
  
},
pageContainer:{
  
  listStyle:'none',
  display:'flex',
  justifyContent:'flex-end',
  borderRadius:'5px',
  color:'#fff',
  cursor:'pointer',
  fontSize:'20px',
  padding:'10px'
},
prevBtn:{
  padding:'10px'
},
nxtBtn:{
  padding:'10px'
},

active:{
  color:'#000',
  backgroundColor:'#fff'

},
page:{

  padding:'0px 10px',
  borderRight:'2px solid #fff',

}



}));


  

export default function FavStocks() {
    const classes = useStyles();
    const currentUser = JSON.parse(localStorage.getItem('user'))
    const [data, setData ] = React.useState([])
    const [loading, setLoading] = React.useState(true);
    const setSnackbar = useSetSnackbar();
    const history = useHistory()
    // const [pageNumber,setPageNumber] = React.useState(0);

    // const picksPerPage = 15;
    // const pagesVisited = pageNumber*picksPerPage;

    // const pageCount = Math.ceil( GuruExits.length/picksPerPage)

    // const changePage =({selected}) =>{
    //   setPageNumber(selected);

    // };

    React.useEffect(() => {
      axios.get(BaseURL+`/stock/fav_stock_list`,{
        params: {
          user_id: currentUser.id
        }, 
        headers: authHeader()
      })
      .then(response =>{
        console.log(response)
        if (response.status===200){ 
          setData(response.data);
          setLoading(false);
        }
      }).catch(error => {
        setLoading(false);
        error && error.response && errorHandler(setSnackbar, history, error.response.status)
      })
    }, []);

    const removeFavStock = (user_id,security_id,filer_id) => {
      try {
        axios({
            method: 'delete',
            url: BaseURL + '/remove_fav_stock',
            data: {
                stock: {
                  user_id: user_id,
                  security_id: security_id,
                  filer_id: filer_id               
                }
            }, 
            headers: authHeader()
        }).then(res => {
          setLoading(false);
          const remove_item = data.filter(i => i.security_id !== security_id)
          setData(remove_item);
          setSnackbar(res.data.message, "success");
        }).catch(error => {
          setLoading(false);
          error && error.response && errorHandler(setSnackbar, history, error.response.status)
        })
      } catch (e) {
        setLoading(false);
        setSnackbar("Some thing went Wrong", "error")
      }
    }

    return (
        <div >
          {
            loading ? <Spinner/> :
              <>
                  <Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumLink}>

                  <Link to ="/"  className={classes.link}>
                    {"Home"}
                  </Link>
                  <Link to="/guruexit"  className={classes.link}>
                    { "My Favorite Stocks "}
                  </Link>
                </Breadcrumbs>
            
                <Typography variant="h6" className={classes.head}gutterBottom >My Favorite Stocks</Typography>

                
                <Table  size="small" aria-label="a dense table">
                  <TableHead  >
                    <TableRow >
                    <TableCell align="left" style={{ width: '30%' }} className={classes.tradeHead}>Stock</TableCell>
                      <TableCell align="left" style={{ width: '15%' }} className={classes.tradeHead}>Ticker</TableCell>
                      <TableCell align="left" style={{ width: '25%' }} className={classes.tradeHead}> Guru Name </TableCell>
                      <TableCell align="left" style={{ width: '8%' }} className={classes.tradeHead}> Last Quarter</TableCell>
                      <TableCell align="right" style={{ width: '15%' }} className={classes.tradeHead}>Total #Shares Invested </TableCell>
                      <TableCell align="left" style={{ width: '7%' }} className={classes.tradeHead}> </TableCell>

                    </TableRow>
                  </TableHead>
                  <TableBody>
                  {data && data.map((row) => (
                  <TableRow  >
                        <TableCell className={classes.tableCell} style={{ width: '30%' }} align="left">{row.stock_name && toTitleCase(row.stock_name)}</TableCell>
                        <TableCell className={classes.tableCell} style={{ width: '15%' }} align="left">{row.symbol}</TableCell>
                        <TableCell className={classes.tableCell} style={{ width: '25%' }} align="left">{row.guru_name && toTitleCase(row.guru_name)}</TableCell>
                        <TableCell className={classes.tableCell} style={{ width: '8%' }} align="left">{row.last_quarter && formattedDate(row.last_quarter)}</TableCell>
                        <TableCell className={classes.tableCell} style={{ width: '15%' }} align="right">{row.shares_invested.toLocaleString('en-US')}</TableCell>
                        <TableCell className={classes.tableCell} style={{ width: '7%' }} align="center">
                          <Tooltip title="Remove favorite stock" placement="right">
                            <Link variant="body" className={classes.wrapIcon}>
                                <DeleteForeverIcon className = {classes.svgIcon} onClick = { () => removeFavStock(currentUser.id, row.security_id, row.filer_id) } />  
                            </Link>
                          </Tooltip>
                        </TableCell>
          
                    </TableRow>
                    ))}
                </TableBody>
                </Table>
                {/* <Grid container
                direction='row'
                justify='flex-end'
                alignItems='center'>
                <Typography variant='h6' className={classes.pagination}>Showing 15 Filers </Typography>
                <ReactPaginate
                previousLabel={''}
                nextLabel ={''}
                pageCount = {pageCount}
                onPageChange={changePage}
                containerClassName={classes.pageContainer}
                previousLinkClassName={classes.prevBtn}
                nextLinkClassName={classes.nxtBtn}
                disabledClassName={classes.disable}
                activeClassName={classes.active}
                pageClassName={classes.page}
                
                />
                </Grid> */}
              
              </>
          }
            

        </div>
      );
    }
    