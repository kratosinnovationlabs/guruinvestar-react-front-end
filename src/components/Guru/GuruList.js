import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Link} from 'react-router-dom';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import { Tooltip } from '@material-ui/core';

const useStyles = makeStyles(({

title:{
  color:'#faed32',

},
subtitle:{
  color:'#faed32',
},

Header:{
  margin:'10px',
  color:'#faed32',
  size: '24px'
},
type:{
  color:'#faed32',
  fontSize:'16px'
},



paper: {
  padding:'10px',
},

Header:{
  margin:'10px',
  color:'#faed32',
  size: '24px'
},
tradeHead: {
  backgroundColor: "#f89b00",
  color: "black",
  padding:'5px',
},
svgIcon:{
  color:'#faed32'
},
link:{
  color:'white'
},
tradeCell:{
  padding:'5px'
},
subType:{
  color:'#faed32',
  fontSize:'16px'
},
apply:{
  backgroundColor:'#faed32',
},
reset:{
  borderColor:'#ffffff',
  margin:'10px'

},
checkBox:{
  padding:'0px !important',}
}));

function createData(guruName, guruType,  issuer, ticker, ) {
  return {guruName, guruType, issuer, ticker, };
}
  const rows  = [
    { guruName: "Warren Buffet", guruType: "Investment Manager", issuer: "Microsoft Crop", ticker: "MSFT",   }, 
    { guruName: "Basant M", guruType: "Investment Manager", issuer: "Reliance", ticker: "REL",   }, 
    { guruName: "Warren Buffet", guruType: "Investment Manager", issuer: "Alphabet Crop", ticker: "ABC",   }, 
    { guruName: "Karl Icon", guruType: "Investment Manager", issuer: "Microsoft Crop", ticker: "MSFT",   }, 
    { guruName: "Satheesh Kumar P", guruType: "Investment Manager", issuer: "Twitter", ticker: "TA",   }, 

    
  ];


export default function GuruList() {
  const classes = useStyles();
  const [instituteType,setInstituteType] = React.useState("Institutional");

  const handleRadio = (event) => {
    setInstituteType(event.target.value);
  }

    return (
        <>
        <Typography variant="h5"className={classes.title} gutterBottom > List of Gurus </Typography>
      
        <div className={classes.root}>
        <Grid container>
          <Grid item xs={2}>
           <Typography variant='h6' className={classes.type} >Guru Type</Typography>  
          </Grid>
          <Grid item xs={10}>
            <FormControl component="fieldset">
              <RadioGroup row aria-label="position" name="institute" value={instituteType} onChange={handleRadio}  >
                <FormControlLabel value="Institutional" control={<Radio color="primary" />} label="Institutional"/>
                <FormControlLabel value="Non Institutional" control={<Radio color="primary" />} label="Non Institutional" />
              </RadioGroup>
            </FormControl>
          </Grid>
        </Grid> 

        { instituteType === "Institutional"  && 
          <Grid container>
            <Grid item xs={2}>
              <Typography variant='h6' className={classes.subType}>Guru Sub-Type</Typography>
            </Grid>
            <Grid item xs={4}>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Non Bank Financial Institutions [NBFC’s]" /><br/>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="REIT's [Real Estate Investment Trust]" /><br/>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="ETF [ Exchange Traded Funds]" /><br/>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Sovereign Funds" />
            </Grid>
            <Grid item xs={2}>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Insurance" /><br/>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Mutual Funds" /><br/>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Investment Manager" /><br/>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Pension Funds" />

                
            </Grid>
            <Grid item xs={2}>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Corporates" /><br/>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Individual Investor" /><br/>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Family Office" /><br/>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Others" />

            </Grid>
            <Grid item xs={2}>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Banks" /><br/>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Hedge Funds" /><br/>
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label=" Endowment Funds" />

            </Grid>
          </Grid> 
        }

        { instituteType === "Non Institutional" && 
          <Grid container>
            <Grid item xs={2}>
              <Typography variant='h6' className={classes.subType}>Guru Sub-Type</Typography>
            </Grid>
            <Grid item xs={10}
            spacing={2}
            direction='row'
            justify='flex-start'
            alignItems='center'
            >
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Individual Investor" />
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Hedge Funds" />
              <FormControlLabel className={classes.checkBox} control={<Checkbox name="checkedC" />} label="Investment Manager" />
              

          </Grid> 
          
        </Grid>  
                  
        }
        <Grid container>
        <Grid item xs={2}>
        </Grid>
        <Grid item xs={8}>
          <Button variant='contained'size='small' className={classes.apply}> Apply</Button>
          <Button variant='outlined'size='small' className={classes.reset}> Reset</Button>
        </Grid>
      </Grid>
    
    </div>

    <TableContainer component={Paper}>
                <Typography variant="h6" gutterBottom  align="left" className={classes.subtitle}>
                Results
                </Typography>
            <Table  size="small" aria-label="a dense table">
              <TableHead >
                <TableRow >
                  <TableCell align="left" style={{ width: '15%' }} className={classes.tradeHead}>Guru Name</TableCell>
                  <TableCell align="left" style={{ width: '15%' }} className={classes.tradeHead}>Guru Type</TableCell>
                  <TableCell align="left" style={{ width: '13%' }} className={classes.tradeHead}> Issuer Name</TableCell>
                  <TableCell align="left" style={{ width: '7%' }} className={classes.tradeHead}>Ticker</TableCell>
                  <TableCell align="left" style={{ width: '50%' }} className={classes.tradeHead}> </TableCell>

                 
  
                </TableRow>
              </TableHead>
              <TableBody>
              {rows.map((row) => (
               <TableRow  >
                    <TableCell className={classes.tradeCell} style={{ width: '15%' }} align="left">{row.guruName}</TableCell>
                    <TableCell className={classes.tradeCell} style={{ width: '15%' }} align="left">{row.guruType}</TableCell>
                    <TableCell className={classes.tradeCell} style={{ width: '13%' }} align="left">{row.issuer}</TableCell>
                    <TableCell className={classes.tradeCell} style={{ width: '7%' }} align="left">{row.ticker}</TableCell>
                    <TableCell style={{ width: '50%' }} align="left" >
                      <Tooltip title="Add to Favorite" placement="right">
                      <Link variant="body" className={classes.wrapIcon}>
                          <GroupAddIcon className = {classes.svgIcon}  />  
                      </Link>
                      </Tooltip >
                    </TableCell>
       
                </TableRow>
                ))}

            </TableBody>
            </Table>
          </TableContainer>
        </>
      );
    }
    