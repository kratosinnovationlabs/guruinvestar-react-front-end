import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { ThemeProvider } from '@material-ui/styles';
import GroupAddIcon from '@material-ui/icons/GroupAdd';



const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 450,
  },
  favStockTitle:{
    color: theme.palette.primary.main

  },
  tableHeadCell:{
    color: '#000000',
    width:'150px'
  },
  tableHead:{
    backgroundColor:'#f89b00',
   padding:'10px'
  },

  favStockHeader: {
      color: theme.palette.primary.main,
      padding: '10px',
      marginBottom: '35px'

  },
  svgIcon: {
    color: theme.palette.primary.main
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.primary
    
  },

  wrapIcon: {
    verticalAlign: 'middle',
    display: 'inline-flex'
  }
}));

function createData(guruName, guruType, lastModified, currentMv, portfolio,icon) {
  return { guruName, guruType, lastModified, currentMv, portfolio,icon };
}

const rows = [
  createData('Amit S Khandelwal', "8.1%", "12-April-2021", 1132,"20%","Follow and Save to my Favorite Gurus" ),
  createData('Amit S Khandelwal', "8.1%", "12-April-2021", 1132,"20%"),
  createData('Amit S Khandelwal', "8.1%", "12-April-2021", 1132,"20%"),
  createData('Amit S Khandelwal', "8.1%", "12-April-2021", 1132,"20%"),
  createData('Amit S Khandelwal', "8.1%", "12-April-2021", 1132,"20%"),
];

export default function OtherGurus() {
  const classes = useStyles();

  return (
      <>
    <Grid container spacing={3}>
    <Grid item xs={5}>
      <Typography variant="subtitle1" gutterBottom className={classes.favStockTitle}>
      View Other Gurus of your Favorite Stock
      </Typography>
    </Grid>
    <Grid item xs={7} >
        <Typography variant="subtitle2" gutterBottom align="right">
          {"Dashboard > My Favorite Guru > Trade Book >View other gurus of Fav Stock "}
        </Typography>
    </Grid> 
  </Grid>
  <Paper className={classes.paper}>
    <Grid container spacing={3}>
      <Grid item xs={12} >
        <Typography variant="subtitle2" gutterBottom className={classes.favStockHeader}>
          Your are viewing other Fund Managers/Gurus who have invested in your favourite stock Issuer Name
        </Typography>
      </Grid>
      </Grid>
    
      <TableContainer component={Paper}>
        <Table className={classes.table} size='small' aria-label="a dense table" >
          <TableHead className={classes.tableHead}>
            <TableRow >
              <TableCell colSpan={1} className={classes.tableHeadCell}>Guru Name</TableCell>
              <TableCell colSpan={1}className={classes.tableHeadCell} align="left">Guru Type</TableCell>
              <TableCell colSpan={2}className={classes.tableHeadCell} align="left">Last Modified</TableCell>
              <TableCell colSpan={2}className={classes.tableHeadCell} align="left">Current MV</TableCell>
              <TableCell colSpan={2}className={classes.tableHeadCell} align="left">% of Portfolio</TableCell>
              <TableCell colSpan={2}className={classes.tableHeadCell} align="left"> </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.name}>
                <TableCell colSpan={1} component="th" scope="row">
                  {row.guruName}
                </TableCell>
                <TableCell colSpan={1}align="center">{row.guruType}</TableCell>
                <TableCell colSpan={2}align="center">{row.lastModified}</TableCell>
                <TableCell colSpan={2} align="center">{row.currentMv}</TableCell>
                <TableCell colSpan={2} align="center">{row.portfolio}</TableCell>
                {/* <TableCell align="right"><GroupAddIcon className = {classes.svgIcon}/>{" Follow andSave to my Favorite Gurus"}</TableCell> */}
                <TableCell  colSpan={2} align="center">
                  <Typography variant="body" className={classes.wrapIcon}>
                      <GroupAddIcon className = {classes.svgIcon}  />  Follow and Save to my Favorite Gurus
                  </Typography>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
    </>
  );
}
