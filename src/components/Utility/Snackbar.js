import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { useSnackbar } from '../../hooks/useSnackbar';
import { green, pink, blue, amber } from '@material-ui/core/colors';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles(theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: pink[600],
  },
  info: {
    backgroundColor: blue[600],
  },
  warning: {
    backgroundColor: amber[700],
  },
  close: {
    padding: theme.spacing(0.5),
  }
}));

export default function SimpleSnackbar() {
  // const classes = useStyles();
  const { snackbar, setSnackbar } = useSnackbar();
 
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setSnackbar('');

  };
 
  const { message, type } = snackbar;

  return (
    <div>
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={!!message}
        autoHideDuration={3000}
        onClose={handleClose}
        >
          <Alert onClose={handleClose} severity={type}>
            { message ? <span>{message}</span> : null}
          </Alert>
      </Snackbar>
    </div>
  );
}
