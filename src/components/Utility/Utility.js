export const formattedDate = (d) => {
    if( d ){
      var date = new Date(d);
      var dd = date.getDate();
      var mm = date.getMonth()+1; 
      var yy = date.getFullYear().toString().substr(-2);
      if(dd<10) { dd='0'+dd; } 
      if(mm<10) { mm='0'+mm; } 

      return `${mm}/${dd}/${yy}`
    }else{
      return null
    }
  }

export const toTitleCase = (str) => {
  
  return str.replace(
    /\w\S*/g,
    function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
  );
}

export const getShortMonth = (index) => {
  const months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]

  return months[index]
}

export const apend = (value, symbol) => {
  
  var result = value+symbol
  return result
}

export const prepend = (value, symbol) => {
  
  var result = symbol+value
  return result
}

export const toMillion = (number) =>{
  var millions = (number * 1000)/1000000.0;
  // return parseFloat(millions).toFixed(2);
  return parseFloat(millions).toLocaleString('en-US', {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  });
}

export const errorHandler = (setSnackbar, history, status) =>{
  if (status === 400){
    setSnackbar("BAD REQUEST", "error")
  } else if (status === 401) {
    setSnackbar("UNAUTHORIZED", "error")
  } else if (status === 403) {
    history.push('/logout')
  } else if (status === 404) {
    setSnackbar("NOT FOUND", "error")
  } else if (status === 412) {
    setSnackbar("Resource has been denied", "error")
  } else if (status === 500) {
    setSnackbar("INTERNAL SERVER ERROR", "error")
  } else if (status === 522) {
    history.push('/logout')
  } else if (status === 422) {
    setSnackbar("Unable to update your request", "error")
  }
}
