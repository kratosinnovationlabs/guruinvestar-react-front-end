import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Height } from '@material-ui/icons';
import {Link} from 'react-router-dom';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';



const useStyles = makeStyles((theme) => ({
    header: {
        color: '#faed32',
        fontSize:"28px"
        
    },
    dashLine: {
      fontSize:'16px'
       
    },

    breadCrumLink:{
      color: theme.palette.secondary.contrastText,
      textAlign: 'right',
      fontSize:'14px'
      
    }

 
  }));



  export default function Container() {
    const classes = useStyles();
  
    return (
      <div>
        <Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumLink}>
          
          <Link to='dashboard' className={classes.breadCrumLink}>
            {"Home"}
          </Link>

          <Link className={classes.breadCrumLink}>
              {"GuruSearch"}
          </Link>
        </Breadcrumbs>
          
          <Typography variant="h5" gutterBottom className={classes.header}>
            Guru Search Screen
          </Typography>
         
         
         
          <Typography variant="body2" gutterBottom  className= {classes.dashLine}>
          Search your guru and add to favourites          </Typography>
      
           
       
        
      </div>
    );
  }
  
  
  