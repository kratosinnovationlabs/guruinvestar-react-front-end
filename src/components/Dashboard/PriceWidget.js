import React from "react";
import clsx from "clsx";
import { fade, makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import TinyAreaChart from './TinyAreaChart'
import Typography from "@material-ui/core/Typography";
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import Box from '@material-ui/core/Box';


const useStyles = makeStyles(theme => ({
    root: {
      display: "flex"
    },
    content: {
        flexGrow: 1,
        height: "100vh",
        overflow: "auto"
    },
   
    paper: {
        padding: theme.spacing(1),
        display: "flex",
    },
    fixedHeight: {
        height: 80,
        // "@media (min-width: 1920px)": {
        //   height: 80,
        // },
    },
    title:{
        color:"#00c4ff",
        fontSize:'18px',
        // "@media (min-width: 1920px)": {
        //   fontSize:'18px'
        // },
    },
    price:{
      fontSize:'18px',
      
    },
    priceOne:{
      fontSize:'18px',
      color:'green',
      // "@media (min-width: 1920px)": {
      //   fontSize:'18px'
      // },
    },
    priceTwo:{
      fontSize:'18px',
      color:'red',
      // "@media (min-width: 1920px)": {
      //   fontSize:'18px'
      // },
    },

    
  }));

export default function PriceWidget(props) {
  const classes = useStyles();
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  return (
    <Paper className={fixedHeightPaper}>
      <Grid container  direction="row" justify="center" alignItems="center" spacing={2}>
        <Grid item lg={5}  sm={6} xs={6}  >
        <Typography variant="body2" className={classes.title}gutterBottom align='right' >
          {props.title}
        </Typography>
        </Grid>
        <Grid item lg={7}  sm={6} xs={6} >
          <Typography variant="body2" className={classes.price} gutterBottom  align='left'  >
          {props.price}
          </Typography>

          {props.priceChange < 0? 
          <Typography variant="caption" className={classes.priceTwo} gutterBottom  align='left'  >
            { props.priceChange}
          </Typography>
            :
          <Typography variant="caption" className={classes.priceOne} gutterBottom  align='left'  >
          { props.priceChange}
        </Typography> }
        {props.priceChange < 0?   
          <Typography variant="caption" className={classes.priceTwo} gutterBottom  align='left'  >
              { props.percentChange} 
          </Typography>   
          :
          <Typography variant="caption" className={classes.priceOne} gutterBottom  align='left'  >
          { props.percentChange} 
         </Typography>  
            }
        </Grid> 
      </Grid>
    </Paper>
  );
}
