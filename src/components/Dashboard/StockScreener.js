import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import InputBase from '@material-ui/core/InputBase';
import Grid from "@material-ui/core/Grid";
import axios from 'axios';
import { BaseURL } from '../../consts';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {Link} from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import VisibilityIcon from '@material-ui/icons/Visibility';
import ControlPointIcon from '@material-ui/icons/ControlPoint';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import { useHistory } from 'react-router-dom';
import {useSetSnackbar} from "../../hooks/useSnackbar";
import SaveIcon from '@material-ui/icons/Save';
import { toMillion, errorHandler } from '../Utility/Utility';
import authHeader from '../../services/auth-header';
import Spinner from '../../Spinner';

const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 6,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    fontSize: 20,
    fontWeight:'bold',
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderRadius: 6,
      borderColor: '#707070',
      backgroundColor: theme.palette.background.paper,
    },
  menuItem:{
    backgroundColor:'#181815'
  }  
  
  },
}))(InputBase);

const useStyles = makeStyles((theme) => ({
  customTableContainer: {
    maxHeight:570,
    // "@media (min-width: 1920px)": {
    //   maxHeight:550,
    // }
  },
  table: {
    
    maxHeight:'500px',
    stickyHeader:'true'
  }  ,
  margin: {
    margin: theme.spacing(1),
  },
  svgIcon: {
    color: theme.palette.primary.main
  },
  cells:{
    color: '#ffc000'
  },
  table: {
    width:'100%',
    tableLayout: 'fixed'
  },
  tableHeadCell:{
    color: '#ffc000',
    fontSize: '24px',
    fontWeight:'bold'
  },
  tableCell:{
    fontSize:'22px',
    // padding: '5px !important'
  },
  paper: {
    padding: '8px',
    color: theme.palette.text.primary,
    marginTop: '20px'
    
  },
}));

export default function StockScreener() {
  const classes = useStyles();
  const [guru, setGuru] = React.useState({
    sectors: [],
    industries: [],
    guruTypes: [],
    guruNames: [],
    industry: '',
    sector: '',
    guruName: '',
    guruType: ''
  })
  const [data, setData] = React.useState([]);
  const history = useHistory();
  const [results, setResults ] = React.useState([]);
  const currentUser = JSON.parse(localStorage.getItem('user'))
  const setSnackbar = useSetSnackbar();
  const [loading, setLoading] = React.useState(true);


  function handleClick(ticker,cik) {
    if(ticker !== "" && cik !== "" ){
     history.push(`/tradebook/${cik}/${ticker}`)
    }
  }
  
  
  const handleSubmit = (user_id,filer_id,security_id) => {
    try {
      axios({
          method: 'post',
          url: BaseURL + '/guru/add_to_favourite_guru',
          data: {
              guru: {
                user_id: user_id,
                filer_id: filer_id,
                security_id: security_id
              }
          },
          headers: authHeader()
      }).then(res => {
        // setLoading(false);
        setSnackbar(res.data.message, "success");
      }).catch(error => {
        setLoading(false);
        error && error.response && errorHandler(setSnackbar, history, error.response.status)
      })
    } catch (e) {
      // setLoading(false);
      setSnackbar("Some thing went Wrong", "error")
    }
  }

  const addToFavStocks = (user_id,security_id,filer_id) => {
  
    try {
      axios({
          method: 'post',
          url: BaseURL + '/stock/add_to_favourite_stocks',
          data: {
              stock: {
                user_id: user_id,
                security_id: security_id,
                filer_id: filer_id               
              }
          },
          headers: authHeader()
      }).then(res => {
        // setLoading(false);
        setSnackbar(res.data.message, "success");
      }).catch(error => {
        // setLoading(false);
        error && error.response && errorHandler(setSnackbar, history, error.response.status)
      })
    } catch (e) {
      setLoading(false);
      setSnackbar("Some thing went Wrong", "error")
    }
  }


  const filterData = (guruType, guruName, sector, industry) => {
    
    let resultData = data;
    if( guruType !== ""){
      resultData = resultData.filter((row) => { return row.filer.guru_type == guruType } )
    }

    if ( guruName !== ""){
      resultData = resultData.filter((row) => { return row.filer.id == guruName } )
    }


    if ( sector !== ""){
      resultData = resultData.filter((row) => { return row.sector.id == sector } )
    }

    if ( industry !== ""){
      resultData = resultData.filter((row) => { return row.industry.id == industry } )
    }


    setResults(resultData)

  }
  
  const handleChangeGuruType = (event) => {
    setGuru({...guru, guruType: event.target.value});
    filterData(event.target.value, guru.guruName, guru.sector, guru.industry)
  };

  const handleChangeGuruName = (event) => {
    setGuru({...guru, guruName: event.target.value});
    filterData(guru.guruType, event.target.value, guru.sector, guru.industry)

  };

  const handleChangeSector = (event) => {
    setGuru({...guru, sector: event.target.value});
    filterData(guru.guruType, guru.guruName, event.target.value , guru.industry)
  };

  const handleChangeIndustry = (event) => {
    setGuru({...guru, industry: event.target.value});
    filterData(guru.guruType, guru.guruName, guru.sector,event.target.value)
  };

  const formattedDate = (d) => {
    if( d ){
      var date = new Date(d);
      var dd = date.getDate();
      var mm = date.getMonth()+1; 
      var yy = date.getFullYear().toString().substr(-2);
      if(dd<10) { dd='0'+dd; } 
      if(mm<10) { mm='0'+mm; } 

      return `${mm}/${dd}/${yy}`
    }else{
      return null
    }
  }
  const toTitleCase = (str) => {
    return str.replace(
      /\w\S*/g,
      function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }
    );
  }

  React.useEffect(() => {
    axios.get(BaseURL+'/guru_data',{
      headers: authHeader()
    })
    .then(response =>{
      console.log(response)
      if (response.status===200){ 
        const data = response.data;
        setGuru({...guru, sectors: data.sectors, industries: data.industries, guruNames: data.guru_names, guruTypes: data.guru_types  });
      }
    }).catch(error => {
      setLoading(false);
      error && error.response && errorHandler(setSnackbar, history, error.response.status)
    })
  }, []);

  React.useEffect(() => {
    axios.get(BaseURL+`/guru_stocks`,{
      headers: authHeader()
    })
    .then(response =>{
      console.log(response)
      if (response.status===200){ 
        setData(response.data);
        setResults(response.data)
        setLoading(false);
      }
    }).catch(error => {
      setLoading(false);
      error && error.response && errorHandler(setSnackbar, history, error.response.status)
    })
  }, []);
  return (
    <div>
      { loading ? <Spinner /> :
        <>
          <Grid container spacing={3}>
                <Grid item xs={6} sm={3}>
                    <FormControl  fullWidth={true}>
                        
                        <Select
                        labelId="guru-type-label"
                        id="guru-type"
                        value={guru.guruType}
                        displayEmpty
                        onChange={handleChangeGuruType}
                        input={<BootstrapInput />}
                        >
                        <MenuItem value="" >Guru Type</MenuItem>
                        { guru.guruTypes && guru.guruTypes.map((g) => (
                          <MenuItem value={g.guru_type}>{ g.guru_type }</MenuItem>
                        ))}
                        </Select>
                    </FormControl>
                    
                </Grid>
                <Grid item xs={6} sm={3}>
                    <FormControl  fullWidth={true}>
                        {/* <InputLabel id="demo-customized-select-label">Age</InputLabel> */}
                        <Select
                        labelId="guru-name-label"
                        id="guru-name"
                        value={guru.guruName}
                        displayEmpty
                        onChange={handleChangeGuruName}
                        input={<BootstrapInput />}
                        >
                        <MenuItem value="">
                            Guru Name
                        </MenuItem>
                        { guru.guruNames && guru.guruNames.map((g) => (
                          <MenuItem value={g.id}>{ g.name }</MenuItem>
                        ))}
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={6} sm={3}>
                    <FormControl  fullWidth={true}>
                        {/* <InputLabel id="demo-customized-select-label">Age</InputLabel> */}
                        <Select
                        labelId="demo-customized-select-label"
                        id="demo-customized-select"
                        value={guru.sector}
                        displayEmpty
                        onChange={handleChangeSector}
                        input={<BootstrapInput />}
                        
                        >
                        <MenuItem value="">
                            Sector
                        </MenuItem>
                        { guru.sectors && guru.sectors.map((s) => (
                          <MenuItem  value={s.id}>{ s.name }</MenuItem>
                        ))}
      
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={6} sm={3}>
                  <FormControl  fullWidth={true}>
                        {/* <InputLabel id="demo-customized-select-label">Age</InputLabel> */}
                        <Select
                        labelId="demo-customized-select-label"
                        id="demo-customized-select"
                        value={guru.industry}
                        displayEmpty
                        onChange={handleChangeIndustry}
                        input={<BootstrapInput />}
                        >
                        <MenuItem value="">
                          Industry
                        </MenuItem>
                        { guru.industries && guru.industries.filter((s) => s.sector_id==  guru.sector).map((i) => (
                          <MenuItem  value={i.id}>{ i.name }</MenuItem>
                        ))}
                        </Select>
                    </FormControl>
                </Grid>
          </Grid>
          <TableContainer classes={{root: classes.customTableContainer}}component={Paper}>
            <Table className={classes.table}  size='small' stickyHeader aria-label="sticky table">
              <TableHead className={classes.tableHead}>
                <TableRow >
                  <TableCell variant='head' style={{ width: '20%' }} className={classes.tableHeadCell}align="left">Guru Name</TableCell>
                  <TableCell variant='head' style={{ width: '12%' }} className={classes.tableHeadCell} align="left">Guru Type</TableCell>
                  <TableCell variant='head' style={{ width: '6%' }} className={classes.tableHeadCell} align="left">Quarter</TableCell>
                  <TableCell variant='head' style={{ width: '5%' }} className={classes.tableHeadCell} align="left">Ticker</TableCell>
                  <TableCell variant='head' style={{ width: '8%' }} className={classes.tableHeadCell} align="left">Sector</TableCell>
                  <TableCell variant='head' style={{ width: '12%' }} className={classes.tableHeadCell} align="left">Industry</TableCell>
                  <TableCell variant='head' style={{ width: '10%' }} className={classes.tableHeadCell} align="right">Value <Typography className={classes.dollar}>[in million] </Typography></TableCell>
                  <TableCell variant='head' style={{ width: '10%' }} className={classes.tableHeadCell} align="right">Shares</TableCell>
                  <TableCell variant='head' style={{ width: '9%' }} className={classes.tableHeadCell} align="right">Market Cap</TableCell>
                  <TableCell variant='head' style={{ width: '8%' }} className={classes.tableHeadCell} align="right"></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                { results && results.map((row) => (
                  <TableRow key={row.name}>
                    <TableCell align='left'  style={{ width: '20%' }}  className={classes.tableCell}>{ toTitleCase(row.filer.name) }</TableCell>
                    <TableCell align='left'  style={{ width: '12%' }}  className={classes.tableCell}>{row.filer.guru_type }</TableCell>
                    <TableCell align='left'  style={{ width: '6%' }} className={classes.tableCell}>{ formattedDate(row.last_qtr) }</TableCell>
                    <TableCell align='left'  style={{ width: '5%' }}  className={classes.tableCell}>{  row.symbol }</TableCell>
                    <TableCell align='left'  style={{ width: '8%' }}  className={classes.tableCell}>{  row.sector && row.sector.name }</TableCell>
                    <TableCell align='left'  style={{ width: '12%' }}  className={classes.tableCell}>{   row.industry && row.industry.name }</TableCell>
                    <TableCell align='right' style={{ width: '10%' }}   className={classes.tableCell}>{  toMillion(row.value) }</TableCell>
                    <TableCell align='right' style={{ width: '10%' }}   className={classes.tableCell}>{  row.shares.toLocaleString('en-US')}</TableCell>
                    <TableCell align='right' style={{ width: '9%' }}   className={classes.tableCell}>{  row.market_cap }</TableCell>
                    <TableCell style={{ display: 'flex', justifyContent: 'space-around', flexDirection: 'row'}} >

                      {/* <Tooltip title="View" placement="left" >
                        <Link variant="body" className={classes.wrapIcon} display={ ! ( row.last_qtr && row.last_qtr.stock ) }>
                          <VisibilityIcon className = {classes.svgIcon} onClick = { () => handleClick(row.symbol, row.filer.cik)} />  
                        </Link>
                      </Tooltip> */}
                      { row && row.stock_profile_id ?
                        <Tooltip title="Follow" placement="top">
                          <Link variant="body" className={classes.wrapIcon}>  
                            <SaveIcon className = {classes.svgIcon} onClick = { () => addToFavStocks(currentUser.id, row.security_id, row.filer_id) }/> 
                          </Link>
                        </Tooltip>
                        :
                        <Tooltip title="Follow" placement="top">
                          <Link variant="body" className={classes.wrapIcon} style={{ pointerEvents: 'none', color: '#ccc' }}>  
                            <SaveIcon className = {classes.svgIcon} style={{ color: '#ccc' }}/> 
                          </Link>
                        </Tooltip>
                      }
                      {/* <Tooltip title="Follow" placement="top">
                        <Link variant="body" className={classes.wrapIcon}>  
                          <SaveIcon className = {classes.svgIcon} onClick = { () => addToFavStocks(currentUser.id, row.filer.id) }/>  
                        </Link>
                      </Tooltip> */}
                      <Tooltip title="Add to favorite Guru" placement="top">
                        <Link variant="body" className={classes.wrapIcon}>
                          <GroupAddIcon className = {classes.svgIcon} onClick = { () => handleSubmit(currentUser.id, row.filer.id, row.security_id) } />  
                        </Link>
                      </Tooltip>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      }
    </div>
  );
}

