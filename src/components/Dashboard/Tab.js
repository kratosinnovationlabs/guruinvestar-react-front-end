import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import { Grid } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { Fragment } from 'react';
import {Link} from 'react-router-dom';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (

    
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  rootOne: {
    flexGrow: 1,
    backgroundColor: '#181815',
    display: 'flex',
    height: 224,
    margin: '25px 10px',
    borderRadius: '3px'
   
   
  },
  rootTwo: {
    flexGrow: 1,
    backgroundColor: '#181815',
    display: 'flex',
    height: 224,
    margin: '10px',
    borderRadius: '3px'
   
   
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
    width: '150px',
    fontSize:'12px'
    
  },

  tabPanels: {
    width:'90%',
    overflowY: 'auto',
    fontSize:'12px'
       
  },

  tabList: {
    textAlign:'left',
    fontSize:'12px',
    padding:'0px !important',
    paddingTop:'0px !important'
  },

  tabButton: {
   width: '10%',
   height: '25px',   
   color: "white",
   margin: '10px',
   borderRadius: '2px',
   borderColor:'#707070',
   textTransform:'none',
   backgroundColor:'#000000',
   fontSize:'15px'
  },

  tabTwo: {
    padding: '100px',
    textAlign: 'left',
   
  },
  listItem:{
    padding:'1px 8px'
  },
  saveLinks:{
    color:'#faed32',
    padding:'0px 10px'
  }

}));

export default function VerticalTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [showCriteria, setShowCriteria] = React.useState(false)
  const [criteria, setCriteria] = React.useState([{
    title: "% Ratios",
    options: ["Market Cap", "P/E Ratios", "P/B Value", "Dividend Yield (%)", "EV/EBIDTA", "EV/EBIT","EV/Sales"]
  },
  {
    title: "Fundamental",
    options: []
  }
  ])

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleClose = () =>{
    setShowCriteria(false);
  }
  return (
    <Grid container spacing={3}>
      <Grid xs={6}>
      <div>
      <Button variant='outlined' type='button' onClick={() => setShowCriteria( prevState =>  !prevState )} className={classes.tabButton}>
        Add Criteria
      </Button>
    </div>
      
        { showCriteria && 
        
          <div className={classes.rootOne}>
            <Tabs
              orientation="vertical"
              variant="scrollable"
              value={value}
              onChange={handleChange}
              aria-label="Vertical tabs example"
              className={classes.tabs}
              indicatorColor="primary"
              textColor="primary"
              
              centered
            >
              { 
                criteria.map( (crt, index)=>{
                  return  <Tab label={crt.title} {...a11yProps(index)} />
                })
              }
              
            </Tabs>
            { 
                criteria.map( (crt, index)=> ( 
                  <TabPanel value={value} index={index} className={classes.tabPanels}>
                    <List className={classes.tabList}>
                        { 
                          crt.options.map( (opt)=> ( 
                            <ListItem className={classes.listItem} >
                              <ListItemText  disableTypography='true' primary={opt}/>
                            </ListItem>
                          ))
                        }
                    </List>
                  </TabPanel>
                ))
            }
          </div>
        }
        
      </Grid>

      <Grid  xs={6}>
        <Button  variant='outlined' type="button" onClick={() => setShowCriteria( prevState =>  !prevState ) } className={classes.tabButton}>Edit Criteria</Button>
        { showCriteria && 
        <div>
        <Grid 
        container
        direction='row'
        justify='flex-end'
      >
          <Link onClick={handleClose} className={classes.saveLinks} variant='contained' size='small'>Save</Link>
          <Link onClick={handleClose} className={classes.saveLinks}  variant='contained' size='small'>Clear</Link>
          </Grid>
        <div className={classes.rootTwo}>
           
        <Tabs >
          

        <Typography variant="subtitle2" gutterBottom className={classes.tabTwo} >
          Refine Your Search using Filters
        </Typography>
         
        </Tabs>
         
          
        </div>
        </div>
      }
    </Grid>
  </Grid>
  );
}
