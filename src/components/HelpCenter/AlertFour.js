import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '../HelpCenter/Alert';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Link} from 'react-router-dom';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Tooltip from '@material-ui/core/Tooltip';



const useStyles = makeStyles((theme) => ({

  tableHead:{
    color:'#f89b00',
    fontSize:'14px',
    fontWeight:'bold',
  },
  tHead:{
    color:'#f89b00',
    fontSize:'14px',
    fontWeight:'bold',
    padding:'0px 8px'
  },
  link:{
    color:'#ffffff'
  },
  svgIcon:{
    color:'#fffc00'
  },
  button:{
    color:'#000000',
    fontSize:'10px',
    border:'0px'
  },
  tableCell:{
    fontSize:'12px',
  },
  tCell:{
    fontSize:'12px',
    padding:'0px 8px'
  }
}));


function createData(guruName, opening,  price,cmp) {
  return {guruName,  opening,  price,cmp};
}

const rows=[
{guruName:"Amit S Khandelwal", opening:"123.4",price:"30.56",cmp:"54.65"},
{guruName:"Amit S Khandelwal", opening:"123.4",price:"30.56",cmp:"54.65"},
{guruName:"Amit S Khandelwal", opening:"123.4",price:"30.56",cmp:"54.65"},
{guruName:"Amit S Khandelwal", opening:"123.4",price:"30.56",cmp:"54.65"},
{guruName:"Amit S Khandelwal", opening:"123.4",price:"30.56",cmp:"54.65"},
{guruName:"Amit S Khandelwal", opening:"123.4",price:"30.56",cmp:"54.65"},


];


export default function AlertFour() {
    const classes = useStyles();
  
  
    return (
    <div>
     
      <Alert  prev="/alert3" next="/alert5" head={"STOCK ALERT - New 13F Filing of your fav stock TA"}time={"Today, 11:00 AM IST"} message={"Your favorite stock has been picked by following fund managers at these price point for December 2020"}/>
      <Table size="small" aria-label="a dense table">
            <TableHead >
            <TableRow >
              <TableCell align="left" style={{width:'11%'}} className={classes.tableHead}>Guru Name</TableCell>
              <TableCell align="left" style={{width:'8%'}} className={classes.tHead}>Issuer Name</TableCell>
              <TableCell align="right" style={{width:'5%'}} className={classes.tHead}>Price</TableCell>           
              <TableCell align="right"style={{width:'6%'}}  className={classes.tHead}>CMP</TableCell>
              <TableCell align="right"style={{width:'70%'}}  className={classes.tHead}></TableCell>

            
            </TableRow>
            </TableHead>
            <TableBody>
        

              { rows.map((row) => (
                <TableRow >
                  <TableCell className={classes.tableCell}style={{width:'11%'}} align="left">{row.guruName}</TableCell>
                  <TableCell className={classes.tCell}style={{width:'8%'}} align="left">{row.opening}</TableCell>
                  <TableCell className={classes.tCell}style={{width:'5%'}} align="right">{row.price} </TableCell>
                  <TableCell className={classes.tCell}style={{width:'6%'}} align="right">{row.cmp} </TableCell>
                  <TableCell className={classes.tCell}className={classes.tableCell}align="left">
                    <Tooltip title="View" placement="right">
                      <Link variant="body"  className={classes.wrapIcon}>
                        <VisibilityIcon className = {classes.svgIcon}  />  
                      </Link>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
      </div>
    );
  }