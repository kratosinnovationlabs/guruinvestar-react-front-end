import React, {useEffect, useState} from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';

const options = [
  'Guru 1',
  'Guru 2',
  'Guru 3',
  'Guru 4',
  'Guru 5',
  'GUru 6 ',
  'Guru 7',
  'Guru 8',
  'Guru 9',
  'Guru 10',
  
];
const ITEM_HEIGHT = 48;

const useStyles = makeStyles((theme) => ({
    
      paper:{
        backgroundColor:'#fffff'
      },
      name:{
        color:'#faed32',
        fontSize:'12px'
      },
      checkbox:{
        color:'#ffffff',
        padding:'0px !important'
      },
      icon:{
        padding:'0px !important'
      },
      menuItem:{
        fontSize:"12px"
      }
  }));

  export default function GuruAlertMenu(props) {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
    const [checked, setChecked] = React.useState(true);

    const handleChange = (event) => {
      setChecked(event.target.checked);
    };
    
    return (
      <div>
        
           
        <Grid container>    
        <Grid item xs={4}> 
          <Typography className={classes.name}>{props.name}</Typography>
        </Grid>
        <Grid direction='row'
            justify='flex-start'
            alignContent='flex-start'>
        <Checkbox
          checked={checked}
          onChange={handleChange}
          inputProps={{ 'aria-label': 'primary checkbox' }}
          color='#ffffff'
          className={classes.checkbox}
          size='small'
        />
        <IconButton
          aria-label="more"
          aria-controls="long-menu"
          aria-haspopup="true"
          onClick={handleClick}
          className={classes.icon}
        >  
          <ArrowDropDownIcon />
        </IconButton>
        <Menu
          id="long-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open}
          onClose={handleClose}
          PaperProps={{
            style: {
              maxHeight: ITEM_HEIGHT * 4.5,
              width: '20ch',
            },
          }}
        >
          {options.map((option) => (
            <MenuItem className={classes.menuItem} key={option} selected={option === 'Pyxis'} onClick={handleClose}>
              {option}
            </MenuItem>
          ))}
        </Menu>
        </Grid> 
         
        </Grid>
        </div>
        );
        }
