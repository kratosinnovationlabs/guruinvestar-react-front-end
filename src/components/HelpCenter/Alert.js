import React, {useEffect, useState} from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import {Link} from 'react-router-dom';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core/styles';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';




const useStyles = makeStyles((theme) => ({
  paper:{
    padding:'10px'
  },
  alert:{
    backgroundColor:'#f89b00',
    color:'#000000',
    padding:'0px 2px'  },
  head:{
    color:'#faed32'
  },
  subHead:{
    paddingLeft:'50px'
  },
  tHead:{
    color:'#f89b00',
    fontSize:'14px',
    fontWeight:'bold'
  },
  link:{
    color:'#ffffff'
  },
  svgIcon:{
    color:'#fffc00'
  },
  button:{
    color:'#000000',
    fontSize:'10px',
    border:'0px'
  },
  tableCell:{
    fontSize:'12px'
  },
  breadCrumLink:{
    color: theme.palette.secondary.contrastText,
    textAlign: 'right',
    
  },
  received:{
    margin:'10px 0px'
  }
}));


export default function Alert(props) {
    const classes = useStyles();
    return (
        <div>
       
          <Grid container
          direction='row'
          justify='space-between'
          alignItems='center' >
            <Typography variant='h6' gutterBottom className={classes.head} >Notifications</Typography>
              <Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumLink}>

                  <Link  className={classes.link}>
                    {"Inbox"}
                  </Link>
                
                  <Link  className={classes.link}>
                  {"Guru Alert"}
                  </Link>
              </Breadcrumbs>
          </Grid>
          <Grid container 
          direction="row"
          justify="space-between"
          alignItems="center"
          className={classes.alert}>         
            <Typography >  {props.head}</Typography>
                  
            <ButtonGroup  >
              <Link  to={props.prev}>
              <Button className={classes.button} ><NavigateBeforeIcon/> Prev</Button>
              </Link>
              <Link  to={props.next}>              
              <Button className={classes.button} >Next<NavigateNextIcon/></Button>
              </Link>

            </ButtonGroup>
            
          </Grid>
          
       
          <Grid container
           className={classes.received}
           direction="row"
           justify="flex-start"
           alignItems="center"
           
           >

            <Typography> From: help@guruinvestars.com </Typography>
            <Typography className={classes.subHead}> Received: {props.time} </Typography>
          </Grid>

        <Typography>{props.message}</Typography>


   </div>
  );
}
