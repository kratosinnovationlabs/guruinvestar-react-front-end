import React, {useEffect, useState} from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import GuruAlertMenu from './GuruAlertMenu';


const useStyles = makeStyles((theme) => ({
    tab: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        
      },
      heading:{
        fontSize:'16px'
      },
      paper:{
        borderRadius:'0px',
        padding:'10px'
      },
      switch:{
        color:'#faed32'
      }
  }));

  export default function GuruAlert() {
    const classes = useStyles();
    const [value, setValue] = React.useState('1');
    const [state, setState] = React.useState({
      checkedA: true,});
    const [checked, setChecked] = React.useState(true);
    
  
  
    const handleChange = (event, newValue) => {
      setValue(newValue);
    };
    const toggleChecked = () => {
      setChecked((prev) => !prev);
    };
  
  return (
      <>
  
      <div className={classes.root}>
        <Typography variant='h6' className={classes.heading}> Stock Price Decline Change Alert</Typography><br/>
        <Grid container>
          <Grid item xs={3}>
          <GuruAlertMenu name={'Twitter'}/>
        <GuruAlertMenu name={'ABC Group'}/>
        <GuruAlertMenu name={'Bekshire H'}/>

          </Grid>
          <Grid item xs={4}>
          <GuruAlertMenu name={'Warren Buffet'}/>
        <GuruAlertMenu name={'Amit S Khala'}/>
        <GuruAlertMenu name={'Karl Icon '}/>

          </Grid>
          <Grid item xs={3}
          container
          direction="row"
            justify="flex-start"
            alignItems="center"
          >
          <Typography>When my selected fav guru-stock price has declined from the price bought by my Favorite Guru</Typography>
          </Grid>
          <Grid item xs={2}
          container
          direction="row"
          justify="center"
          alignItems="center">
          <FormControlLabel
            control={<Switch className={classes.switch} checked={checked} onChange={toggleChecked} />}
           
            
          />
            
            
          </Grid>
        </Grid>
      
    
      
             
          </div>
      </>
    );
  }