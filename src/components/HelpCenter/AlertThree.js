import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '../HelpCenter/Alert';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Link} from 'react-router-dom';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Tooltip from '@material-ui/core/Tooltip';


const useStyles = makeStyles((theme) => ({
  tableHead:{
    color:'#f89b00',
    fontSize:'14px',
    fontWeight:'bold',
  },
  tHead:{
    color:'#f89b00',
    fontSize:'14px',
    fontWeight:'bold',
    padding:'0px 8px'
  },
  link:{
    color:'#ffffff'
  },
  svgIcon:{
    color:'#fffc00'
  },
  button:{
    color:'#000000',
    fontSize:'10px',
    border:'0px'
  },
  tableCell:{
    fontSize:'12px',
  },
  tCell:{
    fontSize:'12px',
    padding:'0px 8px'
  },
}));

function createData(guruName, issuerName,  prev, latest,) {
  return {guruName,  issuerName,  prev, latest,};
}

const rows=[
{guruName:"Amit S Khandelwal", issuerName:"Sample Name",prev:"Q1 2020",latest:"FY 2020"},
{guruName:"Amit S Khandelwal", issuerName:"Sample Name",prev:"Q4 2020",latest:"Q1"},
{guruName:"Amit S Khandelwal", issuerName:"Sample Name",prev:"Q4 2020",latest:"Q1"},
{guruName:"Amit S Khandelwal", issuerName:"Sample Name",prev:"Q1 2020",latest:"FY 2020"}


];

export default function AlertThree() {
    const classes = useStyles();
   
    
  
    return (
      <div>
     
      <Alert  prev="/alert2" next="/alert4" head={"GURU ALERT - Change in Position of stock TA by  your Favorite guru"}time={"Today, 10:00 AM IST"} message={"Your fav guru for the quarter ended December 2020 has made a major change for the following stocks -"}/>
      <Table size="small" aria-label="a dense table">
            <TableHead >
            <TableRow >
              <TableCell align="left" style={{width:'11%'}} className={classes.tableHead}>Guru Name</TableCell>
              <TableCell align="left" style={{width:'8%'}} className={classes.tHead}>Issuer Name</TableCell>
              <TableCell align="left" style={{width:'5%'}} className={classes.tHead}>Prev Quarter Position</TableCell>           
              <TableCell align="left"style={{width:'6%'}}  className={classes.tHead}>Latest Quarter Position</TableCell>
              <TableCell align="right"style={{width:'70%'}}  className={classes.tHead}></TableCell>

            
            </TableRow>
            </TableHead>
            <TableBody>
        

              { rows.map((row) => (
                <TableRow >
                  <TableCell className={classes.tableCell}style={{width:'11%'}} align="left">{row.guruName}</TableCell>
                  <TableCell className={classes.tCell}style={{width:'8%'}} align="left">{row.issuerName}</TableCell>
                  <TableCell className={classes.tCell}style={{width:'5%'}} align="left">{row.prev} </TableCell>
                  <TableCell className={classes.tCell}style={{width:'6%'}} align="left">{row.latest} </TableCell>
                  <TableCell className={classes.tCell}className={classes.tableCell}align="left">
                    <Tooltip title="View" placement="right">
                      <Link variant="body"  className={classes.wrapIcon}>
                        <VisibilityIcon className = {classes.svgIcon}  />  
                      </Link>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>

      </div>
    );
  }