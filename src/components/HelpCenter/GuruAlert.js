import React, {useEffect, useState} from 'react';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from '@material-ui/core/styles';
import TabContext from '@material-ui/lab/TabContext';
import TabList from '@material-ui/lab/TabList';
import TabPanel from '@material-ui/lab/TabPanel';
import Switch from '@material-ui/core/Switch';
import AppBar from '@material-ui/core/AppBar';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import GuruAlertMenu from './GuruAlertMenu';



const useStyles = makeStyles((theme) => ({
  tab: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
      
    },
    startTab:{
      padding:'0px'
    },
    endTabs:{
      padding:'0px 30px'
    },
    paper:{
      borderRadius:'0px'
    },
    switch:{
      color:'#faed32'
    },
    description:{
      fontSize:'14px'
    }
}));


export default function GuruAlert() {
  const classes = useStyles();
  const [value, setValue] = React.useState('1');
  const [state, setState] = React.useState({
    checkedA: true,});
  const [checked, setChecked] = React.useState(true);
  


  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const toggleChecked = () => {
    setChecked((prev) => !prev);
    
  };

return (
    <>

    <div className={classes.root}>
          <TabContext value={value}>
              <TabList  className={classes.tab} onChange={handleChange} aria-label="simple tabs example">
                <Tab className={classes.startTabs}  label="New Holding Alert" value="1" />
                <Tab className={classes.endTabs} label="New Complete Exit Alert" value="2" />
                <Tab className={classes.endTabs} label="Major Change in Position Alert" value="3" />

              </TabList>
              <br/>
            <TabPanel value="1">
             
              <Grid container>
                <Grid item xs={4}>
                <GuruAlertMenu name={'Waran buffet'}/>
                <GuruAlertMenu name={'Karl Icon'}/>
                <GuruAlertMenu name={'Amith S Khala'}/>

                </Grid>
                <Grid item xs={4}
                container
                direction="row"
                 justify="center"
                 alignItems="center"
               >
                  <Typography className={classes.description}> When My Favorite gurus holds new position, alert me</Typography>
                </Grid>
                <Grid item xs={2}
                container
                direction="row"
                justify="center"
                alignItems="center">
                <FormControlLabel
                  control={<Switch  checked={checked} onChange={toggleChecked} />}
                  
                />
                 
                 
                </Grid>
              </Grid>
            
            </TabPanel>
            <TabPanel value="2">
            <Grid container>
                <Grid item xs={4}>
                <GuruAlertMenu name={'Waran buffet'}/>
              <GuruAlertMenu name={'Karl Icon'}/>
              <GuruAlertMenu name={'Amith S Khala'}/>
                </Grid>
                <Grid item xs={4}
                container
                   direction="row"
                   justify="center"
                   alignItems="center"
                 >
                  <Typography> When my selected favorite gurus exits from a stock, alert me</Typography>
                </Grid>
                <Grid item xs={2} container
                  direction="row"
                  justify="center"
                  alignItems="center"
                >
                <FormControlLabel
                  control={<Switch checked={checked} onChange={toggleChecked} />}
                  
                />
                 
                </Grid>
              </Grid>

            </TabPanel>
            <TabPanel value="3">
            <Grid container>
                <Grid item xs={4}>
                <GuruAlertMenu name={'Waran buffet'}/>
              <GuruAlertMenu name={'Karl Icon'}/>
              <GuruAlertMenu name={'Amith S Khala'}/>
                </Grid>
                <Grid item xs={4}
                container
                   direction="row"
                   justify="center"
                   alignItems="center"
                 >
                  <Typography> When my selected favorite gurus have made a major change in stocks</Typography>
                </Grid>
                <Grid item xs={2}
                container
                direction="row"
                justify="center"
                alignItems="center" >
                <FormControlLabel
                  control={<Switch className={classes.switch}checked={checked} onChange={toggleChecked} />}
                  label="ENABLED"
                />
                 
                 
                </Grid>
              </Grid>

            </TabPanel>

          </TabContext>
        </div>
    </>
  );
}