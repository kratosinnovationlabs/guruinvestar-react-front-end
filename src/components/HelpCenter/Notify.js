import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from "@material-ui/core/Grid";
import { useHistory } from 'react-router-dom';

import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Popper from '@material-ui/core/Popper';
import MenuList from '@material-ui/core/MenuList';
import Typography from '@material-ui/core/Typography';

function createData(id, name, description, time, status ) {
  return { id, name, description, time, status };
}

const rowsData = [
  createData(1,'Guru Alert', 'New 13F Filing of your fav stock ' ,'11:00 AM',"Unread"),
  createData(2,'Guru Alert','Your fav guru holding new position ','10:45 AM', "Read" ),
  createData(3,'Guru Alert', 'Your fav guru holding new position ','10:45 AM', "Read"),
  createData(4,'Guru Alert ','Your fav guru holding new position ','10:30 AM', "Unread"),
  createData(5,'Price Change  Alert','Price Change Alert of your fav guru Amit Kalra for your fav stock TA','10:20 AM',"Unread"),
  createData(6,'Guru Alert', 'Your fav guru holding new position ','10:15 AM',"Unread"),
  createData(7,'Exit Alert', 'Your fav guru exited from the stock ','10:10 AM', "Unread"),
  createData(8,'Guru Position Alert', 'Change in Position of stock TA by  your Fav guru','10:00 AM', "Unread"),
  createData(9,'Exit Alert','Your fav guru exited from the stock ','May 27',"Read"),
  createData(10,'Guru Position Alert','Change in Position of stock TA by  your Fav guru','May 27', "Unread"),
  createData(11,'Exit Alert', 'Your fav guru exited from the stock ','May 27',"Read"),
  createData(12,'Exit Alert','Your fav guru exited from the stock ','May 27',"Read"),
  createData(13,'Guru Alert', 'Your fav guru holding new position ','May 27',"Read"),
  createData(14,'Filling Alert', 'New 13F Filing of your fav stock ','May 27',"Read"),
  createData(15,'Filling Alert','New 13F Filing of your fav stock ','May 25',"Read"),
  createData(16,'Filling Alert','New 13F Filing of your fav stock ','May 26',"Read"),
  createData(17,'Guru Alert', 'Your fav guru exited from the stock ','May 27',"Read"),

];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  { id: 'name',  disablePadding: false, label: '' },
  { id: 'description',  disablePadding: false, label: '' },
  { id: 'time', disablePadding: false, label: '' },
  { id: 'status', disablePadding: false, label: '' },
 
];
const ITEM_HEIGHT = 48;
function EnhancedTableHead(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleSelect = (event) => {
    
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {

    setAnchorEl(null);
  };
  const handleClickMenuItem = (event) => {
    if( event.target.textContent === "All" ){
      props.setRows(["Read","Unread"])
      let newRows = rowsData.filter((r) => ["Read","Unread"].includes(r.status) )
      props.setRows(newRows)
    }else if(event.target.textContent === "None"){
      props.setStatus([])
      // props.setRows([])
    }else{
      let statusAry = Array(event.target.textContent)
      props.setStatus(statusAry)
      let newRows = rowsData.filter((r) => statusAry.includes(r.status) )
      props.setRows(newRows)
    }
    
    setAnchorEl(null);
  };

  const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };
  const options = [
    'All',
    'Read',
    'Unread'    
  ];

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Grid container>
            <Grid item xs={6}>
              <Checkbox
                indeterminate={numSelected > 0 && numSelected < rowCount}
                checked={rowCount > 0 && numSelected === rowCount}
                onChange={onSelectAllClick}
                inputProps={{ 'aria-label': 'select all desserts' }}
                className={classes.checkBoxOne}
              />
            </Grid>
            <Grid item xs={6}>
              <IconButton
                aria-label="more"
                aria-controls="long-menu"
                aria-haspopup="true"
                onClick={handleSelect}
              >  
                <ArrowDropDownIcon />
              </IconButton>
                <Menu
                  id="long-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={open}
                  onClose={handleClose}
                  PaperProps={{
                    style: {
                      maxHeight: ITEM_HEIGHT * 4.5,
                      width: '20ch',
                      
                    },
                  }}
                >
                  {options.map((option) => (
                    <MenuItem key={option} selected={option === 'Pyxis'} onClick={handleClickMenuItem}>
                      {option}
                    </MenuItem>
                  ))}
                </Menu>
              </Grid>
            
          </Grid>
        </TableCell>
        {/* {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))} */}

        
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
    minHeight: '32px !important',
  },

  highlight: {
    backgroundColor: '#f89b00',
    minHeight: '48px !important'
  },

  btn: {
    fontSize: '12px',
    color: '#000000'
  },
    
  title: {
    flex: '1 1 100%',
  },
  iconButton:{
    padding: '0px !important'
  },
  checkBox:{
    padding:'3px !important',
    color:'#ffffff'
  }
}));

const EnhancedTableToolbar = (props) => {
  const classes = useToolbarStyles();
  const { selected, rows, setRows, handleRequestDescSort, handleRequestAscSort, numSelected, rowCount, onSelectAllClick, setStatus} = props;
  console.log(rows, selected)
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);
  // -------------------------------------------------------
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open1 = Boolean(anchorEl);

  const handleSelectMenu = (event) => {
    
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenu = () => {

    setAnchorEl(null);
  };
  const handleClickMenuItem = (event) => {
    if( event.target.textContent === "All" ){
      props.setRows(["Read","Unread"])
      let newRows = rowsData.filter((r) => ["Read","Unread"].includes(r.status) )
      props.setRows(newRows)
    }else if(event.target.textContent === "None"){
      props.setStatus([])
      // props.setRows([])
    }else{
      let statusAry = Array(event.target.textContent)
      props.setStatus(statusAry)
      let newRows = rowsData.filter((r) => statusAry.includes(r.status) )
      props.setRows(newRows)
    }
    
    setAnchorEl(null);
  };

  // const createSortHandler = (property) => (event) => {
  //   onRequestSort(event, property);
  // };
  const options = [
    'All',
    'Read',
    'Unread'    
  ];
  // ---------------------------------------------

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  const handleMarkAsRead = () =>{
    let newRows = rows.map( r => selected.includes( r.id ) ? { ...r, status: "Read" } : r )
    setRows(newRows)
  }
  const handleMarkAsUnread = () => {
    let newRows = rows.map( r => selected.includes( r.id ) ? { ...r, status: "Unread" } : r )
    setRows(newRows)

  }

  const handleSort = (event) =>{
    debugger;
    if(event.target.textContent == "Latest to Oldest"){
      handleRequestAscSort(event, "time")
    }else{
      handleRequestDescSort(event, "time")
    }
    
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  }

  return (
    // <Toolbar
    //   className={clsx(classes.root, {
    //     [classes.highlight]: selected.length > 0,
    //   })}
    // >
    //   { selected.length > 0 ? (
    //     <Grid container direction="row" justify="flex-end" alignItems="center">
    //       <Grid item>
    //         <Button
    //           ref={anchorRef}
    //           aria-controls={open ? 'menu-list-grow' : undefined}
    //           aria-haspopup="true"
    //           onClick={handleToggle}
    //           className={classes.btn}
    //           endIcon={<ArrowDropDownIcon/>}
    //         >
    //           Sort By
    //         </Button>
    //         <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
    //           {({ TransitionProps, placement }) => (
    //             <Grow
    //               {...TransitionProps}
    //               style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
    //             >
    //               <Paper>
    //                 <ClickAwayListener onClickAway={handleClose}>
    //                   <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
    //                     <MenuItem onClick={handleSort}>Latest to Oldest</MenuItem>
    //                     <MenuItem onClick={handleSort}>Oldest to Latest</MenuItem>
    //                   </MenuList>
    //                 </ClickAwayListener>
    //               </Paper>
    //             </Grow>
    //           )}
    //         </Popper>
    //       </Grid>
    //       <Grid item>
    //         <Button className={classes.btn} onClick={handleMarkAsRead}>Mark as Read</Button>
    //       </Grid>
    //       <Grid item>
    //         <Button className={classes.btn} onClick={handleMarkAsUnread}>Mark as Unread</Button>
    //       </Grid>
    //     </Grid>
        
    //   ) : ''}
    // </Toolbar>

    <Toolbar
    className={classes.highlight}
    >
        <Grid container 
        alignItems='center'>
            <Grid item >
              <Checkbox
                indeterminate={numSelected > 0 && numSelected < rowCount}
                checked={rowCount > 0 && numSelected === rowCount}
                onChange={onSelectAllClick}
                inputProps={{ 'aria-label': 'select all desserts' }}
                className={classes.checkBox}
                color='#fff'
              />
            </Grid>
            <Grid item>
              <IconButton
                aria-label="more"
                aria-controls="long-menu"
                aria-haspopup="true"
                onClick={handleSelectMenu}
                className={classes.iconButton}
              >  
                <ArrowDropDownIcon />
              </IconButton>
                <Menu
                  id="long-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={open1}
                  onClose={handleCloseMenu}
                  PaperProps={{
                    style: {
                      maxHeight: ITEM_HEIGHT * 4.5,
                      width: '20ch',
                      
                    },
                  }}
                >
                  {options.map((option) => (
                    <MenuItem key={option} selected={option === 'Pyxis'} onClick={handleClickMenuItem}>
                      {option}
                    </MenuItem>
                  ))}
                </Menu>
              </Grid>
              {/* ------------- */}
              <Grid item>
                <Button
                  ref={anchorRef}
                  aria-controls={open ? 'menu-list-grow' : undefined}
                  aria-haspopup="true"
                  onClick={handleToggle}
                  className={classes.btn}
                  endIcon={<ArrowDropDownIcon/>}
                >
                  Sort By
                </Button>
                <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                  {({ TransitionProps, placement }) => (
                    <Grow
                      {...TransitionProps}
                      style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                    >
                      <Paper>
                        <ClickAwayListener onClickAway={handleClose}>
                          <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                            <MenuItem onClick={handleSort}>Latest to Oldest</MenuItem>
                            <MenuItem onClick={handleSort}>Oldest to Latest</MenuItem>
                          </MenuList>
                        </ClickAwayListener>
                      </Paper>
                    </Grow>
                  )}
                </Popper>
              </Grid>
              
              { selected.length > 0 ? (
                <>
                  <Grid item>
                    <Button className={classes.btn} onClick={handleMarkAsRead}>Mark as Read</Button>
                  </Grid>
                  <Grid item>
                    <Button className={classes.btn} onClick={handleMarkAsUnread}>Mark as Unread</Button>
                  </Grid>
                  </>
              ) : ''}
          </Grid>
        {/* ---------------------------- */}
        {/* <Grid container direction="row" justify="flex-end" alignItems="center">
          <Grid item>
            <Button
              ref={anchorRef}
              aria-controls={open ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={handleToggle}
              className={classes.btn}
              endIcon={<ArrowDropDownIcon/>}
            >
              Sort By
            </Button>
            <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
              {({ TransitionProps, placement }) => (
                <Grow
                  {...TransitionProps}
                  style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                >
                  <Paper>
                    <ClickAwayListener onClickAway={handleClose}>
                      <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                        <MenuItem onClick={handleSort}>Latest to Oldest</MenuItem>
                        <MenuItem onClick={handleSort}>Oldest to Latest</MenuItem>
                      </MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </Grid>
          
        { selected.length > 0 ? (
          <>
            <Grid item>
              <Button className={classes.btn} onClick={handleMarkAsRead}>Mark as Read</Button>
            </Grid>
            <Grid item>
              <Button className={classes.btn} onClick={handleMarkAsUnread}>Mark as Unread</Button>
            </Grid>
            </>
        ) : ''}
        
        </Grid> */}
        
      
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },

  
  unread: {
    '& > td': {
      color: '#f5f6fa ! important'
    }
  },

  read: {
    '& > td': {
      color: '#707070 ! important'
    }
  },
  heading:{
    color:'#faed32'
  },
  checkBox:{
    padding:'3px !important',
    color:'#fff'
  },
  pagination:{
  
  }
}));

export default function Notify() {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(25);
  const [status, setStatus] = React.useState(["Read", "Unread"]);
  const [rows, setRows] = React.useState(rowsData)

  const history = useHistory();
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleRequestAscSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder('asc');
    setOrderBy(property);
  };

  const handleRequestDescSort = (event, property) => {
    const isAsc = orderBy === property && order === 'desc';
    setOrder('desc');
    setOrderBy(property);
  };


  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.id);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, id) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };

  const isSelected = (id) => selected.indexOf(id) !== -1;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  function handleClickRedirect() {
    history.push(`/alert1`)
 }
  return (
    <div className={classes.root}>
      <Grid container
      direction='row'
      justify='space-between'
      alignItems='center'>
      <Typography variant='h5' className={classes.heading}> Notifications</Typography><br/>
        <TablePagination
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
          className={classes.pagination}
        />
        </Grid>
        <EnhancedTableToolbar selected={selected} setRows={setRows} rows={rows} handleRequestAscSort = {handleRequestAscSort} handleRequestDescSort = {handleRequestDescSort} numSelected={selected.length} onSelectAllClick={handleSelectAllClick} rowCount={rows.length} setStatus={setStatus} setRows={setRows}/>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
            aria-label="enhanced table"
            label="Dense padding"
          >
            {/* <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
              setStatus={setStatus}
              setRows={setRows}
              rows={rows}

            /> */}
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.id);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      // onClick={(event) => handleClick(event, row.id)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      selected={isItemSelected}
                      key={row.id}
                      className={row.status === "Read" ? classes.read : classes.unread }
                      //onClick={() => handleClickRedirect()}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ 'aria-labelledby': labelId }}
                          onChange={(event) => handleClick(event, row.id)
                          }
                          className={classes.checkBox}
                          color='#fff'
                        />
                      </TableCell>
                      <TableCell id={labelId} scope="row" padding="none" onClick={() => handleClickRedirect()}>{row.name}</TableCell>
                      <TableCell align="left" onClick={() => handleClickRedirect()}>{row.description}</TableCell>
                      <TableCell align="left" onClick={() => handleClickRedirect()}>{row.time}</TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        
    
    </div>
  );
}
