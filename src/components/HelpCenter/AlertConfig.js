import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { lighten, makeStyles } from '@material-ui/core/styles';
import GuruAlert from './GuruAlert';
import StockAlert from'./StockAlert';
import StockGuruAlert from './StockGuruAlert'
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';



const useStyles = makeStyles((theme) => ({

  heading:{
    color:'#faed32'
  },
  subtitle:{
    color:'#faed32'
  },
  
  Button:{
    textTransform:'none'
  },
  radio:{
    padding:'0px 20px',
    fontSize:'14px',
  
  }
 

}));


export default function AlertConfig() {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef();
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const [alertLevel,setAlertLevel] = React.useState("Institutional");

  const handleRadio = (event) => {
    setAlertLevel(event.target.value);
  }

  // const handleClick = () => {
  //   console.info(`You clicked ${options[selectedIndex]}`);
  // };

  // const handleMenuItemClick = (event, index) => {
  //   setSelectedIndex(index);
  //   setOpen(false);
  // };

  // const handleToggle = () => {
  //   setOpen((prevOpen) => !prevOpen);
  // };

  // const handleClose = (event) => {
  //   if (anchorRef.current && anchorRef.current.contains(event.target)) {
  //     return;
  //   }

  //   setOpen(false);
  // };

  return (
    <>
    <Typography variant='h6' className={classes.heading}> Configure Alerts</Typography>
      <Grid container 
      direction='row'
      justify='flex-start'
      alignItems='center'>
       
      <Typography variant='body1' className={classes.subtitle}>Select a alert level to configure</Typography>
        <Grid className={classes.radio}>
            <FormControl component="fieldset">
              <RadioGroup row aria-label="position" name="institute" value={alertLevel} onChange={handleRadio}  >
                <FormControlLabel value="Guru Level Alert" control={<Radio color="primary" />} label="Guru Level Alert"/>
                <FormControlLabel value="Stock Level Alert" control={<Radio color="primary" />} label="Stock Level Alert" />
                <FormControlLabel value="Guru and Stock Level Alert" control={<Radio color="primary" />} label="Guru and Stock Level Alert" />

              </RadioGroup>
            </FormControl>
          </Grid>
        </Grid>
      { alertLevel === 'Guru Level Alert' &&
      <GuruAlert/>
      }
      { alertLevel === 'Stock Level Alert' &&
      <StockAlert/>
      }   
      { alertLevel === 'Guru and Stock Level Alert' &&
      <StockGuruAlert/>
      }     


        
    </>
  );
}




