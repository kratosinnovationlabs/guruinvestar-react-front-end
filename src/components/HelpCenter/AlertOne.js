import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '../HelpCenter/Alert';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Link} from 'react-router-dom';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Tooltip from '@material-ui/core/Tooltip';



const useStyles = makeStyles((theme) => ({
  tableHead:{
    color:'#f89b00',
    fontSize:'14px',
    fontWeight:'bold',
  },
  tHead:{
    color:'#f89b00',
    fontSize:'14px',
    fontWeight:'bold',
    padding:'0px 8px'
  },
  link:{
    color:'#ffffff'
  },
  svgIcon:{
    color:'#fffc00'
  },
  button:{
    color:'#000000',
    fontSize:'10px',
    border:'0px'
  },
  tableCell:{
    fontSize:'12px',
  },
  tCell:{
    fontSize:'12px',
    padding:'0px 8px'
  }
}));

function createData(guruName, issuerName,  shares, price,) {
  return {guruName,  issuerName,  shares, price,};
}

const rows=[
{guruName:"Amit S Khandelwal", issuerName:"Sample Name",shares:"1132",price:"120.56"},
{guruName:"Amit S Khandelwal", issuerName:"Sample Name",shares:"1132",price:"120.56"},
{guruName:"Amit S Khandelwal", issuerName:"Sample Name",shares:"1132",price:"120.56"},
{guruName:"Amit S Khandelwal", issuerName:"Sample Name",shares:"1132",price:"120.56"},
{guruName:"Amit S Khandelwal", issuerName:"Sample Name",shares:"1132",price:"120.56"},
{guruName:"Amit S Khandelwal", issuerName:"Sample Name",shares:"1132",price:"120.56"},


];

export default function AlertOne() {
    const classes = useStyles();

    

    
  
    return (
    <div>
     
      <Alert prev="/alert5"  next="/alert2"head={"GURU ALERT - Your Favorite guru holding new position"}time={"Today, 10:00 AM IST"} message={"Your Fav guru for the December 2020 has invested in new holding"}/>
          <Table size="small" aria-label="a dense table">
            <TableHead >
            <TableRow >
              <TableCell align="left" style={{width:'11%'}} className={classes.tableHead}>Guru Name</TableCell>
              <TableCell align="left" style={{width:'8%'}} className={classes.tHead}>Issuer Name</TableCell>
              <TableCell align="left" style={{width:'5%'}} className={classes.tHead}>No.of Shares</TableCell>           
              <TableCell align="right"style={{width:'6%'}}  className={classes.tHead}>Price Bought</TableCell>
              <TableCell align="right"style={{width:'70%'}}  className={classes.tHead}></TableCell>

            
            </TableRow>
            </TableHead>
            <TableBody>
        

              { rows.map((row) => (
                <TableRow >
                  <TableCell className={classes.tableCell}style={{width:'11%'}} align="left">{row.guruName}</TableCell>
                  <TableCell className={classes.tCell}style={{width:'8%'}} align="left">{row.issuerName}</TableCell>
                  <TableCell className={classes.tCell}style={{width:'5%'}} align="left">{row.shares} </TableCell>
                  <TableCell className={classes.tCell}style={{width:'6%'}} align="right">{row.price} </TableCell>
                  <TableCell className={classes.tCell}className={classes.tableCell}align="left">
                    <Tooltip title="View" placement="right">
                      <Link variant="body"  className={classes.wrapIcon}>
                        <VisibilityIcon className = {classes.svgIcon}  />  
                      </Link>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>

      </div>
    );
  }