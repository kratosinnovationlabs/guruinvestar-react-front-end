import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '../HelpCenter/Alert';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import {Link} from 'react-router-dom';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Tooltip from '@material-ui/core/Tooltip';


const useStyles = makeStyles((theme) => ({

  tableHead:{
    color:'#f89b00',
    fontSize:'14px',
    fontWeight:'bold',
  },
  tHead:{
    color:'#f89b00',
    fontSize:'14px',
    fontWeight:'bold',
    padding:'0px 8px'
  },
  link:{
    color:'#ffffff'
  },
  svgIcon:{
    color:'#fffc00'
  },
  button:{
    color:'#000000',
    fontSize:'10px',
    border:'0px'
  },
  tableCell:{
    fontSize:'12px',
  },
  tCell:{
    fontSize:'12px',
    padding:'0px 8px'
  }
}));

function createData(guruName, stock,price,  cmp, change,) {
  return {guruName,  stock,  price, cmp, change};
}

const rows=[
{guruName:"Amit S Khandelwal", stock:"ABC Group",price:"30.56",cmp:"54.56",change:'5%'},
{guruName:"Amit S Khandelwal", stock:"ABC Group",price:"30.56",cmp:"54.56",change:'5%'},
{guruName:"Amit S Khandelwal", stock:"ABC Group",price:"30.56",cmp:"54.56",change:'5%'},
{guruName:"Amit S Khandelwal", stock:"ABC Group",price:"30.56",cmp:"54.56",change:'5%'}


];

export default function AlertFour() {
    const classes = useStyles();
   
    return (
    <>
     
      <Alert  prev="/alert4" next="/alert1" head={"STOCK GURU ALERT - Price Change Alert of your favorite guru Warren Buffet for your favorite stock TA "}time={"Today, 11:00 AM IST"} message={"There has been an increase/decrease in your favorite stock TA as compared to price at which your favorite guru Warren Buffet bought/sold for the quarter ended December 2020 -"} />
      <Table size="small" aria-label="a dense table">
            <TableHead >
            <TableRow >
              <TableCell align="left" style={{width:'11%'}} className={classes.tableHead}>Guru Name</TableCell>
              <TableCell align="left" style={{width:'8%'}} className={classes.tHead}>Stock Name</TableCell>
              <TableCell align="right" style={{width:'5%'}} className={classes.tHead}>Bought/Sold by your <br/> Guru at Price</TableCell>           
              <TableCell align="right"style={{width:'6%'}}  className={classes.tHead}>CMP</TableCell>
              <TableCell align="left"style={{width:'7%'}}  className={classes.tHead}>%Change</TableCell>
              <TableCell align="right"style={{width:'63%'}}  className={classes.tHead}></TableCell>


            
            </TableRow>
            </TableHead>
            <TableBody>
        

              { rows.map((row) => (
                <TableRow >
                  <TableCell className={classes.tableCell}style={{width:'11%'}} align="left">{row.guruName}</TableCell>
                  <TableCell className={classes.tCell}style={{width:'8%'}} align="left">{row.stock}</TableCell>
                  <TableCell className={classes.tCell}style={{width:'5%'}} align="right">{row.price} </TableCell>
                  <TableCell className={classes.tCell}style={{width:'6%'}} align="right">{row.cmp} </TableCell>
                  <TableCell className={classes.tCell}style={{width:'7%'}} align="left">{row.change} </TableCell>

                  <TableCell className={classes.tCell}className={classes.tableCell}align="left">
                    <Tooltip title="View" placement="right">
                      <Link variant="body"  className={classes.wrapIcon}>
                        <VisibilityIcon className = {classes.svgIcon}  />  
                      </Link>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
      </>
    );
  }