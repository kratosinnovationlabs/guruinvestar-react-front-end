import React, { useContext, useState, useEffect } from 'react';
import { useSetSnackbar } from '../../hooks/useSnackbar';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
//import axios from 'axios'
import Container from '@material-ui/core/Container';  
import {Link} from 'react-router-dom'
//import { URL } from '../consts';
import ArrowBack from "@material-ui/icons/ArrowBack";
import IconButton from "@material-ui/core/IconButton";
//import NavContext from '../context/NavContext';
//import {UserContext} from '../context/user-context';
//import LinkedInPage from './LInkedInPage';
//import GooglePage from './GooglePage';
//import {PLATFORM_NAME} from "../consts";
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import clsx from 'clsx';
import LockIcon from '@material-ui/icons/Lock';
import EmailIcon from '@material-ui/icons/Email';
import PersonIcon from '@material-ui/icons/Person';
import Logo from '../../assets/logo.png';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';
import { BaseURL } from '../../consts';
import { useDispatch, useSelector } from "react-redux";
import { register } from "../../actions/auth";
import Footer from '../Footer/Footer';
import FooterOne from '../Footer/FooterOne';


const useStyles = makeStyles({
  scren:{
    maxWidth:'450px',
    "@media (min-width: 1920px)": {
      maxWidth:'700px'
    }
  },
  card: {
    borderRadius: '6px',
    padding: '12px',
    
  },

  projTitile: {
    padding: 20,
    fontSize: 30,
    textAlign: 'center',
    textTransform: 'uppercase',
    letterSpacing: '3px',
    color: '#fff'
  },
  cardHeader:{
    textAlign: 'center',
    color:'#f5f6fa',
    fontSize:'18px',
   "@media (min-width: 1920px)": {
    fontSize:'25px'
  }
  },

  actions: {
    display: 'block',
  },

  button: {
    
    backgroundColor:'#faed32',
    size:'small',
    height:'40px',
    "@media (min-width: 1920px)": {
      marginBottom: 20,
    width:'326px',
    height:'60px',
    fontSize:'30px',
    fontFamily:'Montserrat',
    fontWeight:'500',
    }
    
  },

  logoContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent:'center',
  
  },

  logoImg: {
    height: 50
  },

  textField: {
    width: '100%',
    
  },

  errorText: {
    textAlign: 'center',
    color: 'red',
    marginBottom: 0
  },

  root: {
    flexGrow: 1,
  },

  link: {
    color: '#3f51b5 !important',
    fontSize:'30px'
  },

  forgot_button: {
    textAlign: "right"
  },
  linkText:{
    color: "#faed32",
    fontSize:'20px',
    "@media (min-width: 1920px)": {
      fontSize:'25px'
    }
  },
  errors:{
    textAlign:'right',
    color:'#f92948',
   marginTop:'5px'
  },
  input:{
    backgroundColor:'#444444',
    height:'40px',
    "@media (min-width: 1920px)": {
      height:'60px',
      backgroundColor:'#444444',
    }


  },

  foot:{
    fontSize:'25px',
    "@media (min-width: 1920px)": {
      fontSize:"20px"
    }
  },
  footer:{
    position:'absolute',
    bottom:0,   
    marginTop:"auto",
    
    

  },
  
  
});

const STATUS = {
  IDLE: "IDLE",
  SUBMITTED: "SUBMITTED",
  SUBMITTING: "SUBMITTING",
  COMPLETED: "COMPLETED",
};

function SignUp(props) {
  
  const classes = useStyles();
  const setSnackbar = useSetSnackbar();
  const [ user, setUser ] = useState({
    username: '',
    email: '',
    password: '',
    confirm_password: '',
    firstname: '',
    lastname: '',
    isLoading: false,
    error: {},
    showPassword: false,
    showConfirmPassword: false
  })
  const [status, setStatus] = useState(STATUS.IDLE);
  const [touched, setTouched] = useState({});
  const [validations, setValidations] = useState(false);
  
  const dispatch = useDispatch();
  const error_messages = useSelector(state => state.auth.errors);
  
  // Derived state
  const errors = getErrors(user);
  const isValid = Object.keys(errors).length === 0;

  useEffect(() => {
    if( sessionStorage.getItem('auth') === "true" ){
      props.history.push( '/dashboard' );
    }

  }, [])

  const validateForm = () => {
    return user.email.length > 0 && user.password.length >= 6 && ! user.isLoading ;
  }

  const handleClickShowPassword = () => {
    setUser({ ...user, showPassword: !user.showPassword });
  };

  const handleClickShowConfirmPassword = () => {
    setUser({ ...user, showConfirmPassword: !user.showConfirmPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  function handleBlur(event) {
    event.persist();
    setTouched((cur) => {
      return { ...cur, [event.target.id]: true };
    });
  }

  function getErrors(user) {
    const result = {};
    if (!user.firstname) result.firstname = "First Name is required";
    if (!user.email) result.email = "Email is required";
    if (!user.password) result.password = "Password is required";
    if (!user.confirm_password) result.confirm_password = "Confirm Password is required";
    return result;
  }

  // const handleEmail = (e) => {
  //   setUser({...user,  email: e.target.value })
  //   setValidations(false)
  //   debugger;
  // }

  const handlechange = (e) => {
    setUser({...user,  [e.target.id]: e.target.value })
    setValidations(false)
  }

  const handleSubmit = async event => {
    event.preventDefault();
    setStatus(STATUS.SUBMITTING);
    let userObj = { 
      user:{
        frst_nm: user.firstname,
        lst_nm: user.lastname,
        email: user.email, 
        password: user.password, 
        password_confirmation: user.confirm_password
      }
    }
    if (isValid) {
      dispatch(register(userObj))
      .then((response) => {
        setSnackbar("We have sent a confirmation mail to your email address", "success");
        props.history.push("/")
        setStatus(STATUS.COMPLETED);
      }).catch((error) => {
        setStatus(STATUS.SUBMITTED);
        setValidations(true)
        debugger;
      });

      
    } else {
      setStatus(STATUS.SUBMITTED);
    }
  };

  
  
  return (  
    <div className="logoContainer" style={{ backgroundImage: `url('${process.env.PUBLIC_URL}/login-bg.jpg')`, height:'50px'}}>
      <Container maxWidth="lg">
        <Grid container direction="row" justify="center" alignItems="center" >
          <Grid className={classes.screen} item lg={5} xs={12}>
            
            <Card className={classes.card}>
              <CardHeader  title='Follow GuruInveSTARs' titleTypographyProps={{color:'#f5f6fa',variant:'h4',fontSize:'20px',"@media (min-width: 1920px)": {fontSize:'25px',variant:'h3'  } }} className={classes.cardHeader}/>
              
              <form onSubmit={handleSubmit}>
                <CardContent>
                  <Grid container spacing={3}> 
                    <Grid item xs={6}>
                          <FormControl className={clsx(classes.margin, classes.textField)} variant="filled" size='small' >
                              <OutlinedInput
                                  
                                  className={classes.input}
                                  id="firstname"
                                  value={user.firstname}
                                  placeholder="First Name"
                                  onBlur={handleBlur}
                                  onChange={handlechange}
                                  // onChange={e => setUser({...user,  firstname: e.target.value })}
                                  // startAdornment={<InputAdornment position="start"><PersonIcon/></InputAdornment>}
                              /> 
                          </FormControl>
                        <Typography variant="body2" className={classes.errors}>
                          {(touched.firstname || status === STATUS.SUBMITTED) && errors.firstname}
                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined" size='small'>
                            <OutlinedInput
                                className={classes.input}
                                id="lastname"
                                value={user.lastname}
                                placeholder="Last Name"
                                onChange={handlechange}
                                // onChange={e => setUser({...user,  lastname: e.target.value })}
                                // startAdornment={<InputAdornment position="start"><PersonIcon/></InputAdornment>}
                            /> 
                        {/* <Typography variant="body2" className={classes.errors}>
                          { user.error !== {} && user.error.lst_nm && user.error.lst_nm.join(", ") }
                        </Typography> */}
                        </FormControl>

                    </Grid>
                    <Grid item xs={12}>
                      <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined" size='small'>
                          <OutlinedInput 
                             className={classes.input}
                              id="email"
                              type="email" 
                              value={user.email}
                              placeholder="Email"
                              onBlur={handleBlur}
                              onChange={handlechange}
                              // startAdornment={<InputAdornment position="start"><EmailIcon/></InputAdornment>}
                          /> 
                      </FormControl>
                      <Typography variant="body2" className={classes.errors}>
                      {(touched.email || status === STATUS.SUBMITTED) && errors.email} 
                      { error_messages && validations && error_messages.email}
                      </Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined" size='small'>
                          <OutlinedInput
                            className={classes.input}
                              id="password"
                              type={user.showPassword ? 'text' : 'password'}
                              value={user.password}
                              placeholder="Password"
                              onBlur={handleBlur}
                              onChange={handlechange}
                              // onChange={e => setUser({...user,  password: e.target.value })}
                              // startAdornment={<InputAdornment position="start"><LockIcon/></InputAdornment>}
                              // endAdornment={
                              // <InputAdornment position="end">
                              //     <IconButton
                              //     aria-label="toggle password visibility"
                              //     onClick={handleClickShowPassword}
                              //     onMouseDown={handleMouseDownPassword}
                              //     edge="end"
                              //     >
                              //     {user.showPassword ? <Visibility /> : <VisibilityOff />}
                              //     </IconButton>
                              // </InputAdornment>
                              // }
                            
                          />
                        </FormControl>
                      <Typography variant="body2" className={classes.errors}>
                      {(touched.password || status === STATUS.SUBMITTED) && errors.password }
                      {error_messages && validations && error_messages.password}
                      </Typography>
                      </Grid>
                      <Grid item xs={12}>
                        <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined" size='small'>
                          <OutlinedInput
                             className={classes.input}
                              id="confirm_password"
                              type={user.showConfirmPassword ? 'text' : 'password'}
                              value={user.confirm_password}
                              placeholder="Confirm Password"
                              onBlur={handleBlur}
                              onChange={handlechange}
                              // onChange={e => setUser({...user,  confirm_password: e.target.value })}
                              // startAdornment={<InputAdornment position="start"><LockIcon/></InputAdornment>}
                              // endAdornment={
                              // <InputAdornment position="end">
                              //     <IconButton
                              //     aria-label="toggle password visibility"
                              //     onClick={handleClickShowConfirmPassword}
                              //     onMouseDown={handleMouseDownPassword}
                              //     edge="end"
                              //     >

                              //     {user.showConfirmPassword ? <Visibility /> : <VisibilityOff />}
                              //     </IconButton>
                              // </InputAdornment>
                              // }
                            
                          />
                        </FormControl>
                      <Typography variant="body2" className={classes.errors}>
                      {(touched.confirm_password || status === STATUS.SUBMITTED) && errors.confirm_password}
                      {error_messages && validations && error_messages.password_confirmation}
                      </Typography>
                    </Grid>
                  </Grid>
                </CardContent>
                <CardActions className={classes.actions}>
                  <Grid container spacing={3} direction="column" justify="center" alignItems="center">
                      <Grid item xs={12}>
                        <Button variant="contained" color="primary" className={classes.button}  type="submit">
                          { user.isLoading && <CircularProgress size={24} />}
                          { ! user.isLoading && "Sign Up"}
                        </Button>
                      </Grid>
                      <Grid item xs={12}>
                        <Typography className={classes.foot}>Already have an account ? <Link to="/" className={classes.linkText}> Sign In </Link></Typography>
                      </Grid> 
                    </Grid>
                </CardActions>
              </form>
            </Card>
          </Grid>
        </Grid>
      </Container>  
      <FooterOne className={classes.footer}/>
      
    </div>

  )
}

export default SignUp
