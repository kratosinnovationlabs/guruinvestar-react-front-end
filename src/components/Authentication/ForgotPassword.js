import React, { useContext, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Container from '@material-ui/core/Container';  
import {Link} from 'react-router-dom'
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import clsx from 'clsx';
import EmailIcon from '@material-ui/icons/Email';
import Typography from '@material-ui/core/Typography';
import Footer from '../Footer/Footer';
import { BaseURL } from '../../consts';
import axios from 'axios';
import {useSetSnackbar} from "../../hooks/useSnackbar";


const useStyles = makeStyles({
  card: {
    width: '100%',
    borderRadius: '6px',
    padding: '12px'
  },

  projTitile: {
    padding: 20,
    fontSize: 30,
    textAlign: 'center',
    textTransform: 'uppercase',
    letterSpacing: '3px',
    color: '#fff'
  },
  cardHeader:{
    textAlign: 'center'
  },

  actions: {
    display: 'block',
  },

  button: {
    marginBottom: 20
  },

  logoContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent:'center',
    marginBottom: 20
  },

  logoImg: {
    height: 50
  },

  textField: {
    width: '100%',
  },

  errorText: {
    textAlign: 'center',
    color: 'red',
    marginBottom: 0
  },

  root: {
    flexGrow: 1,
  },

  link: {
    color: '#3f51b5 !important'
  },

  forgot_button: {
    textAlign: "right"
  },
  linkText:{
    color: "#faed32"
  }
  
});

function ForgotPassword(props) {
  
  const classes = useStyles();
  const setSnackbar = useSetSnackbar();

  const [user, setUser] = useState({
    email: '',
    isLoading: false,
    error: ''
})

  useEffect(() => {
    if( sessionStorage.getItem('auth') === "true" ){
      props.history.push( '/dashboard' );
    }

  }, [])

  const validateForm = () => {
    return user.email.length > 0 && user.password.length >= 6 && ! user.isLoading ;
  }

  const handleClickShowPassword = () => {
    setUser({ ...user, showPassword: !user.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };


  const handleSubmit = async event => {
    event.preventDefault();
    setUser(prev => ({
        ...prev,
        isLoading: true
    }))

    try {
        axios({
            method: 'post',
            url: BaseURL + '/password',
            data: {
                user: {
                    email: user.email
                }
            },
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': '*',
            }
        }).then(res => {
            setSnackbar("You will receive an email with instructions on how to reset your password in a few minutes.", "success");
            setUser(prev => ({
                email: '',
                isLoading: false,
            }))
            props.history.push( "/" );
        }).catch(error => {
            if (error.response && error.response.status !== 200) {
                for (const err in error.response.data.errors) {
                    console.log(error.response.data.errors[err]);
                    setUser(prev => ({
                        ...prev,
                        isLoading: false,
                        error: `${err} ${error.response.data.errors[err]}`
                    }))
                    setSnackbar(`${err} ${error.response.data.errors[err]}`, "error");
                }

            }
        })
    } catch (e) {
        setSnackbar("Some thing went Wrong", "error")
        setUser({...user, isLoading: false})
    }
};


  return (
    <div className="Login-container">
      <Container maxWidth="lg">
        <Grid container direction="row" justify="center" alignItems="center" >
          <Grid item md={4} xs={12}>
           
            <Card className={classes.card}>
              <CardHeader 
                title="Forgot Password"  className={classes.cardHeader}
                />
              <form onSubmit={handleSubmit}>
                <CardContent>
                  <Grid container spacing={3}> 
                    <Grid item xs={12}>
                      <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined" size='small'>
                          <OutlinedInput
                              id="outlined-adornment-email"
                              value={user.email}
                              placeholder="Email"
                              onChange={e => setUser({...user,  email: e.target.value })}
                              startAdornment={<InputAdornment position="start"><EmailIcon/></InputAdornment>}
                          /> 
                      </FormControl>
                    </Grid>
                    
                  </Grid>
                </CardContent>
                <CardActions className={classes.actions}>
                  <Grid container spacing={3} direction="column" justify="center" alignItems="center">
                      <Grid item xs={12}>
                        <Button variant="contained" color="primary" className={classes.button}  type="submit">
                          { user.isLoading && <CircularProgress size={24} />}
                          { ! user.isLoading && "Submit"}
                        </Button>
                      </Grid>
                      <Grid item xs={12}>
                        <Typography variant="body1"> Already have an account ? <Link to="/" className={classes.linkText}> Sign In </Link></Typography>
                      </Grid> 
                    </Grid>
                </CardActions>
              </form>
            </Card>
          </Grid>
        </Grid>
      </Container> 
     
    </div>
  )
}

export default ForgotPassword
