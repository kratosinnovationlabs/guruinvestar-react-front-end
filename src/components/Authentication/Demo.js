import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';  
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import EmailIcon from '@material-ui/icons/Email';
import Logo from '../../assets/logo.png'
import Footer from '../Footer/Footer'
import AppBar from '@material-ui/core/AppBar';
import clsx from 'clsx';



const useStyles = makeStyles({
  root:{
    backgroundColor:'#000000'
    // backgroundImage: linearGradient(to bottom,' #000000' ,'#262626')
    
  },
  head:{
  backgroundColor: '#262626',
  color: '#faed32',
  width:'100%'

  },
  card: {
    borderRadius: '6px',
    padding: '12px',
   
    backgroundColor: '#262626'
  },

  cardHeader: {
    textAlign: 'center',
    size: '42px',
    margin: '10px'
  },
  logoContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent:'center',
    marginBottom: 20,
    
  },

  text: {
    textAlign: "center",
    
    padding: '10px',
 
  },
  btnOne: {
    margin: '20px',
    backgroundColor: "#faed32"
  },
  btnTwo: {
    
    backgroundColor: "#faed32",
    
  },

  appbar: {
    backgroundColor: '#181815',
    padding: '2px',
   
    height: '30px',
    textAlign: 'left',
    color: '#faed32'

  },
  
});

export default function Demo() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
     {/* <Grid item sm={12} >
        <Typography variant="subtitle2"  className={classes.head}>
           Reach us at : support@guruinvestars.com
        </Typography>
      </Grid> */}
    
    <AppBar position="static" >
      <Typography variant="subtitle1" className={classes.appbar} >
       Reach us at : support@guruinvestars.com
      </Typography>
      
      </AppBar>
    
     <Container maxWidth="lg" >
     
      <Grid container direction="row" justify="center" alignItems="center" >
        <Grid item md={5} xs={12}>
          <div className={classes.logoContainer}>
            <img src={Logo} width="70%" />
          </div>
        <Card className={classes.card}>
          <CardHeader
           title="Request Demo"  className={classes.cardHeader}
                />
          <Typography variant="subtitle1" className={classes.text}>
             Enter your Email id and our team would contact you for a demo
          </Typography>
          <CardContent>
            <Grid container 
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
              <Grid item xs={12}>
                <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined" size='small' >
                    <OutlinedInput 
                        id="outlined-adornment-email"
                      
                        placeholder="Email"
                      
                        startAdornment={<InputAdornment position="start"><EmailIcon/></InputAdornment>}
                    /> 
                </FormControl>
              </Grid>
              <Grid >
                <Button variant="contained" size='medium ' className={classes.btnOne}>
                  SUBMIT
                </Button>
                </Grid>
                <Grid>
                <Button variant="contained"   size='medium'className={classes.btnTwo}>
                  TAKE A TOUR
                </Button>
                </Grid>
            
            </Grid>
          </CardContent>
        </Card>
       </Grid>
       </Grid>
       </Container>
      

   
   
   
   
    
  </div>

  );
}
