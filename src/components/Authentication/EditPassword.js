import React, {useState} from 'react'
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import {Link} from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";
import CardContent from "@material-ui/core/CardContent";
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FormControl from '@material-ui/core/FormControl';
import clsx from 'clsx';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import {makeStyles} from "@material-ui/core/styles";
import axios from "axios";
import {BaseURL} from "../../consts";
import {useSetSnackbar} from "../../hooks/useSnackbar";

const useStyles = makeStyles({

    projTitile: {
      padding: 20,
      fontSize: 30,
      textAlign: 'center',
      textTransform: 'uppercase',
      letterSpacing: '3px',
      color: '#fff'
    },
    card: {
        width: '100%',
        borderRadius: '6px',
    },

    cardHeader: {
      textAlign: 'center'
    },

    actions: {
      display: 'flex',
      justifyContent: 'center'
    },

    textField: {
        width: '100%',
    },

    errorText: {
        textAlign: 'center',
        color: 'red',
        marginBottom: 0
    },

});

function EditPassword(props) {
    const classes = useStyles();
    const setSnackbar = useSetSnackbar();

    const [user, setUser] = useState({
        password: '',
        password_confirmation: '',
        isLoading: false,
        error: ''
    })

    const validateForm = () => {
        return user.password.length >= 6 && user.password === user.password_confirmation && !user.isLoading;
    }

    const handlePasswordConfirmation = event => {
        const {value} = event.target;
        setUser(prev => ({...prev, password_confirmation: value}))
        if (user.password !== value) {
            setUser(prev => ({...prev, error: "Passwords do not match!"}))
        } else {
            setUser(prev => ({...prev, error: ""}))
        }
    }

    const handleSubmit = async event => {
        event.preventDefault();
        setUser(prev => ({
            ...prev,
            isLoading: true
        }))
        var token = props.match.params.token;

        try {
            axios({
                method: 'put',
                url: BaseURL + '/password',
                data: {
                    user: {
                        reset_password_token: token,
                        password: user.password,
                        password_confirmation: user.password_confirmation
                    }
                },
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                    'Authorization': localStorage.getItem("JWTTOKEN")
                }
            }).then(res => {
                setSnackbar("Password is updated successfully. Please Login", "success");
                setUser(prev => ({
                    password: '',
                    password_confirmation: '',
                    isLoading: false,
                }))
                props.history.push( "/" );
            }).catch(error => {
                if (error.response && error.response.status !== 200) {
                    for (const err in error.response.data.errors) {
                        setUser(prev => ({
                            ...prev,
                            isLoading: false,
                            error: `${err.replaceAll('_', ' ')} ${error.response.data.errors[err]}`
                        }))
                        setSnackbar(`${err.replaceAll('_', ' ')} ${error.response.data.errors[err]}`, "error");
                    }

                }
            })
        } catch (e) {
            setSnackbar("Some thing went Wrong", "error")
            setUser({...user, isLoading: false})
        }
    };

    return (
        <div className="edit-password-container">
            <Container maxWidth="lg">
                <Grid container direction="row-reverse" justify="center" alignItems="center" style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                    <Grid item md={4} xs={12}>
                        <Card className={classes.card}>
                            <CardHeader title="Change Password" className={classes.cardHeader}/>
                            <form onSubmit={handleSubmit}>
                                <CardContent>
                                  <Grid>
                                        <Grid item xs={12}>
                                          <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined" size='small'>
                                            <OutlinedInput
                                                id="password"
                                                type={'password'}
                                                value={user.password}
                                                placeholder="Password"
                                                required={true}
                                                onChange={e => setUser({...user,  password: e.target.value })}
                                              
                                              
                                            />
                                            </FormControl>
                                        </Grid>
                                        <br/><br/>
                                        <Grid item xs={12}>
                                          <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined" size='small'>
                                            <OutlinedInput
                                                id="password"
                                                type={'password'}
                                                value={user.password_confirmation}
                                                placeholder="Password Confirmation"
                                                required={true}
                                                onChange={e => setUser({...user,  password_confirmation: e.target.value })}
                                              
                                              
                                            />
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                                <CardActions className={classes.actions}>
                                    <Button variant="contained" color="primary" className={classes.button}
                                             type="submit">
                                        {user.isLoading && <CircularProgress size={24}/>}
                                        {!user.isLoading && "Update Password"}
                                    </Button>
                                </CardActions>
                            </form>
                        </Card>
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}

export default EditPassword
