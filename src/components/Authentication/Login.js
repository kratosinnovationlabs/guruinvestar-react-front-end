import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Container from '@material-ui/core/Container';
import { Link } from 'react-router-dom'
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FormControl from '@material-ui/core/FormControl';
import clsx from 'clsx';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Footer from '../Footer/Footer'
import { login } from "../../actions/auth";
import { Redirect } from 'react-router-dom';
import FooterOne from '../Footer/FooterOne';
import { useSetSnackbar } from '../../hooks/useSnackbar';


const useStyles = makeStyles({
  screen: {
    maxWidth: '450px',
    "@media (min-width: 1920px)": {
      maxWidth: '700px'
    }

  },
  card: {
    borderRadius: '2px',
    padding: '12px',
    // backgroundColor: '#262626'
  },

  cardHeader: {
    textAlign: 'center'
  },

  projTitile: {
    padding: 20,
    fontSize: 30,
    textAlign: 'center',
    textTransform: 'uppercase',
    letterSpacing: '3px',
    color: '#fff'
  },

  actions: {
    display: 'flex',
    justifyContent: 'center'
  },

  logoContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    height: '150px'
  },

  logoImg: {
    height: 50
  },

  textField: {
    width: '100%',
  },

  errorText: {
    color: 'red',
    marginBottom: 0
  },

  root: {
    flexGrow: 1,
  },

  link: {
    color: '#3f51b5 !important'
  },

  forgot_button: {
    textAlign: "right"
  },
  linkText: {
    color: "#faed32",
    fontSize: '20px',
    "@media (min-width: 1920px)": {
      fontSize: '25px'
    }
  },
  button: {
    backgroundColor: '#faed32',
    size: 'small',
    height: '40px',
    "@media (min-width: 1920px)": {
    }
  },

  input: {
    backgroundColor: '#444444',
    height: '40px',
    "@media (min-width: 1920px)": {
      height: '60px',
      backgroundColor: '#444444',
    }

  },
  foot: {
    fontSize: '20px',
    "@media (min-width: 1920px)": {
      fontSize: '25px'
    }
  },
  remember: {
    fontSize: '18px',
    "@media (min-width: 1920px)": {
      fontSize: '25px'
    }
  },

});

function Login(props) {

  const classes = useStyles();
  const setSnackbar = useSetSnackbar();
  const dispatch = useDispatch();
  const [user, setUser] = useState({
    email: '',
    password: '',
    isLoading: false,
    error: '',
    showPassword: false
  })
  const [touched, setTouched] = useState({});
  const [validations, setValidations] = useState(false);
  // Derived state
  const errors = getErrors(user);
  const isValid = Object.keys(errors).length === 0;

  const error_message = useSelector(state => state.auth.errors);
  function getErrors(user) {
    const result = {};
    if (!user.email) result.email = "Email is required";
    if (!user.password) result.password = "Password is required";
    return result;
  }

  function handleBlur(event) {
    event.persist();
    setTouched((cur) => {
      return { ...cur, [event.target.id]: true };
    });
  }

  const validateForm = () => {
    return user.email.length > 0 && user.password.length >= 6 && !user.isLoading;
  }

  const handleClickShowPassword = () => {
    setUser({ ...user, showPassword: !user.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();

  };

  const handleSubmit = event => {
    setUser({ ...user, isLoading: true })
    event.preventDefault();
    if (isValid) {
      dispatch(login(user.email, user.password))
        .then(() => {
          setSnackbar("Logged In Successfully", "success")
          props.history.push("/home");
        })
        .catch((error) => {
          setUser({ ...user, isLoading: false })
          setValidations(true)
        });
    } else {
      setUser({ ...user, isLoading: false })
    }
  };

  if (localStorage.getItem('user')) {
    return <Redirect to="/home" />;
  }

  return (
    <div className="logoContainer" style={{ backgroundImage: `url('${process.env.PUBLIC_URL}/login-bg.jpg')`, height: '50px' }}>
      <Container maxWidth="lg">
        <Grid container direction="row" justify="center" alignItems="center" >
          <Grid className={classes.screen} item lg={6} xs={12}>
            <Card className={classes.card}>
              {/* <CardHeader 
                title="Welcome Back !"titleTypographyProps={{variant:'h3', fontWeight:'bold',color:'#f5f6fa' }}   className={classes.cardHeader}
                /> */}
              <Typography variant="body2" className={classes.errorText} align="center">
                {/* { validations instanceof String && validations} */}
                {typeof error_message === 'string' && validations && error_message}

              </Typography>
              <form onSubmit={handleSubmit}>
                <CardContent>
                  <Grid container spacing={3}>
                    <Grid item xs={12}>
                      <FormControl className={clsx(classes.margin, classes.textField)} variant="filled" size='small'>
                        <OutlinedInput
                          className={classes.input}
                          id="email"
                          value={user.email}
                          placeholder="Email"
                          type="email"
                          onBlur={handleBlur}
                          onChange={e => setUser({ ...user, email: e.target.value })}
                          required={true}
                        />
                      </FormControl>
                      <Typography variant="body2" className={classes.errorText} align="right">
                        {(touched.email) && errors.email}
                      </Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined" size='small'>
                        <OutlinedInput
                          className={classes.input}
                          id="password"
                          type={user.showPassword ? 'text' : 'password'}
                          value={user.password}
                          placeholder="Password"
                          required={true}
                          onBlur={handleBlur}
                          onChange={e => setUser({ ...user, password: e.target.value })}


                        />
                      </FormControl>
                      <Typography variant="body2" className={classes.errorText} align="right">
                        {(touched.password) && errors.password}
                      </Typography>
                    </Grid>
                  </Grid>
                </CardContent>
                <CardActions className={classes.actions}>
                  <Grid container spacing={3} direction="column" justify="center" alignItems="center">
                    <Grid item xs={12}>
                      <Typography className={classes.foot} variant="body1">Forgot your password ?<Link to="/forgotpassword" className={classes.linkText}> Click Here </Link> </Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <FormControlLabel control={<Checkbox name="checkedC" color="primary" />} label={<Typography className={classes.remember}>Remember Me</Typography>} />
                    </Grid>

                    <Grid item xs={12}>
                      <Button variant="contained" color="primary" className={classes.button} type="submit">
                        {user.isLoading && <CircularProgress size={24} style={{ color: 'black' }} />}
                        {!user.isLoading && "LOGIN"}
                      </Button>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography className={classes.foot} variant="body1">Don't have an account yet ? <Link to="/signup" className={classes.linkText}> Sign Up </Link></Typography>
                    </Grid>
                  </Grid>
                </CardActions>
              </form>
            </Card>
          </Grid>
        </Grid>
      </Container>
      <FooterOne />


    </div>
  )
}

export default Login
