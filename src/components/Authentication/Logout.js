import React from 'react'
import { useDispatch, useSelector } from "react-redux";
import {useSetSnackbar} from "../../hooks/useSnackbar";

export default function Logout(props) {
  const dispatch = useDispatch();
  const setSnackbar = useSetSnackbar();
  

  React.useEffect(() => {
    localStorage.removeItem("user")
    localStorage.removeItem("JWTTOKEN");
    props.history.push("/")
    setSnackbar("Your sessrin got expired. Please login again", "error")

  }, [])
  return (
      <div>
          
      </div>
  )
}
