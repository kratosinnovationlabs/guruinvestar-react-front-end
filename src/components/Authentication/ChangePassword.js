import React, {useState} from 'react'
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import {Link} from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import {makeStyles} from "@material-ui/core/styles";
import axios from "axios";
import {URL} from "../consts";
import {useSetSnackbar} from "../hooks/useSnackbar";
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import clsx from 'clsx';

const useStyles = makeStyles({
    card: {
        width: '100%',
        borderRadius: 0,
    },

    cardHeader: {},

    actions: {
        display: 'block',
    },

    button: {
        marginBottom: 20
    },

    textField: {
        width: '100%',
    },

    errorText: {
        textAlign: 'center',
        color: 'red',
        marginBottom: 0
    },

});

function ChangePassword(props) {
    const classes = useStyles();
    const setSnackbar = useSetSnackbar();

    const [user, setUser] = useState({
        current_password: '',
        show_current_password: false,
        password: '',
        show_password: false,
        password_confirmation: '',
        show_password_confirmation: false,
        isLoading: false,
        error: ''
    })

    const validateForm = () => {
        return user.current_password.length >= 6 && user.password.length >= 6 && user.password === user.password_confirmation && !user.isLoading;
    }

    const handleClickShowNewPassword = (e) => {
        setUser({ ...user, show_password: !user.show_password });
    };

    const handleClickShowCurrentPassword = (e) => {
      setUser({ ...user, show_current_password: !user.show_current_password });
    };

    const handleClickShowConfirmPassword = (e) => {
      setUser({ ...user, show_password_confirmation: !user.show_password_confirmation });
    };



    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };


    const handlePasswordConfirmation = event => {
        const {value} = event.target;
        setUser(prev => ({...prev, password_confirmation: value}))
        if (user.password !== value) {
            setUser(prev => ({...prev, error: "Passwords do not match!"}))
        } else {
            setUser(prev => ({...prev, error: ""}))
        }
    }

    const handleSubmit = async event => {
        event.preventDefault();

        try {
            axios({
                method: 'put',
                url: URL + '/passwords',
                data: {
                    user: {
                        current_password: user.current_password,
                        password: user.password,
                        password_confirmation: user.password_confirmation
                    }
                },
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                    'Authorization': localStorage.getItem("JWTTOKEN")
                }
            }).then(res => {
                setSnackbar("Password is updated successfully.", "success");
                props.history.push("/dashboard")
                // setTimeout(() => props.history.push("/logout"), 2000)
            }).catch(error => {
                if (error.response && error.response.status !== 200) {
                    setUser(prev => ({
                        ...prev,
                        isLoading: false,
                        error: error.response.data.error
                    }))
                    setSnackbar(error.response.data.error, "error")
                }
            })
        } catch (e) {
            setSnackbar("Some thing went Wrong", "error")
            setUser({...user, isLoading: false})
        }
    };

    return (
        <div className="Main-container change-password-container">
            <Container maxWidth="lg">
                <Grid container direction="row-reverse" justify="center" alignItems="center" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center',}}>
                    <Grid item md={4} xs={12}>
                        <Card className={classes.card}>
                            <CardHeader
                                title="Change Password" className={classes.cardHeader}
                                action={
                                    <div>
                                        <Link to="/dashboard">
                                            <IconButton aria-label="back">
                                                <ArrowBack/>
                                            </IconButton>
                                        </Link>
                                    </div>
                                }/>
                            <form onSubmit={handleSubmit}>
                                <CardContent>
                                    <Grid>
                                        <Grid item xs={12}>
                                          <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                            <InputLabel htmlFor="current_password">Current Password</InputLabel>
                                            <Input
                                              id="current_password"
                                              required
                                              type={user.show_current_password ? 'text' : 'password'}
                                              value={user.current_password}
                                              onChange={e => setUser({...user, current_password: e.target.value })}
                                              endAdornment={
                                                <InputAdornment position="end">
                                                  <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowCurrentPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                    edge="end"
                                                  >
                                                    {user.show_current_password ? <Visibility /> : <VisibilityOff />}
                                                  </IconButton>
                                                </InputAdornment>
                                              }
                                              labelWidth={130}
                                            />
                                          </FormControl>
                                        </Grid>
                                        <Grid item xs={12}>
                                          <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                            <InputLabel htmlFor="new_password">New Password</InputLabel>
                                            <Input
                                              id="new_password"
                                              required
                                              type={user.show_password ? 'text' : 'password'}
                                              value={user.password}
                                              onChange={e => setUser({...user, password: e.target.value })}
                                              endAdornment={
                                                <InputAdornment position="end">
                                                  <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowNewPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                    edge="end"
                                                  >
                                                    {user.show_password ? <Visibility /> : <VisibilityOff />}
                                                  </IconButton>
                                                </InputAdornment>
                                              }
                                              labelWidth={110}
                                            />
                                          </FormControl>
                                        </Grid>
                                        <Grid item xs={12}>
                                        <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                                            <InputLabel htmlFor="confirm_password">Confirm Password</InputLabel>
                                            <Input
                                              id="confirm_password"
                                              required
                                              type={user.show_password_confirmation ? 'text' : 'password'}
                                              value={user.password_confirmation}
                                              onChange={e => setUser({...user, password_confirmation: e.target.value })}
                                              endAdornment={
                                                <InputAdornment position="end">
                                                  <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowConfirmPassword}
                                                    onMouseDown={handleMouseDownPassword}
                                                    edge="end"
                                                  >
                                                    {user.show_password_confirmation ? <Visibility /> : <VisibilityOff />}
                                                  </IconButton>
                                                </InputAdornment>
                                              }
                                              labelWidth={130}
                                            />
                                          </FormControl>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                                <CardActions className={classes.actions}>
                                    <Button fullWidth variant="contained" color="primary" className={classes.button}
                                            type="submit">
                                        {user.isLoading && <CircularProgress size={24}/>}
                                        {!user.isLoading && "Change Password"}
                                    </Button>
                                </CardActions>
                            </form>
                        </Card>
                    </Grid>
                </Grid>
            </Container>
        </div>
    )
}

export default ChangePassword
