import React from 'react'
import Grid from '@material-ui/core/Grid';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import YouTubeIcon from '@material-ui/icons/YouTube';
import Link from '@material-ui/core/Link';
import Typography from "@material-ui/core/Typography";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';


const useStyles = makeStyles({
    root:{
        backgroundColor: '#000000',
        marginTop: '20px',
    },
    head: {
      textAlign: 'left',
      color: '#f5f6fa',
     
    },
    
    icons: {
 
    padding: '0px 10px',
    color: '#ffffff'
    
    },
    foot: {
    backgroundColor: '#000000',
    textAlign: 'left',
    color: 'white',
    height: '30px',
    fontSize:'18px'
    },
    textLink: {
      color: '#f5f6fa',
      fontFamily: 'Montserrat',
      padding:'0px 70px',
      fontSize:'22px',
     
     
    },
    textLinkLast: {
      color: '#f5f6fa',
      fontFamily: 'Montserrat',
      fontSize:'22px',
    },
   
    link:{
      padding: '5px'
    },
    footer:{
      marginBottom:'10px',
      position:'relative',
      bottom:'-15px',
      maxWidth:'1800px'
      // "@media (min-width: 1920px)": {
      //   marginTop:'40px',
      //   position:'relative',
      //   bottom:'20px'
      // }
     
    }
    
  });

export default function Footer() {
  const classes = useStyles();
    return (
      <div  >
        
          <Grid className={classes.footer}container> 
            <Grid  item xs={4}
              container
              direction="column"
              className={classes.cell}
              >
              <Link className={classes.textLink}>About us</Link>
              <Link className={classes.textLink}>News</Link>
              <Link className={classes.textLink}>Contact us</Link>
            </Grid>
            <Grid  item xs={4}
              container
              direction="column"
              className={classes.cellTwo}
            >
              <Link className={classes.textLink}>Terms and Conditions</Link>
              <Link className={classes.textLink}>Privacy Policy</Link>
            </Grid>
            <Grid  item xs={4}
              container
              direction="column"
              className={classes.cellThree}
            >
              <Grid container direction='row'>
                <Typography variant='body2' className={classes.textLinkLast}>Follow us </Typography>
                <Link  className={classes.icons}><TwitterIcon fontSize="large"/></Link>
                <Link className={classes.icons}><LinkedInIcon fontSize="large"/></Link>
                <Link className={classes.icons}><YouTubeIcon fontSize="large"/></Link>
              </Grid>
                <Typography variant='body2'className={classes.textLinkLast}>  Reach us 
                  <Link className={classes.link}>
                    support@guruinvestars.com
                  </Link>
                
                </Typography>
              
            </Grid>
          </Grid>
         
    
        </div>
        
    )
}
