import React from 'react'
import Grid from '@material-ui/core/Grid';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import YouTubeIcon from '@material-ui/icons/YouTube';
import Link from '@material-ui/core/Link';
import Typography from "@material-ui/core/Typography";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';


const useStyles = makeStyles({
    root:{
        backgroundColor: '#000000',
        marginTop: '40px',
    },
   
    
    icons: {
 
    padding: '15px',
    color: '#ffffff'
    
    },
    foot: {
    color: 'white',
    fontSize:'22px',
    padding:'0px 0px',
    position:'absolute',
    bottom: -30,
    left:700
    },
    textLink: {
      color: '#f5f6fa',
      fontFamily: 'Montserrat',
      padding:'0px 70px'
     
    },
   
    link:{
      padding: '5px'
    },
    footer:{
      // marginTop:'60px',
      position:'absolute',
      bottom:'-40px',
     
     
    }
    
   
  });

export default function FooterOne() {
  const classes = useStyles();
    return (
      <div >
        <Grid container className={classes.footer} xs={11}>
          <Typography variant="subtitle2"  className={classes.foot}>© GuruInveSTARs 2021, All Rights Reserved.</Typography>
        </Grid>   
        </div>
    )
}
