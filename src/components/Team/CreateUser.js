import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import {  useTheme } from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import Dialog from '@material-ui/core/Dialog';
import Paper from '@material-ui/core/Paper';


//Email....

function TabPanel(props) {
  const classes = props ;
  const { children, value, index, ...other } = props;


  return(
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

function Check(props) {
  return (
    <FormControl component="fieldset">
      <FormGroup aria-label="position" row>
       
        <FormControlLabel
          value="start"
          control={<Checkbox color="primary" />}
         
          labelPlacement="start"
        />
        </FormGroup>
    </FormControl>
  );
}


 function Email(props) {
  const classes = useStyles(props);
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
         
        
        >
          <Tab className={classes.tab} label="Single User" {...a11yProps(0)} />
          <Tab label="Multiple Users" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <Grid container
          direction='row'
          justify='space-evenly'
          alignItems='center'
          className={classes.users}>
            <Typography variant='body2'>Enter email address to invite the users to the platform</Typography>
            <TextField id="outlined-basic" label="Enter Email Address" variant="outlined" />
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
        <Grid container
          direction='row'
          justify='space-evenly'
          alignItems='center'
          className={classes.users}>
            <Typography variant='body2'>Enter email address to invite the users to the platform</Typography>
            <form className={classes.TextField} noValidate autoComplete="off">
            <TextField  id="outlined-basic" label="Enter Email Address" helperText="Upload a CSV file or plain text file in CSV format" variant="outlined" ></TextField>
            </form>
            <Button variant='contained' size='small'>Choose a File</Button>
           
          </Grid>
        </TabPanel>
   
      </SwipeableViews>
     
    </div>
  )
}

function createData(user, addUser, editUser, deleteUser, manage) {
  return {user, addUser, editUser, deleteUser, manage};
}


const rows = [
  createData('smitha.shny@gmail.com',<Check/>, <Check/>, <Check/>,<Check/>),
  createData('smitha@kratos.com',<Check/>, <Check/>, <Check/>,<Check/>, ),
  createData('gireesh@kratos.com',<Check/>, <Check/>, <Check/>,<Check/>),
  createData('samiksha@kratos.com',<Check/>, <Check/>, <Check/>,<Check/>),

];


function Permissions(props) {
  const classes = useStyles(props);
  const theme = useTheme();
  const [click, setClick] = React.useState(false);

  const handleClickOpen = () => {
    setClick(true);
  };
  // const handleNext =() =>{
  //   setActiveStep((prevActiveStep) => prevActiveStep + 1);
  // }
  const handleClosed = () => {
    setClick(false);
  };
  
  return (
    <div>
      <Grid item xs={12} className={classes.title}>
        <Typography variant='body2'>By default, all users will have view access, to assign roles and customize the access please check the permission levels below</Typography>
      </Grid>
      <Grid className={classes.table}>
      <Table  aria-label="a dense table" size="small" >
        <TableHead className={classes.tableHead} >
          <TableRow>
          
          <TableCell align="left" className={classes.tableCell}>User Email ID</TableCell>
          <TableCell align="left" className={classes.tableCell}>Add User</TableCell>
          <TableCell align="left" className={classes.tableCell}>Edit User</TableCell>
          <TableCell align="left"className={classes.tableCell}>Delete User </TableCell>
          <TableCell align="left"className={classes.tableCell}>Manage Subscription</TableCell>

          

        </TableRow>
      </TableHead>
      <TableBody>

      {rows.map((row) => (
        <TableRow key={row.name}>
        
          <TableCell  align="left">{row.user}</TableCell>
          <TableCell align="left">{row.addUser}</TableCell>
          <TableCell align="left">{row.editUser}</TableCell>
          <TableCell align="left">{row.deleteUser}</TableCell>
          <TableCell align="left">{row.manage}</TableCell>
        </TableRow>
      ))}
        
      </TableBody>
    </Table>
    </Grid>
   
    </div>
  )
}
function Confirm(props) {
  const classes = useStyles(props);
  const theme = useTheme();
  const [click, setClick] = React.useState(false);

  const handleClickOpen = () => {
    setClick(true);
  };
  // const handleNext =() =>{
  //   setActiveStep((prevActiveStep) => prevActiveStep + 1);
  // }
  const handleClosed = () => {
    setClick(false);
  };
  
  return (
    <div>
      <Grid item xs={12} className={classes.title}>
        <Typography variant='body2'>By default, all users will have view access, to assign roles and customize the access please check the permission levels below</Typography>
      </Grid>
      <Grid className={classes.table}>
      <Table  aria-label="a dense table" size="small" >
        <TableHead className={classes.tableHead} >
          <TableRow>
          
          <TableCell align="left" className={classes.tableCell}>User Email ID</TableCell>
          <TableCell align="left" className={classes.tableCell}>Add User</TableCell>
          <TableCell align="left" className={classes.tableCell}>Edit User</TableCell>
          <TableCell align="left"className={classes.tableCell}>Delete User </TableCell>
          <TableCell align="left"className={classes.tableCell}>Manage Subscription</TableCell>

          

        </TableRow>
      </TableHead>
      <TableBody>

      {rows.map((row) => (
        <TableRow key={row.name}>
        
          <TableCell  align="left">{row.user}</TableCell>
          <TableCell align="left">{row.addUser}</TableCell>
          <TableCell align="left">{row.editUser}</TableCell>
          <TableCell align="left">{row.deleteUser}</TableCell>
          <TableCell align="left">{row.manage}</TableCell>
        </TableRow>
      ))}
        
      </TableBody>
    </Table>
    </Grid>
    <Grid container
    direction='column'
    justify='center'
    alignItems='flex-end'
    className={classes.nextButton}> 
       <Button  className={classes.sendButton}onClick={handleClickOpen} variant='contained'>Send</Button>
    </Grid>
    <Dialog className={classes.dialog} fullWidth='true' maxWidth='sm' onClose={handleClosed}  open={click}>
      <Paper className={classes.dialogPaper}>
      <Grid className={classes.text}>
         <Typography variant='body1' className={classes.text}>Would you like to confirm the roles assigned to the users ?</Typography>
         </Grid>
      <Grid container
         direction='row'
         justify='space-around'
         alignItems='center'
         className={classes.dialogButtons}
         >
           <Button className={classes.button} autoFocus onClick={handleClosed}  variant='contained' color="#ffffff">
             Cancel 
           </Button>
           <Link to='team'>
           <Button className={classes.button}  autoFocus variant='contained' color="primary">
             OK 
           </Button>
           </Link>
           </Grid>

          </Paper>
    </Dialog> 

    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
    borderColor:'#ffffff'
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  tab:{
    width:'300px'
  },
  users:{
    margin:'20px'
  },
  title:{
    margin:'20px'
  },
  table:{
    margin:'20px'

  },
  tableCell:{
    color:'#f88e00'
  },
  nextButton:{
    position: 'relative',
    right: '236px',
    top:  '40px',
   
  },
  sendButton:{
    backgroundColor:"#faed32"
  },
  text:{
    textAlign:'center',
    margin:'20px'
  },
  dialogPaper:{
    backgroundColor:'#707070'
  },
  dialogButtons:{
    marginBottom:'20px'
  }

}));

function getSteps() {
  return ['Email', 'Permissions', 'Confirm'];
}

function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return <Email/>;
   
    case 1:
      return <Confirm/>;
   
  }
}
export default function CreateUser(props) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();
  
  const handleNext = (props) => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };


  const handleBack = () => {
    
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  

  

  return (
    <div className={classes.root}>
      <Stepper  activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
       {getStepContent(activeStep)}
      </div>
         
    <Grid
     container
     direction='row'
     justify ='space-around'
     alignItems='center'>  

     {activeStep === 0 ? 

     <Link to ='team'>
     <Button
        variant='outlined' 
        className={classes.backButton}
      >
        Cancel

      </Button>
      </Link>
    :
      <Button
        variant={activeStep === 0 ? 'disabled' : 'outlined'}
        onClick= {handleBack}
        className={classes.backButton}
      >
        Back

      </Button>
      }
       
          <Button
            variant={activeStep === 2 ? 'disabled' : 'outlined'}
            onClick= {handleNext}
            className={classes.backButton}
          >
             {activeStep === 2 ? '':'Next'}
            
            

          </Button>
          
    </Grid>   
    </div>
  );
}
