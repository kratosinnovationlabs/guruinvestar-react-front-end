import React from 'react';
import { lighten, makeStyles,fade } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';


const useStyles = makeStyles((theme) => ({

  role:{
    color:'#f88e00',
    marginTop:'10px'

  },
  dialog:{
    margin:'20px'
  },
  buttonGroup:{
    border:'none',
  },
  option:{ 
    backgroundColor:'#5a607f',
    margin:'20px',
    textTransform:'none'
  },
  subTitle:{
    marginTop:'20px'
  },
  title:{
    marginTop:'10px'

  },
  tableCell:{
    color:'#f88e00'
  }
  
}))

function Check(props) {
  return (
    <FormControl component="fieldset">
      <FormGroup aria-label="position" row>
       
        <FormControlLabel
          value="start"
          control={<Checkbox color="primary" />}
         
          labelPlacement="start"
        />
        </FormGroup>
    </FormControl>
  );
}

const roles=['Administrative','Business Lead','Analyst']

function createData(role, addUser, editUser, deleteUser, manage) {
  return {role, addUser, editUser, deleteUser, manage};
}


const rows = [
  createData('Administaror',<Check/>, <Check/>, <Check/>,<Check/>),
  createData('Business Leader',<Check/>, <Check/>, ),
  createData('Analyst',<Check/>,),


];



export default function EditUser(props) {
  const classes = useStyles();
  const [open,setOpen]= React.useState(false);
  const anchorRef = React.useRef();
  const [selectedIndex, setSelectedIndex] = React.useState(0);

 

  const handleMenuItemClick = (event, index) => {
    setSelectedIndex(index);
    setOpen(false);
  };

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

 



 
  const handleClosed = (props) => {
    setOpen(false);
  };

  return (
      <div className={classes.dialog} onClose={handleClosed} open={open}>
        <Typography variant='h5'className={classes.title}>Smitha Shenoy</Typography>
        <Typography  className={classes.subTitle}>
          Customize which roles the user can view, edit or delete.
        </Typography>
        <Grid container
          
          direction ='row'
          justify ='flex-start'
          alignItems ='center'
          >
        
          <Typography className={classes.role}>Current Role</Typography>

            
              <ButtonGroup    variant='contained' className={classes.buttonGroup} ref={anchorRef} aria-label="split button">
                <Button
                className={classes.option}
                  size='small'
                  aria-controls={open ? 'split-button-menu' : undefined}
                  aria-expanded={open ? 'true' : undefined}
                  aria-label="select merge strategy"
                  aria-haspopup="menu"
                  onClick={handleToggle}
                >{roles[selectedIndex]}
                  <ArrowDropDownIcon />
                </Button>
              </ButtonGroup>
              <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                {({ TransitionProps, placement }) => (
                  <Grow
                    {...TransitionProps}
                    style={{
                      transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
                    }}
                  >
                    <Paper>
                      <ClickAwayListener onClickAway={handleClosed}>
                        <MenuList className={classes.menu}>
                          {roles.map((roles, index) => (
                            <MenuItem         
                              
                              selected={index === selectedIndex}
                              onClick={(event) => handleMenuItemClick(event, index)}
                            >
                              {roles}
                            </MenuItem>
                          ))}
                        </MenuList>
                      </ClickAwayListener>
                    </Paper>
                  </Grow>
                )}
              </Popper>
              </Grid>
            <Table  aria-label="a dense table" size="small" >
              <TableHead className={classes.tableHead} >
                <TableRow>
                
                <TableCell align="left" className={classes.tableCell}>Role</TableCell>
                <TableCell align="left" className={classes.tableCell}>Add User</TableCell>
                <TableCell align="left" className={classes.tableCell}>Edit User</TableCell>
                <TableCell align="left"className={classes.tableCell}>Delete User </TableCell>
                <TableCell align="left"className={classes.tableCell}>Manage Subscription</TableCell>

               

              </TableRow>
            </TableHead>
            <TableBody>

            {rows.map((row) => (
              <TableRow key={row.name}>
             
                <TableCell  align="left">{row.role}</TableCell>
                <TableCell align="left">{row.addUser}</TableCell>
                <TableCell align="left">{row.editUser}</TableCell>
                <TableCell align="left">{row.deleteUser}</TableCell>
                <TableCell align="left">{row.manage}</TableCell>
              </TableRow>
            ))}
              
            </TableBody>
          </Table>
      
           
           
          </div>
  )
}