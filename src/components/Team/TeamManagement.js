import React from 'react';
import PropTypes from 'prop-types';
import { lighten, makeStyles,fade } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import InputBase from '@material-ui/core/InputBase';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import EditUser from './EditUser';
import CreateUser from './CreateUser';
import Snackbar from '@material-ui/core/Snackbar';
import {Link} from 'react-router-dom';


function createData(name, role, access, lastLogin, ) {
  return { name, role, access, lastLogin };
  
}

const rows = [
  createData('Smitha Shenoy' , 'Business Leader', 'User  Management | Data Management' ,'20 mins ago'),
  createData('Gireesh', 'Business Leader', 'User  Management | Data Management', '5 mins ago'),
  createData('Samiksha Seth', 'Admin','Subscription | User Management |  Data Management','30 mins ago'),
  createData('Ramshiva', 'Admin','Subscription | User Management |  Data Management','Yesterday 10 PM IST'),
  createData('Satheesh', 'Admin','Subscription | User Management |  Data Management','15th June 10:00pm IST'),

];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  { id: 'name', numeric: false, disablePadding: true, label: 'Name' }, 
  { id: 'role',  disablePadding: false, label: 'Role' },
  { id: 'access', disablePadding: false, label: 'Access' },
  { id: 'lastLogin',  disablePadding: false, label: 'Last Login' },
  { id: 'menu', disablePadding: true, label: '' },
];



function EnhancedTableHead(props) {
  const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow className={classes.TableHead}>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ 'aria-label': 'select all desserts' }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            align='center'
            className={classes.tableHeadCell}
            key={headCell.id}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              className={classes.sorttableHeadCell}
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}



const roles=['Business Lead','Administartor','Analyst']
function DropDown(props){
  const classes = props ;
  const [menu, setMenu] = React.useState(false);
  const anchorRef = React.useRef();
 

  const handleToggleMenu = () => {
    setMenu((prevmenu) => !prevmenu);
  };

 

  const handleCloseMenu = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setMenu(false);
  };
  //Edit User
  const [click, setClick] = React.useState(false);

  const handleClickOpen = () => {
    setClick(true);
  };

  const handleClosed = () => {
    setClick(false);
  };

  //Make Admin
  const [admin, setAdmin] = React.useState(false);



  const adminHandleClick = ()=> {
    setAdmin(true);
  };

  const adminHandleClose = () => {
    setAdmin(false );
  };
   //Reset User
   const [reset, setReset] = React.useState(false);



   const resetHandleClick = ()=> {
     setReset(true);
   };
 
   const resetHandleClose = () => {
     setReset(false );
   };
    //Remove User
  const [remove, setRemove] = React.useState(false);

  const removeHandleClickOpen = () => {
    setRemove(true);
  };

  const removeHandleClosed = () => {
    setRemove(false);
  };
return(
    <>
    <ButtonGroup  variant='text' className={classes.buttonGroup} ref={anchorRef} aria-label="split button">
            <Button
              aria-controls={menu ? 'split-button-menu' : undefined}
              aria-expanded={menu ? 'true' : undefined}
              aria-label="select merge strategy"
              aria-haspopup="menu"
              onClick={handleToggleMenu}
            >
              <MoreVertIcon />
            </Button>
          </ButtonGroup>
          <Popper open={menu} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
                }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={handleCloseMenu}>
                    <MenuList className={classes.menu}>
                      {/* {options.map((option, index) => (
                        <MenuItem         
                          key={option}
                          selected={index === selectedIndex}
                          onClick={(event) => handleMenuItemClick(event, index)}
                        >
                          {option}
                        </MenuItem>
                      ))} */}
                       <MenuItem  onClick={handleClickOpen}>Edit </MenuItem>
                      <MenuItem onClick={adminHandleClick}>Make Admin </MenuItem>
                      <MenuItem onClick={resetHandleClick}>Reset Password</MenuItem>
                      <MenuItem onClick={removeHandleClickOpen}>Remove from Account</MenuItem>

                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>

         {/* Edit User  */}

          <Dialog className={classes.dialog} fullWidth='true' maxWidth='md' onClose={handleClosed}  open={click}>
            <EditUser/>
            <Grid container
            direction='row'
            justify='space-between'
            alignItems='center'
            >
              <Button className={classes.button} autoFocus onClick={handleClosed} size='small' variant='contained' color="#ffffff">
                Cancel 
              </Button>
              <Button className={classes.button}  autoFocus onClick={handleClosed} size='small'variant='contained' color="primary">
                Save 
              </Button>
              </Grid>
          </Dialog>  
          

          {/* Make Admin */}

          <Snackbar
            open={admin}
            onClose={adminHandleClose}
            message="This User has been made Admin"
          />

          {/* Reset Password */}
           <Snackbar
            open={reset}
            onClose={resetHandleClose}
            message="Password reset instructions have been mailed to the user"
          />
        {/* Remove Account */}
        
        <Dialog 
        className={classes.remove}  
        fullWidth='true' 
        maxWidth='xs' 
        onClose={handleClosed}  
        open={remove}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title"></DialogTitle>
          <DialogContent>
            <Grid  container
              direction='row'
              justify='space-around'
              alignItems='center'
              >
             <Typography variant='body2' className={classes.content} > Are you sure you want to delete this user ?</Typography>
             </Grid>
          </DialogContent><br/>
          <DialogActions>
            <Grid container
              direction='row'
              justify='space-around'
              alignItems='flex-end'
              >
                <Button className={classes.yes} autoFocus onClick={removeHandleClosed} size='small' variant='contained' color="#ffffff">
                  Yes 
                </Button>
                <Button className={classes.yes}  autoFocus onClick={removeHandleClosed} size='small'variant='contained' color="primary">
                  No 
                </Button>
                </Grid>
              </DialogActions>
          </Dialog>   
    </>
);

}
EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};




const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  TableHead:{
    backgroundColor:'#f89b00',
  },
  tableHeadCell:{
    color:'#000000',
  },
  sorttableHeadCell:{
    color:'#000000'
  },

  heading:{
    color:'#faed32'
  },
  searchIcon: {
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor:'#000000',
    padding:'5px',
    marginLeft: 0,
    width:'100%',
  },
  searchField:{
    marginTop:'10px'
  
  },
  status:{
    margin:'10px'
  },
  buttonGroup:{
    border:'none',
    textTransform:'none'

  },
  option:{
    color:'#f89b00',
    textTransform:'none'

  },
  createUser:{
    backgroundColor:'#faed32',
    textTransform:'none'
  },
  dialog:{
    padding:'20px'
  },
  button:{
    margin:'10px'
  },
 
  
  paper:{
    width:'500px'
  },
   content:{
    color:'#ffffff',
     }
}));




export default function EnhancedTable() {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };



  const isSelected = (name) => selected.indexOf(name) !== -1;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  const options = ['All', 'Pending'];

  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef();
  const [selectedIndex, setSelectedIndex] = React.useState(0);

 

  const handleMenuItemClick = (event, index) => {
    setSelectedIndex(index);
    setOpen(false);
  };

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  //Create User


  const [user, setUser] = React.useState(false);

  const userHandleClick = () => {
    setUser(true);
  };

  const userHandleClosed = (props) => {
    setUser(false);
  };

  return (
    <div className={classes.root}>
      <Typography variant='h5' className={classes.heading}> Team Management</Typography><br/>
 
      <Typography variant='body1' className={classes.body}>Create new users, customize user permissions, and remove users from your account.</Typography><br/>
      <Grid container >
        <Grid item xs={3} >
          <div className={classes.search}>
         
            <InputBase
              placeholder="Search"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
       
          </div>
        </Grid>
        <Typography className={classes.status}>Status</Typography>

        <Grid item xs={6}>
          <ButtonGroup  variant='text' className={classes.buttonGroup} ref={anchorRef} aria-label="split button">
            <Button className={classes.option} size='small' >{options[selectedIndex]}</Button>
            <Button
              aria-controls={open ? 'split-button-menu' : undefined}
              aria-expanded={open ? 'true' : undefined}
              aria-label="select merge strategy"
              aria-haspopup="menu"
              onClick={handleToggle}
            >
              <ArrowDropDownIcon />
            </Button>
          </ButtonGroup>
          <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
                }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={handleClose}>
                    <MenuList className={classes.menu}>
                      {options.map((option, index) => (
                        <MenuItem         
                          key={option}
                          selected={index === selectedIndex}
                          onClick={(event) => handleMenuItemClick(event, index)}
                        >
                          {option}
                        </MenuItem>
                      ))}
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>
        </Grid>
        <Grid item xs={1}>
          <Link to='createuser'>
          <Button  size='small' className={classes.createUser} variant='contained'>
              Create User
          </Button>
          </Link>
        </Grid>
      </Grid><br/>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
           
            aria-label="a dense table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.name);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover                      
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.name}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          onClick={(event) => handleClick(event, row.name)}
                          checked={isItemSelected}
                          inputProps={{ 'aria-labelledby': labelId }}
                        />
                      </TableCell>
                      <TableCell component="th" id={labelId} scope="row" padding="none">
                        {row.name}
                      </TableCell>
                      <TableCell align="center">{row.role}</TableCell>
                      <TableCell align="center">{row.access}</TableCell>
                      <TableCell align="center">{row.lastLogin}</TableCell>
                      <TableCell><DropDown/></TableCell>

                    </TableRow>
                  );
                })}
             
            </TableBody>
          </Table>
        </TableContainer>

      <Dialog fullWidth='true' maxWidth='md' onClose={userHandleClosed}  open={user}><CreateUser/></Dialog> 
  
    </div>
  );
}
