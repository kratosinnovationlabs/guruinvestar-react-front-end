import React ,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

import InputBase from '@material-ui/core/InputBase';


const ChangePassword = (props) => {
  const classes = useStyles();
  const [close,setClose]=useState(true);

  const closeHandler =()=>{
    setClose(false);
  }
  

  return(
    <>
      <Grid container close={close} >
        <Grid item xs={2}
        container
        direction ='column'
        justify='space-around'
        alignItems='stretch'
        >
          <Typography variant="subtitle1" gutterBottom className={classes.password}>New Password </Typography>   
          <Typography variant="subtitle1" gutterBottom className={classes.password}>Confirm New Password </Typography>   

        </Grid>
        <Grid item xs ={3}
        container
        direction='column'
        justify='space-around'
        alignItems='stretch'>
          <TextField id="outlined-basic" type='password' className={classes.valueTwo} placeholder="********"  variant="outlined" size="small"  />
          <TextField id="outlined-basic" type='password' className={classes.valueTwo} placeholder="********"  variant="outlined" size="small" />

        </Grid>
    </Grid>    

    <Button variant="contained" onClick={closeHandler} className={classes.buttonOne}>Update</Button>
      </>
  );
}


const useStyles = makeStyles({
      head: {
       color: '#faed32',
       fontSize:'28px',
      //  "@media (min-width: 1920px)": {
      //   fontSize:'28px'    
       
      // },
      },

    
      subTitle:{
        fontSize:'16px'
      },
      key:{
        color:'#faed32',
        fontSize:'22px',
        textAlign:'left',
        padding:'0px 10px 0px 0px',
        // "@media (min-width: 1920px)": {
        //   fontSize:"22px",
         
        // },

      },
      value:{
        padding:'0px ',
        minWidth:'300px',
        backgroundColor:'#444444',
        borderRadius:'5px'
      },
      email:{                                                                                                                                                   
        
      },
      verified:{
        fontSize:'14px',
        padding:'0px 5px',
        color:'#faed32'
      },
      icon:{
        color:'green'
      },
      textFieldTwo:{
      },
      changePassword:{
        marginTop:'10px'
      },
      valueTwo:{
        backgroundColor:'#444444',
        borderRadius:'5px'
      },
      button:{
        backgroundColor:'#faed32',
        height:"45px",
        fontSize:'18px',
        margin:'0px 10px',
        textTransform:'none',
        // "@media (min-width: 1920px)": {
        //   fontSize:"18px",
        //   height:"45px",

         
        // },

      },
      buttonOne:{
        backgroundColor:'#faed32',
        height:"45px",
        fontSize:'18px',
        margin:'10px 0px',
        textTransform:'none',
        // "@media (min-width: 1920px)": {
        //   fontSize:"18px",
        //   height:"45px",

         
        // },

      },
      password:{
        padding:'15px 0px',
        color:'#faed32',
        fontSize:'22px',
        textAlign:'left',
        // "@media (min-width: 1920px)": {
        //   fontSize:"22px",
         
        // },
        
      },
      input:{
        backgroundColor:'#444444',
        color:'#c0c0c0',
        fontSize:"20px",
        borderRadius:'6px',
        margin:'5px 0px',
        // "@media (min-width: 1920px)": {
        //   fontSize:"20px",
         
        // },

       

        
      },
      inputLast:{
        backgroundColor:'#444444',
        color:'#c0c0c0',
        fontSize:"20px",
        borderRadius:'6px',

        

      }
      
    });

    
 
  
  export default function EditProfile() {
    const classes = useStyles();
    const[open,setOpen]=useState('');
    const currentUser = JSON.parse(localStorage.getItem('user'))
    debugger;

    const clickHandler=()=>{
      setOpen(<ChangePassword/>);
    }

    return (
      <div>
      
        <Typography variant="h6" gutterBottom className={classes.head} >My Account</Typography>
        {/* <Typography variant="h6" gutterBottom className={classes.subTitle} align='left'>Please edit your personal details below -</Typography> */}
          
        <Grid container
         alignItems='center'>
          <Grid item xs={2}>
            <Typography variant="subtitle1" gutterBottom className={classes.key}>First Name </Typography>

          </Grid>
          <Grid item xs={3} >
          <TextField
              className={classes.input}
              id='outlined-basci'
              defaultValue={currentUser.frst_nm}
              variant='outlined'
              style={{width:'40%',
              "@media (min-width: 1920px)": {
                width:'40%'
               
              },
            }}
            />   
          </Grid> 
          <Grid item xs={6}
           container
           direction='row'
           justify='flex-start'
           alignItems='center'>
            <Typography variant="subtitle1" gutterBottom className={classes.key}>Last Name </Typography>  
            <TextField
              className={classes.inputLast}
              id="outlined-basic"
              variant='outlined'
              defaultValue={currentUser.lst_nm}
              inputProps={{  }}
            />
           
       
          </Grid>
        </Grid>
       
        <Grid container
        alignItems='center'
        className={classes.textFieldTwo}>
          <Grid item xs={2}>
          <Typography variant="subtitle1" gutterBottom  className={classes.key}>Email </Typography>

          </Grid>
          <Grid item xs={9} container
          direction='row'
          justify='flex-start'
          alignItems='center'>
          <TextField
              className={classes.input}
              id="outlined-basic"
              variant='outlined'
              style={{width:'40%'}}
              defaultValue={currentUser.email}
            />          
          </Grid>

        </Grid>
        <hr style={{borderColor:'#707070'}}/>
        <Grid container  className={classes.password}>
          <Grid item xs={2}>
            <Typography variant="subtitle1"  gutterBottom className={classes.key}>Current Password    </Typography>

          </Grid>
          <Grid item xs={6}>
            <TextField id="outlined-basic"
             style={{width:'40%',
             "@media (min-width: 1920px)": {
               width:'40%',
               height:"40px",

              
             },
           }} type='password' 
           className={classes.valueTwo} 
           placeholder="********" 
           variant="outlined" 
           size="small" />
            <Button variant='contained' onClick={clickHandler} className={classes.button}> Change Password</Button>
          </Grid>

         
        </Grid>

      {open}   
      
     </div>
    );
  }
  
  

  