import React, {useEffect} from 'react'
import { emailURL } from '../../consts';
import axios from 'axios'
// import {useSetSnackbar} from "../hooks/useSnackbar";
//import MuiAlert from '@material-ui/lab/Alert';
import { useHistory, useParams } from "react-router-dom";

function ConfirmEmail() {
  const [open, setOpen] = React.useState(false);
  //const setSnackbar = useSetSnackbar();
  let history = useHistory();
  let { token } = useParams();

  debugger;

  useEffect(() => {
    // API Call
    //var token = props.match.params.token;
    const token = history.location.pathname.split("/")[2]
    axios.get( emailURL + '/users/confirmation?confirmation_token=' + token)
    .then(function (res) {
      console.log(res)
      setOpen(true);
    //   setSnackbar("Your email address has been successfully confirmed.", "success")
      
    }).catch(error => {
      if (error.response && error.response.status === 403)
        history.push('/logout')
    })
    history.push("/")

  })

  return (
    <div>
    </div>
  )
}

export default ConfirmEmail