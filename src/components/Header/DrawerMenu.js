import React from "react";
import {  Link, useLocation } from "react-router-dom";
import {  makeStyles, useTheme  } from "@material-ui/core/styles";

const drawerWidth = 240;


const useStyles = makeStyles(theme => ({
  
  active:{
    "& .MuiTypography-root":{
      color: '#7e84a3'
    },

    "&::before":{
      content: `''`,
      position: 'absolute',
      borderLeft: '4px solid #7e84a3',
      height: '48px',
      left: 0,
      borderRadius: '10px'
    },
    active:{
      color:'#ffffff'
    },

  },
 
  
}));

export default function MainListItems() {
 
  const location = useLocation();
  const classes = useStyles();
  const isActive = ( value) => ( location.pathname === value ? classes.active : '' )
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  
  return (
    <div >   
      
        

      </div>  
  );
}





