import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useHistory } from "react-router-dom";
import { BaseURL } from '../../consts';
import { CallMissedSharp } from '@material-ui/icons';
import {  makeStyles, useTheme  } from "@material-ui/core/styles";
import axios from 'axios';
import authHeader from '../../services/auth-header';
import {useSetSnackbar} from "../../hooks/useSnackbar";
import { errorHandler } from '../Utility/Utility';
import Spinner from '../../Spinner';

const useStyles = makeStyles((theme) => ({
  search:{
    border:'1px solid #0707070'
  }
}));

export default function Search() {
  const history = useHistory();
  const [value, setValue] = React.useState(null);
  const [inputValue, setInputValue] = React.useState('');
  const [options, setOptions] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const setSnackbar = useSetSnackbar();

  const handleChange = (event,newValue ,reason) => {
    setInputValue(newValue)
    if(newValue !== null){
      history.push(`/stockview/${newValue.symbol}`);
    }
  }

  React.useEffect(() => {
    let active = true;

    if (inputValue === '') {
      setOptions(value ? [value] : []);
      return undefined;
    }

    // (async () => {
    //   const response = await fetch(`${BaseURL}/search_stocks/${inputValue}`, {
    //     headers: authHeader()
    //   });
    //   const stocks = await response.json();
    //   if (active) {
    //     setOptions(stocks);
    //   }
    // })();

    axios.get(BaseURL+`/search_stocks/${inputValue}`, {
      headers: authHeader()
    })
    .then(response =>{
      console.log(response)
      if (response.status===200){ 
        if (active) {
          debugger
          setOptions(response.data);
        }
        setLoading(false);
      }
    }).catch(error => {
      setLoading(false);
      error && error.response && errorHandler(setSnackbar, history, error.response.status)
    })

    return () => {
      active = false;
    };
  }, [value, inputValue]);

  // if (loading) return <Spinner />;

  return (
    <div className={CallMissedSharp.search}>
    <Autocomplete
      style={{ width: 500 , border:'1px solid #707070', borderRadius:'4px'}}
      getOptionLabel={(option) => `${option.symbol} - ${option.name}`}
      filterOptions={(x) => x}
      options={options}
      value={value}
      onChange={(event, newValue) => {
        setOptions(newValue ? [newValue, ...options] : options);
        setValue(newValue);
        handleChange(event, newValue)
      }}
      onInputChange={(event, newInputValue) => {
        setInputValue(newInputValue);
      }}
      renderInput={(params) => (
        <TextField  {...params}  size="small"  label="Search Stock or Company" placeholder='Search' variant="outlined" fullWidth />
        
      )}
    />
    </div>
  );

}