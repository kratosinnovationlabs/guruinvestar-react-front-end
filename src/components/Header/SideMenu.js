import React from 'react'
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import {  makeStyles, useTheme } from "@material-ui/core/styles";
import clsx from "clsx";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import HomeIcon from "@material-ui/icons/Home";
import PeopleIcon from "@material-ui/icons/People";
import BarChartIcon from "@material-ui/icons/BarChart";
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import {  Link, useLocation } from "react-router-dom";
import NotificationsIcon from "@material-ui/icons/Notifications";
import AirplayIcon from '@material-ui/icons/Airplay';
import { Typography } from '@material-ui/core';

const drawerWidth = 350;

const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    top: '135px',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    backgroundColor: '#181815'

  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    top: '135px',
    width: theme.spacing(5) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(7) + 1,
    },
    backgroundColor: '#181815'

  },
  hide: {
    display: 'none',
  },
  active:{
    "& .MuiTypography-root":{
      color: '#7e84a3'
    },

    "&::before":{
      content: `''`,
      position: 'absolute',
      borderLeft: '4px solid #7e84a3',
      height: '48px',
      left: 0,
      borderRadius: '10px'
    },
    active:{
      color:'#ffffff'
    },

    listItemText:{
      fontSize:'14px !important',
      "@media (min-width: 1920px)": {
        fontSize:'20px !important',
      }
    },
    listItem:{
      variant:'h6',
      fontSize:'20px !important',
      "@media (min-width: 1920px)": {
        fontSize:'20px !important',
        variant:'h6'
      }
    }

  },
 
}));

export default function SideMenu(props) {
  const { open } = props;
  const classes = useStyles();
  const theme = useTheme;
  const location = useLocation();
  const isActive = ( value) => ( location.pathname === value ? classes.active : '' )

  return (
    <div>
      <Drawer variant="permanent"  anchor="left" open={open} 
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <List>
       
        <ListItem button  className={classes.listItem} component={Link} to="/GuruSearch" color="inherit" className={isActive('/Dashboard')}>
              <ListItemIcon>
                <HomeIcon/>
              </ListItemIcon>
              <ListItemText  primary={<Typography variant='h6'>Guru Search Screen</Typography>} />
            </ListItem>
            <ListItem button component={Link}  color="inherit">
              <ListItemIcon>
                <AirplayIcon />
              </ListItemIcon>
              <ListItemText primary={<Typography variant='h6'>Stock Search Screen</Typography>} />
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <BarChartIcon />
              </ListItemIcon>
              <ListItemText primary={<Typography variant='h6'>Account Management</Typography>} />
            </ListItem>
            <ListItem button component={Link}  color="inherit">
              <ListItemIcon>
                <PeopleIcon />
              </ListItemIcon>
              <ListItemText primary={<Typography variant='h6'>Team Management</Typography>} />
            </ListItem>
            <ListItem button component={Link}  color="inherit">
              <ListItemIcon>
                < NotificationsIcon />
              </ListItemIcon>
              <ListItemText primary={<Typography variant='h6'>Notifications</Typography>} />
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <HelpOutlineIcon />
              </ListItemIcon>
              <ListItemText primary={<Typography variant='h6'>Help Center</Typography>}/>
            </ListItem>
       
      
        </List>
        {/* <IconButton onClick={handleDrawerClose}>
          <ChevronLeftIcon />
        </IconButton> */}
      </Drawer>

      </div>
  )
}
