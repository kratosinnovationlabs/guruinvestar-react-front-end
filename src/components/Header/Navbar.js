import React, { useState, Fragment } from "react";
import clsx from "clsx";
import { fade, makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import {Link} from 'react-router-dom'
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import Logo from '../../assets/logo.png'
import Search from './Search'
import MenuList from './MenuList'


// For Switch Theming
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";


const drawerWidth = 250;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  toolbar: {
    padding: 0 // keep right padding when drawer closed
    
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    backgroundColor: '#444444',
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  menuButton: {
    marginRight: 36,
    color: '#f5f6fa'
  },
  menuButtonHidden: {
    display: "none"
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column"
  },
  fixedHeight: {
    height: 150
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
    color: '#f5f6fa'
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#ff7f00'   
  },
  inputRoot: {
    color: 'inherit'
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
  rightMenu: {
      marginLeft: 'auto'
  },
  profileMenu: {
    marginLeft: '12px',
    fontSize: '14px',
    color: '#f5f6fa',
    textTransform: 'capitalize'
  },


  iconButtonColor: {
    color: '#f5f6fa'
  },
  logoContainer:{
    width: '216px',
    img: {
      width: "100%"
    }
  },
  linkNav: {
    color: '#f5f6fa !important',
    marginRight: '15px'
  },
  demo:{
    backgroundColor:'#faed32',

  }
}));


export default function Navbar(props) {
  
  const [anchorEl, setAnchorEl] = useState(null);
  const classes = useStyles();
  const currentUser = JSON.parse(localStorage.getItem('user'))
  const { toggleDrawer, open} = props
  
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };


  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  return (
    <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
        >
          <Toolbar className={classes.toolbar}>
            <div className={classes.logoContainer}>
              <img src={Logo} width="100%"/> 
            </div>
            {/* <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                inputProps={{ 'aria-label': 'search' }}
              />
            </div> */}

            {  currentUser &&  <Search/> }
            
            <div className={classes.rightMenu}>
              {/* {  ! currentUser &&  
                <Link to="/" className = {classes.linkNav}>
                  <Button size='small' variant='contained' className={classes.demo}>Request Demo</Button>
                </Link>
              } */}
              {/* <IconButton color="inherit" className={classes.iconButtonColor}>
                <Badge badgeContent={0} color="secondary">
                  <ChatIcon />
                </Badge>
              </IconButton> */}

              {/* <IconButton color="inherit" className={classes.iconButtonColor}>
                <Badge badgeContent={4} color="secondary">
                  <NotificationsIcon />
                </Badge>
              </IconButton> */}
              { currentUser && 
                <Fragment>
                  <Button aria-controls="simple-menu" className={classes.profileMenu} variant='text' aria-haspopup="true" onClick={handleClick} endIcon={<KeyboardArrowDownIcon/>}>
                    { `${currentUser.frst_nm} ${currentUser.lst_nm ? currentUser.lst_nm : "" }`}
                  </Button>
                  <Menu
                  id="simple-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={Boolean(anchorEl)}
                  onClose={ handleClose}>
                  <MenuItem onClick={ handleClose }>Profile</MenuItem>
                  <MenuItem onClick={ handleClose }>My account</MenuItem>
                  <MenuItem onClick={ handleClose } component={Link} to='/editprofile' >Edit Profile</MenuItem>

                  <MenuItem onClick={ handleClose } component={Link} >Edit Alerts</MenuItem>
                  <MenuItem  onClick={ handleClose } component={Link} to='/logout'>Logout</MenuItem>
                </Menu>
                </Fragment>
              }
            </div>
          </Toolbar>
          
        </AppBar>
        
        <MenuList toggleDrawer={toggleDrawer} open={open}/>
        
      </div>

  );
}