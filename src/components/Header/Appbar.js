import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
  navBar: {
    top: '90px !important',
    borderBottom:'inset 1px #707070' ,    

    // height: '50px'
  },
  navMenuStart:{
    textAlign:'left',
    marginRight: '40px',
    color: '#ffc000',
    textDecoration: 'none',
    fontSize:'1.875rem',
    '&:hover': {
      textDecoration: 'none'
    },
    '&:visited': {
      color: '#ffc000'
    }
  },
  navMenu: {
    // margin: '0 24px 10px',
    margin: '0px 40px',
    color: '#ffc000',
    textDecoration: 'none',
    fontSize:'1.875rem',
    '&:hover': {
      textDecoration: 'none'
    },
    '&:visited': {
      color: '#ffc000'
    }
  },
  menuBar: {
    top: '115px !important',
    borderRadius: 'unset',
    // borderBottom:'inset 1px #707070' ,    
    borderColor:'#707070',
    "@media (min-width: 1920px)": {
      top:'120px !important',
    },
    
  },

  menuBtn: {
    position: 'absolute',
    left: 15,
    zIndex: 999999
  },
  toolBar: {
    "@media (min-width: 1920px)": {
    },
   
    minHeight: '35px !important',
    color: '#faed32 !important',
    textAlign:'center'
  },
  toolMenu: {
    fontSize:"28px",
    margin: '0px 20px 0px',
    textAlign:'center',
    justifyContent: 'space-between',
    color: '#faed32',
    '&:hover': {
      textDecoration: 'none'
    },
   
  }
}));
export default function MenuList(props) {
  const { open, toggleDrawer} = props
  const classes = useStyles();


    return (
      
        <div>
          { localStorage.getItem("user") && 
            <> 
              {/* <AppBar position="fixed" className={classes.navBar}>
                <Toolbar className={classes.toolBar}> */}
                  {/* { ! open ?
                      <IconButton
                        aria-label="open drawer"
                        onClick={toggleDrawer}
                        edge="start"
                        className={classes.menuBtn}
                      >
                        <MenuIcon />
                      </IconButton> 
                    :
                    <IconButton
                      aria-label="open drawer"
                      onClick={toggleDrawer}
                      edge="start"
                      className={classes.menuBtn}
                    >
                      <ChevronLeftIcon />
                    </IconButton> 

                  } */}
                 
{/*                     
                    <Link to="/" className = {classes.navMenuStart}>
                    Home
                    </Link>
                    <Link to="gurulist" className = {classes.navMenu}>
                    List of Gurus
                    </Link>
                    <Link href="#" className = {classes.navMenu}>
                      Gurus Trade Book
                    </Link>
                    <Link href="#" className = {classes.navMenu}>
                      Multi Guru Stocks
                    </Link>
                    <Link href="#" className = {classes.navMenu}>
                      Industry Picks
                    </Link>
                    <Link href="#" className = {classes.navMenu}>
                      Sector Trend
                    </Link>
                  

                </Toolbar>
              </AppBar>
               */}
              <Grid container>
              <AppBar position="fixed" className={classes.menuBar}>
                <Toolbar className={classes.toolBar}>
                <Grid item xs={4}></Grid>

                <Grid item xs={8} container direction='row' justifyContent='space-evenly' alignItems='center'>

                  <Typography> <Link to='/favgurus' className = {classes.toolMenu}> Favorite Gurus</Link></Typography>
                  <Typography>  <Link to="/favstocks" className = {classes.toolMenu}> Favorite Stocks</Link></Typography>
                  <Typography>  <Link href="#" className = {classes.toolMenu}> Favorite Screens</Link>      </Typography> 
                    </Grid>
                    <Grid item xs={1}></Grid>

                </Toolbar>
              </AppBar>
              </Grid>
            </>
          }
        </div>
    )
}
