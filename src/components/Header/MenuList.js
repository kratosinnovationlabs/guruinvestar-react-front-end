import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles((theme) => ({
  root: {
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
  navBar: {
    top: '80px !important',
    padding: '0px 20px',


    // height: '50px'
  },
  navMenuStart: {
    textAlign: 'left',
    color: '#ffc000',
    textDecoration: 'none',
    fontSize: '30px',
    '&:hover': {
      textDecoration: 'none'
    },
    '&:visited': {
      color: '#ffc000'
    },
    padding: '0px 40px 0px 0px',


  },
  navMenu: {
    // margin: '0 24px 10px',
    padding: '0px 0px 0px 25px',
    textAlign: 'left',

    color: '#ffc000',
    textDecoration: 'none',
    fontSize: '30px',
    '&:hover': {
      textDecoration: 'none'
    },
    '&:visited': {
      color: '#ffc000'
    },

  },

  menuBar: {
    top: '125px !important',
    borderRadius: 'unset',
    borderBottom: 'inset 1px #707070',
    borderColor: '#707070',
    fontSize: '16px',
    minHeight: '150px'
  },

  menuBtn: {
    position: 'absolute',
    left: 0,

  },
  toolBar: {
    padding: '0px 10px 0px 10px',
    minHeight: '35px !important',
    color: '#faed32 !important',

  },
  toolMenu: {
    // margin: '0 24px 10px',
    //justifyContent: 'center',
    marginLeft: '80px',
    color: '#faed32',
    '&:hover': {
      textDecoration: 'none'
    }
  }
}));
export default function MenuList(props) {
  const { open, toggleDrawer } = props
  const classes = useStyles();


  return (

    <div>
      {localStorage.getItem("user") &&
        <>
          <AppBar position="fixed" className={classes.navBar}>
            <Toolbar className={classes.toolBar}>
              {!open ?
                <IconButton
                  aria-label="open drawer"
                  onClick={toggleDrawer}
                  edge="start"
                  className={classes.menuBtn}
                >
                  <MenuIcon />
                </IconButton>
                :
                <IconButton
                  aria-label="open drawer"
                  onClick={toggleDrawer}
                  edge="start"
                  className={classes.menuBtn}
                >
                  <ChevronLeftIcon />
                </IconButton>

              }

              <Grid
                container
                direction='row'
                justify='space-evenly'
                alignItems='center'>
                <Link to="/" className={classes.navMenuStart}>Home</Link>
                <Link to="#" className={classes.navMenu}>List of GuruinveStars</Link>
                <Link href="#" className={classes.navMenu}>Trade Books</Link>
                <Link href="#" className={classes.navMenu}>Multi Guru Stocks</Link>
                <Link href="#" className={classes.navMenu}>Industry Picks</Link>
                <Link href="#" className={classes.navMenu}>Sector Trend</Link>
              </Grid>

            </Toolbar>
          </AppBar>

          {/* <AppBar position="fixed" className={classes.menuBar}>
                <Toolbar className={classes.toolBar}>
                
                    <Link to='/dashboard' className = {classes.toolMenu}>
                      My Favorite Guru
                    </Link>
                    <Link href="#" className = {classes.toolMenu}>
                      My Favorite Stock
                    </Link>
                    <Link href="#" className = {classes.toolMenu}>
                      My Favorite Screen
                    </Link>              

                </Toolbar>
              </AppBar> */}

        </>
      }
    </div>
  )
}
