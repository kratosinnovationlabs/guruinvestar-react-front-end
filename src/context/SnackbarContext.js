import React, { createContext, useState, memo } from 'react';

export const SnackbarValueContext = createContext({});
export const SnackbarSetContext = createContext(() => {});

const SnackbarProvider = memo(({ setSnackbar, children }) => {  
  const handleSnackbarSet = (message, type) => {
    setSnackbar({
      message, type
    })
  };

  return (
    <SnackbarSetContext.Provider value={handleSnackbarSet}>
      {children}
    </SnackbarSetContext.Provider>
  )
});

export const SnackbarContainer = ({ children }) => {
  const [snackbar, setSnackbar] = useState({
    message: '',
    type: 'success'
  });

  return (
    <SnackbarValueContext.Provider value={snackbar}>
      <SnackbarProvider setSnackbar={setSnackbar}>
        {children}
      </SnackbarProvider>
    </SnackbarValueContext.Provider>
  )
};
